﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace XMLDataTypes_AI.nodes.generic
{
    public class WatchDog : Node
    {
        public Node Condition;
        public Node SubTree;
        
        public WatchDog()
        {
            ClassName = "ai.nodes.WatchDog";
        }

    }
}
