texture screenTexture;
float time;

sampler textureSampler = sampler_state
{
	Texture = <screenTexture>;
};

float4 PixelShaderFunction(float4 color : COLOR0, float2 textureCoordinates : TEXCOORD0) : COLOR0
{
	float4 pixel = tex2D(textureSampler, textureCoordinates) * color;

	pixel.rgb += float3(1, -0.2, -0.2) * pow((length(textureCoordinates - float2(0.5, 0.5)) / sqrt(2)), 1.5) * time * time;
	return pixel;
}

technique Technique1
{
    pass Pass1
    {
        PixelShader = compile ps_2_0 PixelShaderFunction();
    }
}
