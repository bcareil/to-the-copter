texture screenTexture;
float playerHealth;

sampler textureSampler = sampler_state
{
	Texture = <screenTexture>;
};

float4 PixelShaderFunction(float4 color : COLOR0, float2 textureCoordinates : TEXCOORD0) : COLOR0
{
	float4 pixel = tex2D(textureSampler, textureCoordinates) * color;
	float grayscale;

	grayscale = dot(pixel.rgb, float3(0.3, 0.59, 0.11) * (1.0 - length(textureCoordinates - float2(0.5, 0.5))));
	pixel.rgb = lerp(grayscale, pixel.rgb, playerHealth);
	return pixel;
}

technique Technique1
{
    pass Pass1
    {
        PixelShader = compile ps_2_0 PixelShaderFunction();
    }
}
