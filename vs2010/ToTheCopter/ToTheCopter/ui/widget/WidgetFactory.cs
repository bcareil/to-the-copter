﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;

namespace ToTheCopter.ui.widget
{
    public class WidgetFactory
    {
        /*
         * private static attributes
         */
        private static WidgetFactory _instance;

        /*
         * private attributes
         */

        private Texture2D _buttonIcon;
        private SpriteFont _buttonFont;
        private SoundEffect _buttonSound;

        /*
         * private constructor
         */

        private WidgetFactory(ContentManager contentManager)
        {
            _buttonFont = contentManager.Load<SpriteFont>("fonts/menu-entry");
            _buttonIcon = contentManager.Load<Texture2D>("sprites/ui/red-arrow");
            _buttonSound = contentManager.Load<SoundEffect>("sounds/ui/select");
        }

        /*
         * public static methods
         */

        public static void CreateInstance(ContentManager contentLoader)
        {
            _instance = new WidgetFactory(contentLoader);
        }
        
        public static WidgetFactory GetInstance()
        {
            return _instance;
        }

        public ButtonWidget CreateButton(String label)
        {
            return new ButtonWidget(
                new Rectangle(),
                _buttonSound,
                _buttonIcon,
                _buttonFont,
                label
                );
        }

    }
}
