﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace ToTheCopter.ui.widget
{
    public class LabelWidget : IUIGraphicComponent
    {
        /*
         * Attributes
         */
        private Rectangle _bound;
        private SpriteFont _font;
        private String _text;
        private Vector2 _stringDimensions;

        /*
         * Constructors
         */

        public LabelWidget(Rectangle bound, SpriteFont font, String text)
        {
            _bound = bound;
            _font = font;
            _text = text;
            this.Color = Color.White;

            UpdateBound();
        }

        /*
         * Properties implementation
         */

        public Rectangle Bound
        {
            get { return new Rectangle(_bound.X, _bound.Y, _bound.Width, _bound.Height); }
            set { _bound.X = value.X; _bound.Y = value.Y; }
        }

        /*
         * Properties
         */

        public SpriteFont Font
        {
            get { return _font; }
            set { _font = value; UpdateBound(); }
        }

        public String Text
        {
            get { return _text; }
            set { _text = value; UpdateBound(); }
        }

        public Color Color { get; set; }

        /*
         * Public implementation
         */

        public void Draw(SpriteBatch sb)
        {
            sb.DrawString(_font, _text, new Vector2(_bound.Location.X, _bound.Location.Y), this.Color);
        }

        public void Update(float elapsed, float totalElapsed)
        {
            // do nothing
        }

        /*
         * Private methods
         */

        private void UpdateBound()
        {
            if (_bound == null)
            {
                _bound = new Rectangle();
            }
            _stringDimensions = _font.MeasureString(_text);
            _bound.Width = (int)_stringDimensions.X;
            _bound.Height = (int)_stringDimensions.Y;
        }
    }
}
