﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Audio;

namespace ToTheCopter.ui.widget
{

    public delegate void ButtonCallback(ButtonWidget button);

    public class ButtonWidget : IUIComponent
    {
        /*
         * Constants
         */
        private const int ICON_PADDING = 5;
        private const int ICON_SIZE = 16;

        /*
         * Public types
         */

        public enum ButtonState
        {
            UP,
            SELECTED,
            DOWN
        }

        /*
         * Attributes
         */

        private Rectangle _bound;
        private SoundEffect _sound;
        private Texture2D _icon;
        private LabelWidget _label;
        private ButtonState _state;

        /*
         * Events
         */
        
        public ButtonCallback OnSelected;
        public ButtonCallback OnPressed;

        /*
         * Constructor
         */

        public ButtonWidget(Rectangle bound, SoundEffect sound, Texture2D icon, SpriteFont font, String text)
        {
            // create the label
            _label = new LabelWidget(bound, font, text);
            // then set bound
            Bound = bound;
            // set other attributes
            _sound = sound;
            _icon = icon;
            _state = ButtonState.UP;
        }

        /*
         * Properties implementation
         */

        public Rectangle Bound
        {
            get { return _bound; }
            set
            {
                _bound = value;
                _label.Bound = new Rectangle(_bound.X + ICON_SIZE + ICON_PADDING, _bound.Y, _bound.Width, _bound.Height);
                _bound = _label.Bound;
                _bound.X -= ICON_PADDING + ICON_SIZE;
                _bound.Height = Math.Max(ICON_SIZE, _bound.Height);
            }
        }

        /*
         * Properties
         */

        public LabelWidget Label
        {
            get { return _label; }
            set { _label = value; Bound = _label.Bound; }
        }

        public ButtonState State
        {
            get { return _state; }
            set { _state = value; }
        }

        public Texture2D Icon
        {
            get { return _icon; }
            set { _icon = value; }
        }

        /*
         * Public implementation
         */

        public void Draw(SpriteBatch sb)
        {
            if (_state == ButtonState.SELECTED)
            {
                sb.Draw(_icon, new Rectangle(_bound.X, _bound.Y, ICON_SIZE, ICON_SIZE), Color.White);
            }
            _label.Draw(sb);
        }

        public void Update(float elapsed, float totalElapsed)
        {
            _label.Update(elapsed, totalElapsed);
        }

        public bool SetFocusCoordinates(Point p)
        {
            if (_bound.Contains(p))
            {
                SetFocus(FocusPolicy.FIRST);
                return true;
            }
            return false;
        }

        public bool ActivateCoordinates(Point p)
        {
            if (_bound.Contains(p))
            {
                Activate();
                return true;
            }
            return false;
        }

        public bool FocusPrev()
        {
            ClearFocus();
            return false;
        }

        public bool FocusNext()
        {
            ClearFocus();
            return false;
        }

        public bool SetFocus(FocusPolicy policy)
        {
            if (OnSelected != null)
            {
                OnSelected(this);
            }
            _sound.Play();
            _state = ButtonState.SELECTED;
            return true;
        }

        public void Activate()
        {
            if (OnPressed != null)
            {
                OnPressed(this);
            }
        }

        public void ClearFocus()
        {
            _state = ButtonState.UP;
        }

        public bool HasFocus()
        {
            return _state != ButtonState.UP;
        }
    }
}
