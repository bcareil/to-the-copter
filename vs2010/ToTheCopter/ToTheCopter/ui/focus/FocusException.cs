﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ToTheCopter.ui
{
    public class FocusException : System.Exception
    {
        private const string MESSAGE = "Focus handling exception";
        public FocusException() : base(MESSAGE) { }
        public FocusException(string message) : base(MESSAGE + " : " + message) { }
        public FocusException(string message, System.Exception exception) : base(MESSAGE + " : " + message, exception) { }
    }

    public class FocusCorrdinatesException : System.Exception
    {
        private const string MESSAGE = "Invalid coordinates";
        public FocusCorrdinatesException() : base(MESSAGE) { }
        public FocusCorrdinatesException(string message) : base(MESSAGE + " : " + message) { }
        public FocusCorrdinatesException(string message, System.Exception exception) : base(MESSAGE + " : " + message, exception) { }
    }

}
