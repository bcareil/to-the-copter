﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ToTheCopter.engine.inputs;

namespace ToTheCopter.ui.focus
{
    public class FocusHandler
    {
        /*
         * Attributes
         */
        private IUIFocusableComponent _child;

        /*
         * Constructors
         */
        public FocusHandler(IUIFocusableComponent child)
        {
            _child = child;
            _child.SetFocus(FocusPolicy.FIRST);
        }

        public void HandleInputs()
        {
            InputsState inputsState;

            inputsState = InputsHandler.GetInstance().GetGlobalInputs();
            if (inputsState.GetAction(InputAction.MENU_DOWN) == InputState.RELEASED)
            {
                if (_child.FocusNext() == false)
                {
                    _child.SetFocus(FocusPolicy.FIRST);
                }
            }
            if (inputsState.GetAction(InputAction.MENU_UP) == InputState.RELEASED)
            {
                if (_child.FocusPrev() == false)
                {
                    _child.SetFocus(FocusPolicy.LAST);
                }
            }
            if (inputsState.GetAction(InputAction.MENU_SELECT) == InputState.RELEASED)
            {
                if (_child.HasFocus())
                {
                    _child.Activate();
                }
            }
        }
    }
}
