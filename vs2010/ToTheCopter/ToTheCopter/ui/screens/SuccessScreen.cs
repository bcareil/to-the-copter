﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using ToTheCopter.engine;
using ToTheCopter.ui.focus;
using ToTheCopter.ui.layout;
using ToTheCopter.ui.widget;
using Microsoft.Xna.Framework.Graphics;

namespace ToTheCopter.ui.screens
{
    public class SuccessScreen : engine.api.IScreen
    {
        /*
         * Attributes
         */
        private bool _opened;
        private IUIComponent _mainComponent;
        private FocusHandler _focusHandler;
        private Texture2D _squareTexture;
        private Texture2D _successTexture;

        /*
         * Constructor
         */
        public SuccessScreen()
        {
        }

        /*
         * Public implementation
         */

        public void Initialize(ContentManager content, int screenWidth, int screenHeight)
        {
            WidgetFactory widgetFactory;
            ButtonWidget button;
            VerticalLayout layout;

            _squareTexture = content.Load<Texture2D>("sprites/primitives/square");
            _successTexture = content.Load<Texture2D>("sprites/ui/success-defeat");

            widgetFactory = WidgetFactory.GetInstance();

            layout = new VerticalLayout(new Rectangle(0, 0, 200, 300), 12, null);

            button = widgetFactory.CreateButton("quit");
            button.OnPressed = this.OnQuitPressed;
            layout.AddChild(button);

            _mainComponent = new CenterLayout(
                MyGame.GetInstance().Graphics.GraphicsDevice.Viewport.Bounds,
                CenterLayout.CenterBehaviour.BOTH,
                layout);
            _focusHandler = new FocusHandler(_mainComponent);
            _opened = true;
        }

        public void Draw(Microsoft.Xna.Framework.Graphics.SpriteBatch sb)
        {
            Rectangle successDestination;

            successDestination = MyGame.GetInstance().Graphics.GraphicsDevice.Viewport.Bounds;
            successDestination.X = successDestination.Width / 2 - 256;
            successDestination.Y = successDestination.Height / 4 - 128;
            successDestination.Width = 512;
            successDestination.Height = 128;
            sb.Begin();
            sb.Draw(_squareTexture, MyGame.GetInstance().Graphics.GraphicsDevice.Viewport.Bounds, Color.Black * 0.5f);
            sb.Draw(_successTexture, successDestination, new Rectangle(0, 256, 512, 256), new Color(0x38, 0x90, 0x00));
            _mainComponent.Draw(sb);
            sb.End();
        }

        public void Update(float elapsed, float totalElapsed)
        {
            _focusHandler.HandleInputs();
            _mainComponent.Update(elapsed, totalElapsed);
        }

        public void TearDown()
        {
            // do nothing
        }

        public bool IsOpened()
        {
            return _opened;
        }

        public bool IsBlockingUpdate()
        {
            return true;
        }

        public bool IsBlockingDraw()
        {
            return false;
        }

        /*
         * Private buttons handlers
         */

        private void OnQuitPressed(ButtonWidget b)
        {
            _opened = false;
        }
    }
}
