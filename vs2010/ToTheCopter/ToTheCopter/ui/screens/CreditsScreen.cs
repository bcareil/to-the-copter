﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using ToTheCopter.engine;
using ToTheCopter.ui.focus;
using ToTheCopter.ui.layout;
using ToTheCopter.ui.widget;
using Microsoft.Xna.Framework.Graphics;

namespace ToTheCopter.ui.screens
{
    public class CreditsScreen : engine.api.IScreen
    {
        /*
         * Attributes
         */
        private bool _opened;
        private IUIComponent _mainComponent;
        private FocusHandler _focusHandler;
        private Texture2D _creditsTexture;

        /*
         * Constructor
         */
        public CreditsScreen()
        {
        }

        /*
         * Public implementation
         */

        public void Initialize(ContentManager content, int screenWidth, int screenHeight)
        {
            WidgetFactory widgetFactory;
            ButtonWidget button;
            VerticalLayout layout;

            _creditsTexture = content.Load<Texture2D>("sprites/ui/credits");

            widgetFactory = WidgetFactory.GetInstance();

            layout = new VerticalLayout(new Rectangle(0, MyGame.GetInstance().Graphics.GraphicsDevice.Viewport.Bounds.Height - 50, 100, 300), 12, null);

            button = widgetFactory.CreateButton("back");
            button.OnPressed = this.OnBackPressed;
            button.Label.Color = Color.Black;
            layout.AddChild(button);

            _mainComponent = new CenterLayout(
                MyGame.GetInstance().Graphics.GraphicsDevice.Viewport.Bounds,
                CenterLayout.CenterBehaviour.ABSCISSA,
                layout);
            _focusHandler = new FocusHandler(_mainComponent);
            _opened = true;
        }

        public void Draw(Microsoft.Xna.Framework.Graphics.SpriteBatch sb)
        {
            sb.Begin();
            sb.Draw(_creditsTexture, MyGame.GetInstance().Graphics.GraphicsDevice.Viewport.Bounds, Color.White);
            _mainComponent.Draw(sb);
            sb.End();
        }

        public void Update(float elapsed, float totalElapsed)
        {
            _focusHandler.HandleInputs();
            _mainComponent.Update(elapsed, totalElapsed);
        }

        public void TearDown()
        {
            // do nothing
        }

        public bool IsOpened()
        {
            return _opened;
        }

        public bool IsBlockingUpdate()
        {
            return true;
        }

        public bool IsBlockingDraw()
        {
            return false;
        }

        /*
         * Private buttons handlers
         */

        private void OnBackPressed(ButtonWidget b)
        {
            _opened = false;
        }
    }
}
