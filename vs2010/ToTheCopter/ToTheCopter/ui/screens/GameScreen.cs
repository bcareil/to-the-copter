﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ToTheCopter.engine;
using Microsoft.Xna.Framework;
using ToTheCopter.engine.inputs;

namespace ToTheCopter.ui.screens
{
    class GameScreen : engine.api.IScreen
    {
        private game.Game _game;
        private bool _opened;
        private String _levelName;
        private game.Game.DifficultyLevel _difficultyLevel;
        private int _nbPlayers;

        public GameScreen(String levelName, game.Game.DifficultyLevel difficultyLevel, int nbPlayers)
        {
            _levelName = levelName;
            _difficultyLevel = difficultyLevel;
            _nbPlayers = nbPlayers;
        }

        public void Initialize(Microsoft.Xna.Framework.Content.ContentManager content, int screenWidth, int screenHeight)
        {
            _game = new game.Game(
                this.OnSuccess,
                this.OnDefeat,
                content,
                new Point(screenWidth, screenHeight),
                "resources\\maps\\" + _levelName + ".png",
                _difficultyLevel, _nbPlayers
                );
            _opened = true;
        }

        public void Draw(Microsoft.Xna.Framework.Graphics.SpriteBatch sb)
        {
            _game.Draw(sb);
        }

        public void Update(float elapsed, float totalElapsed)
        {
            if (InputsHandler.GetInstance().GetGlobalInputs().GetAction(InputAction.MENU_BACK).IsDown())
            {
                ScreenStackBuilder.GetScreenStack().Push(new IngameMenu(this.Close));
            }
            else
            {
                _game.Update(elapsed, totalElapsed);
            }
        }

        public void TearDown()
        {
            _game = null;
        }

        public bool IsOpened()
        {
            return _opened;
        }

        public bool IsBlockingUpdate()
        {
            return true;
        }

        public bool IsBlockingDraw()
        {
            return true;
        }

        /*
         * private members
         */

        private void Close()
        {
            _opened = false;
        }

        private void OnSuccess()
        {
            Close();
            ScreenStackBuilder.GetScreenStack().Push(new SuccessScreen());
        }

        private void OnDefeat()
        {
            Close();
            ScreenStackBuilder.GetScreenStack().Push(new DefeatScreen());
        }

    }
}
