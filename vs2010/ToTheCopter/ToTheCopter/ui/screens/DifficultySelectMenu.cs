﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using ToTheCopter.engine;
using ToTheCopter.ui.focus;
using ToTheCopter.ui.layout;
using ToTheCopter.ui.widget;

namespace ToTheCopter.ui.screens
{
    public class DifficultySelectMenu : engine.api.IScreen
    {
        /*
         * Attributes
         */
        private bool _opened;
        private IUIComponent _mainComponent;
        private FocusHandler _focusHandler;
        private string _levelName;

        /*
         * Constructor
         */
        public DifficultySelectMenu(string levelName)
        {
            _levelName = levelName;
        }

        /*
         * Public implementation
         */

        public void Initialize(ContentManager content, int screenWidth, int screenHeight)
        {
            WidgetFactory widgetFactory;
            ButtonWidget button;
            VerticalLayout layout;

            widgetFactory = WidgetFactory.GetInstance();

            layout = new VerticalLayout(new Rectangle(0, 0, 200, 300), 12, null);

            button = widgetFactory.CreateButton("easy");
            button.OnPressed = this.OnEasyPressed;
            layout.AddChild(button);

            button = widgetFactory.CreateButton("normal");
            button.OnPressed = this.OnNormalPressed;
            layout.AddChild(button);

            button = widgetFactory.CreateButton("hard");
            button.OnPressed = this.OnHardPressed;
            layout.AddChild(button);

            button = widgetFactory.CreateButton("back");
            button.OnPressed = this.OnBackPressed;
            layout.AddChild(button);

            _mainComponent = new CenterLayout(
                MyGame.GetInstance().Graphics.GraphicsDevice.Viewport.Bounds,
                CenterLayout.CenterBehaviour.BOTH,
                layout);
            _focusHandler = new FocusHandler(_mainComponent);
            _opened = true;
        }

        public void Draw(Microsoft.Xna.Framework.Graphics.SpriteBatch sb)
        {
            MyGame.GetInstance().GraphicsDevice.Clear(new Color(64, 64, 64));
            sb.Begin();
            _mainComponent.Draw(sb);
            sb.End();
        }

        public void Update(float elapsed, float totalElapsed)
        {
            _focusHandler.HandleInputs();
            _mainComponent.Update(elapsed, totalElapsed);
        }

        public void TearDown()
        {
            // do nothing
        }

        public bool IsOpened()
        {
            return _opened;
        }

        public bool IsBlockingUpdate()
        {
            return true;
        }

        public bool IsBlockingDraw()
        {
            return true;
        }

        /*
         * Private buttons handlers
         */

        private void OnEasyPressed(ButtonWidget b)
        {
            ScreenStackBuilder.GetScreenStack().Push(new GameScreen(_levelName, game.Game.DifficultyLevel.EASY, 1));
            _opened = false;
        }

        private void OnNormalPressed(ButtonWidget b)
        {
            ScreenStackBuilder.GetScreenStack().Push(new GameScreen(_levelName, game.Game.DifficultyLevel.NORMAL, 1));
            _opened = false;
        }

        private void OnHardPressed(ButtonWidget b)
        {
            ScreenStackBuilder.GetScreenStack().Push(new GameScreen(_levelName, game.Game.DifficultyLevel.HARD, 1));
            _opened = false;
        }

        private void OnBackPressed(ButtonWidget b)
        {
            _opened = false;
        }
    }
}
