﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using ToTheCopter.engine;
using ToTheCopter.ui.focus;
using ToTheCopter.ui.layout;
using ToTheCopter.ui.widget;

namespace ToTheCopter.ui.screens
{
    public delegate void OnQuit();

    public class IngameMenu : engine.api.IScreen
    {
        /*
         * Attributes
         */
        private bool _opened;
        private IUIComponent _mainComponent;
        private FocusHandler _focusHandler;
        private OnQuit _onQuit;

        /*
         * Constructor
         */
        public IngameMenu(OnQuit onQuit)
        {
            _onQuit = onQuit;
        }

        /*
         * Public implementation
         */

        public void Initialize(ContentManager content, int screenWidth, int screenHeight)
        {
            WidgetFactory widgetFactory;
            ButtonWidget button;
            VerticalLayout layout;

            widgetFactory = WidgetFactory.GetInstance();

            layout = new VerticalLayout(new Rectangle(0, 0, 200, 300), 12, null);

            button = widgetFactory.CreateButton("resume");
            button.OnPressed = this.OnResumePressed;
            layout.AddChild(button);

            button = widgetFactory.CreateButton("quit");
            button.OnPressed = this.OnQuitPressed;
            layout.AddChild(button);

            _mainComponent = new CenterLayout(
                MyGame.GetInstance().Graphics.GraphicsDevice.Viewport.Bounds,
                CenterLayout.CenterBehaviour.BOTH, 
                layout);
            _focusHandler = new FocusHandler(_mainComponent);
            _opened = true;
        }

        public void Draw(Microsoft.Xna.Framework.Graphics.SpriteBatch sb)
        {
            sb.Begin();
            _mainComponent.Draw(sb);
            sb.End();
        }

        public void Update(float elapsed, float totalElapsed)
        {
            _focusHandler.HandleInputs();
            _mainComponent.Update(elapsed, totalElapsed);
        }

        public void TearDown()
        {
            // do nothing
        }

        public bool IsOpened()
        {
            return _opened;
        }

        public bool IsBlockingUpdate()
        {
            return true;
        }

        public bool IsBlockingDraw()
        {
            return false;
        }

        /*
         * Private buttons handlers
         */

        private void OnResumePressed(ButtonWidget b)
        {
            _opened = false;
        }

        private void OnQuitPressed(ButtonWidget b)
        {
            _opened = false;
            _onQuit();
        }
    }
}
