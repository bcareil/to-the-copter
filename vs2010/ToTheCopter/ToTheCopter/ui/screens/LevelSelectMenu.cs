﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using ToTheCopter.engine;
using ToTheCopter.ui.focus;
using ToTheCopter.ui.layout;
using ToTheCopter.ui.widget;
using System;

namespace ToTheCopter.ui.screens
{
    public class LevelSelectMenu : engine.api.IScreen
    {
        /*
         * Attributes
         */
        private bool _opened;
        private IUIComponent _mainComponent;
        private FocusHandler _focusHandler;
        private string[][] _levels = new string[][] {
#if DEBUG
            new string[] { "test-00", "test-00" },
#endif
            new string[] { "level-0", "downtown" },
        };

        /*
         * Constructor
         */
        public LevelSelectMenu()
        {
        }

        /*
         * Public implementation
         */

        public void Initialize(ContentManager content, int screenWidth, int screenHeight)
        {
            WidgetFactory widgetFactory;
            ButtonWidget button;
            VerticalLayout layout;

            widgetFactory = WidgetFactory.GetInstance();

            layout = new VerticalLayout(new Rectangle(0, 0, 200, 300), 12, null);

            for (int i = 0; i < _levels.Length; ++i)
            {
                button = widgetFactory.CreateButton(_levels[i][1]);
                button.OnPressed = this.OnLevelSelected;
                layout.AddChild(button);
            }

            button = widgetFactory.CreateButton("back");
            button.OnPressed = this.OnBackPressed;
            layout.AddChild(button);

            _mainComponent = new CenterLayout(
                MyGame.GetInstance().Graphics.GraphicsDevice.Viewport.Bounds,
                CenterLayout.CenterBehaviour.BOTH,
                layout);
            _focusHandler = new FocusHandler(_mainComponent);
            _opened = true;
        }

        public void Draw(Microsoft.Xna.Framework.Graphics.SpriteBatch sb)
        {
            MyGame.GetInstance().GraphicsDevice.Clear(new Color(64, 64, 64));
            sb.Begin();
            _mainComponent.Draw(sb);
            sb.End();
        }

        public void Update(float elapsed, float totalElapsed)
        {
            _focusHandler.HandleInputs();
            _mainComponent.Update(elapsed, totalElapsed);
        }

        public void TearDown()
        {
            // do nothing
        }

        public bool IsOpened()
        {
            return _opened;
        }

        public bool IsBlockingUpdate()
        {
            return true;
        }

        public bool IsBlockingDraw()
        {
            return true;
        }

        /*
         * Private buttons handlers
         */

        private void OnLevelSelected(ButtonWidget b)
        {
            int i;
            string levelName;

            levelName = null;
            for (i = 0; i < _levels.Length; ++i)
            {
                if (_levels[i][1] == b.Label.Text)
                {
                    levelName = _levels[i][0];
                    break;
                }
            }
            if (i == _levels.Length)
            {
                throw new IndexOutOfRangeException();
            }
            ScreenStackBuilder.GetScreenStack().Push(new DifficultySelectMenu(levelName));
            _opened = false;
        }

        private void OnBackPressed(ButtonWidget b)
        {
            _opened = false;
        }
    }
}
