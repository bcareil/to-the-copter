﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using ToTheCopter.engine;
using ToTheCopter.ui.focus;
using ToTheCopter.ui.layout;
using ToTheCopter.ui.widget;

namespace ToTheCopter.ui.screens
{
    public class MainMenu : engine.api.IScreen
    {
        /*
         * Attributes
         */
        private bool _opened;
        private IUIComponent _mainComponent;
        private FocusHandler _focusHandler;

        /*
         * Constructor
         */
        public MainMenu()
        {
        }

        /*
         * Public implementation
         */

        public void Initialize(ContentManager content, int screenWidth, int screenHeight)
        {
            WidgetFactory widgetFactory;
            ButtonWidget button;
            VerticalLayout layout;

            widgetFactory = WidgetFactory.GetInstance();

            layout = new VerticalLayout(new Rectangle(0, 0, 200, 300), 12, null);

            button = widgetFactory.CreateButton("tutorial");
            button.OnPressed = this.OnTutorialPressed;
            layout.AddChild(button);

            button = widgetFactory.CreateButton("new game");
            button.OnPressed = this.OnNewGamePressed;
            layout.AddChild(button);

            //button = widgetFactory.CreateButton("continue");
            //button.OnPressed = this.OnContinueGamePressed;
            //layout.AddChild(button);

            button = widgetFactory.CreateButton("credits");
            button.OnPressed = this.OnCreditsPressed;
            layout.AddChild(button);

            button = widgetFactory.CreateButton("quit");
            button.OnPressed = this.OnQuitPressed;
            layout.AddChild(button);

            _mainComponent = new CenterLayout(
                MyGame.GetInstance().Graphics.GraphicsDevice.Viewport.Bounds,
                CenterLayout.CenterBehaviour.BOTH,
                layout);
            _focusHandler = new FocusHandler(_mainComponent);
            _opened = true;
        }

        public void Draw(Microsoft.Xna.Framework.Graphics.SpriteBatch sb)
        {
            MyGame.GetInstance().GraphicsDevice.Clear(new Color(64, 64, 64));
            sb.Begin();
            _mainComponent.Draw(sb);
            sb.End();
        }

        public void Update(float elapsed, float totalElapsed)
        {
            _focusHandler.HandleInputs();
            _mainComponent.Update(elapsed, totalElapsed);
        }

        public void TearDown()
        {
            // do nothing
        }

        public bool IsOpened()
        {
            return _opened;
        }

        public bool IsBlockingUpdate()
        {
            return true;
        }

        public bool IsBlockingDraw()
        {
            return true;
        }

        /*
         * Private buttons handlers
         */

        private void OnTutorialPressed(ButtonWidget b)
        {
            ScreenStackBuilder.GetScreenStack().Push(new GameScreen("tutorial/tutorial", game.Game.DifficultyLevel.EASY, 1));
        }

        private void OnNewGamePressed(ButtonWidget b)
        {
            ScreenStackBuilder.GetScreenStack().Push(new LevelSelectMenu());
        }

        private void OnContinueGamePressed(ButtonWidget b)
        {
            // TODO
        }

        private void OnCreditsPressed(ButtonWidget b)
        {
            ScreenStackBuilder.GetScreenStack().Push(new CreditsScreen());
        }

        private void OnQuitPressed(ButtonWidget b)
        {
            _opened = false;
        }
    }
}
