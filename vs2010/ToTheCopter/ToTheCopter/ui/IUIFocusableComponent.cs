﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace ToTheCopter.ui
{
    public enum FocusPolicy
    {
        FIRST,
        LAST
    }

    public interface IUIFocusableComponent
    {
        bool SetFocusCoordinates(Point p);
        bool ActivateCoordinates(Point p);
        bool FocusPrev();
        bool FocusNext();
        bool SetFocus(FocusPolicy policy);
        void Activate();
        void ClearFocus();
        bool HasFocus();
    }
}
