﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace ToTheCopter.ui.layout
{
    public abstract class AChildrenCollectionLayout : IUIComponent
    {
        /*
         * Protected attributes
         */
        protected ICollection<IUIComponent> _children;

        /*
         * C# inheritance sucks
         */
        public virtual Rectangle Bound
        {
            get { throw new NotImplementedException(); }
            set { throw new NotImplementedException(); }
        }

        /*
         * Public implementation
         */

        public virtual void Draw(SpriteBatch sb)
        {
            foreach (IUIComponent child in _children)
            {
                child.Draw(sb);
            }
        }

        public virtual void Update(float elapsed, float totalElapsed)
        {
            foreach (IUIComponent child in _children)
            {
                child.Update(elapsed, totalElapsed);
            }
        }

        public bool SetFocusCoordinates(Point p)
        {
            foreach (IUIComponent child in _children)
            {
                if (child.SetFocusCoordinates(p))
                {
                    return true;
                }
            }
            return false;
        }

        public bool ActivateCoordinates(Point p)
        {
            foreach (IUIComponent child in _children)
            {
                if (child.ActivateCoordinates(p))
                {
                    return true;
                }
            }
            return false;
        }

        public bool FocusPrev()
        {
            IUIComponent prev;

            prev = null;
            foreach (IUIComponent child in _children)
            {
                if (child.HasFocus())
                {
                    if (child.FocusPrev())
                    {
                        return true;
                    }
                    else
                    {
                        if (prev == null)
                        {
                            return false;
                        }
                        else
                        {
                            prev.SetFocus(FocusPolicy.LAST);
                            return true;
                        }
                    }
                }
                prev = child;
            }
            return false;
        }

        public bool FocusNext()
        {
            bool focusNext;
            int idx;

            idx = 0;
            focusNext = false;
            foreach (IUIComponent child in _children)
            {
                if (focusNext)
                {
                    child.SetFocus(FocusPolicy.FIRST);
                    return true;
                }
                else if (child.HasFocus())
                {
                    if (child.FocusNext())
                    {
                        return true;
                    }
                    else
                    {
                        focusNext = true;
                    }
                }
                idx++;
            }
            return false;
        }

        public bool SetFocus(FocusPolicy policy)
        {
            IEnumerator<IUIComponent> it;

            if (policy == FocusPolicy.FIRST)
            {
                it = _children.GetEnumerator();
            }
            else
            {
                it = _children.Reverse().GetEnumerator();
            }
            while (it.MoveNext())
            {
                if (it.Current.SetFocus(policy))
                {
                    return true;
                }
            }
            return false;
        }

        public void Activate()
        {
            foreach (IUIComponent child in _children)
            {
                if (child.HasFocus())
                {
                    child.Activate();
                }
            }
        }

        public void ClearFocus()
        {
            foreach (IUIComponent child in _children)
            {
                child.ClearFocus();
            }
        }

        public bool HasFocus()
        {
            foreach (IUIComponent child in _children)
            {
                if (child.HasFocus())
                {
                    return true;
                }
            }
            return false;
        }
    }
}
