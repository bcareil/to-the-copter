﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace ToTheCopter.ui.layout
{
    public abstract class ASingleChildLayout : IUIComponent
    {
        protected IUIComponent _child;

        /*
         * C# inheritance sucks
         */
        public virtual Rectangle Bound
        {
            get { throw new NotImplementedException(); }
            set { throw new NotImplementedException(); }
        }

        /*
         * Public implementation
         */

        public void Draw(SpriteBatch sb)
        {
            _child.Draw(sb);
        }

        public void Update(float elapsed, float totalElapsed)
        {
            _child.Update(elapsed, totalElapsed);
        }

        public bool SetFocusCoordinates(Point p)
        {
            return _child.SetFocusCoordinates(p);
        }

        public bool ActivateCoordinates(Point p)
        {
            return _child.ActivateCoordinates(p);
        }

        public bool FocusPrev()
        {
            return _child.FocusPrev();
        }

        public bool FocusNext()
        {
            return _child.FocusNext();
        }

        public bool SetFocus(FocusPolicy policy)
        {
            return _child.SetFocus(policy);
        }

        public void Activate()
        {
            if (_child.HasFocus())
            {
                _child.Activate();
            }
        }

        public void ClearFocus()
        {
            _child.ClearFocus();
        }

        public bool HasFocus()
        {
            return _child.HasFocus();
        }
    }
}
