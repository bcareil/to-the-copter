﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace ToTheCopter.ui.layout
{
    public class AbsoluteLayout : ASingleChildLayout
    {
        /*
         * Attributes
         */
        private Rectangle _bound;
        private Rectangle _subBound;

        /*
         * Constructors
         */
        public AbsoluteLayout(Rectangle bound, Rectangle subBound, IUIComponent child)
        {
            _bound = bound;
            _subBound = subBound;
            _child = child;

            ComputeSubBound();
            SetChildBound();
        }

        /*
         * Properties implementation
         */

        override public Rectangle Bound
        {
            get { return new Rectangle(_bound.X, _bound.Y, _bound.Width, _bound.Height); }
            set { _bound = value; ComputeSubBound(); SetChildBound(); }
        }

        /*
         * Properties
         */

        public Rectangle SubBound
        {
            get
            {
                return new Rectangle(
                    _subBound.X - _bound.X,
                    _subBound.Y - _bound.Y,
                    _subBound.Width,
                    _subBound.Height
                    );
            }
            set { _subBound = value; ComputeSubBound(); SetChildBound(); }
        }

        public IUIComponent Child
        {
            get { return _child; }
            set { _child = value; SetChildBound(); }
        }

        /*
         * Private methods
         */

        private void ComputeSubBound()
        {
            if (_bound != null && _subBound != null)
            {
                _subBound.Location = new Point(
                    _subBound.Location.X + _bound.Location.X,
                    _subBound.Location.Y + _bound.Location.Y
                    );
            }
        }

        private void SetChildBound()
        {
            if (_child != null && _subBound != null)
            {
                _child.Bound = _subBound;
            }
        }
    }
}
