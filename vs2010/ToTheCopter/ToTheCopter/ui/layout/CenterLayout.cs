﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace ToTheCopter.ui.layout
{
    public class CenterLayout : ASingleChildLayout
    {
        /*
         * Public types
         */

        public enum CenterBehaviour
        {
            ORDINATE,
            ABSCISSA,
            BOTH
        }

        /*
         * Attributes
         */

        private Rectangle _bound;
        private CenterBehaviour _behaviour;

        /*
         * Constructors
         */

        public CenterLayout(Rectangle bound, CenterBehaviour behaviour, IUIComponent child)
        {
            _bound = bound;
            _behaviour = behaviour;
            _child = child;

            SetChildBound();
        }

        /*
         * Properties implementation
         */

        override public Rectangle Bound
        {
            get { return new Rectangle(_bound.X, _bound.Y, _bound.Width, _bound.Height); }
            set { _bound = value; SetChildBound(); }
        }

        /*
         * Properties
         */

        public CenterBehaviour Behavior
        {
            get { return _behaviour; }
            set { _behaviour = value; SetChildBound(); }
        }

        public IUIComponent Child
        {
            get { return _child; }
            set { _child = value; SetChildBound(); }
        }

        /*
         * Private methods
         */

        private void SetChildBound()
        {
            if (_behaviour == CenterBehaviour.ABSCISSA)
            {
                _child.Bound = new Rectangle(
                    _bound.X + (int)(_bound.Width / 2f - Math.Min(_bound.Width, _child.Bound.Width) / 2f),
                    _child.Bound.Y,
                    _bound.Width,
                    _child.Bound.Height
                    );
            }
            else if (_behaviour == CenterBehaviour.ORDINATE)
            {
                throw new NotImplementedException();
            }
            else if (_behaviour == CenterBehaviour.BOTH)
            {
                _child.Bound = new Rectangle(
                    _bound.X + (int)(_bound.Width / 2f - Math.Min(_bound.Width, _child.Bound.Width) / 2f),
                    _bound.Y + (int)(_bound.Height / 2f - Math.Min(_bound.Height, _child.Bound.Height) / 2f),
                    _child.Bound.Width,
                    _child.Bound.Height
                    );
            }
        }
        
    }
}
