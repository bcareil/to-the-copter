﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using ToTheCopter.engine.utils;

namespace ToTheCopter.ui.layout
{
    class RelativeLayout : ASingleChildLayout
    {
        /*
         * Attributes
         */
        private Rectangle _bound;
        private Rectangle _subBound;
        private Rectf _relativeSubBound;

        /*
         * Constructors
         */

        public RelativeLayout(Rectangle bound, Rectf relativeSubBound, IUIComponent child)
        {
            _bound = bound;
            _relativeSubBound = relativeSubBound;
            _child = child;

            ComputeSubBound();
            SetChildBound();
        }

        /*
         * Properties implementation
         */

        override public Rectangle Bound
        {
            get { return new Rectangle(_bound.X, _bound.Y, _bound.Width, _bound.Height); }
            set { _bound = value; ComputeSubBound(); SetChildBound(); }
        }

        /*
         * Properties
         */

        public Rectf SubBound
        {
            get { return new Rectf(_relativeSubBound); }
            set { _relativeSubBound = value; ComputeSubBound(); SetChildBound(); }
        }

        public IUIComponent Child
        {
            get { return _child; }
            set { _child = value; SetChildBound(); }
        }

        /*
         * Private methods
         */

        private void ComputeSubBound()
        {
            if (_bound != null)
            {
                _subBound = new Rectangle(
                    (int)(_relativeSubBound.X * (float)_bound.Width) + _bound.X,
                    (int)(_relativeSubBound.Y * (float)_bound.Height) + _bound.Y,
                    (int)(_relativeSubBound.Width * (float)_bound.Width),
                    (int)(_relativeSubBound.Height * (float)_bound.Height)
                    );
            }
        }

        private void SetChildBound()
        {
            if (_child != null && _subBound != null)
            {
                _child.Bound = _subBound;
            }
        }
    }
}
