﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using ToTheCopter.engine.inputs;

namespace ToTheCopter.ui.layout
{
    public class VerticalLayout : AChildrenCollectionLayout
    {
        /*
         * Attributes
         */
        private Rectangle _bound;
        private Point _nextPosition;
        private int _padding;

        /*
         * Constructor
         */

        public VerticalLayout(Rectangle bound, int padding, ICollection<IUIComponent> children)
        {
            _bound = bound;
            _padding = padding;
            _children = children;

            SetAllChildrenBound();
        }

        /*
         * Properties implementation
         */

        override public Rectangle Bound
        {
            get { return _bound; }
            set { _bound = value; SetAllChildrenBound(); }
        }

        /*
         * Properties
         */

        public ICollection<IUIComponent> Children
        {
            get { return new List<IUIComponent>(_children); }
            set { _children = value; SetAllChildrenBound(); }
        }

        public int Padding
        {
            get { return _padding; }
            set { _padding = value; SetAllChildrenBound(); }
        }

        /*
         * Public methods
         */

        public void AddChild(IUIComponent child)
        {
            if (_children == null)
            {
                _children = new LinkedList<IUIComponent>();
            }
            _children.Add(child);
            SetAllChildrenBound();
        }

        /*
         * Private methods
         */

        private void SetAllChildrenBound()
        {
            if (_children != null && _bound != null)
            {
                _nextPosition = _bound.Location;
                foreach (IUIComponent child in _children)
                {
                    int nextHeight;

                    nextHeight = Math.Min(
                        _bound.Y + _bound.Height - _nextPosition.Y,
                        child.Bound.Height
                        );
                    child.Bound = new Rectangle(
                        _nextPosition.X,
                        _nextPosition.Y,
                        _bound.Width,
                        nextHeight
                        );
                    _nextPosition.Y += nextHeight + _padding;
                }
            }
        }
    }
}
