﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace ToTheCopter.ui
{
    public interface IUIGraphicComponent : engine.api.IDrawable, engine.api.IUpdatable
    {
        Rectangle Bound { get; set; }
    }
}
