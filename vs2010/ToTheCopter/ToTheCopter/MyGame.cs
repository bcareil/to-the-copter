using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using ToTheCopter.engine;
using ToTheCopter.ui.screens;
using ToTheCopter.engine.inputs;
using ToTheCopter.ui.widget;

namespace ToTheCopter
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class MyGame : Microsoft.Xna.Framework.Game
    {
        public const int NB_MAX_PLAYERS = 4;

        private static MyGame _instance;

        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        SpriteFont _fpsFont;
        engine.api.IScreen _mainScreen;
        engine.api.IScreenStack _screenStack;
#if DEBUG
        int _nbFrames;
        int _fps;
        float _elapsedTime;
#endif

        public static MyGame GetInstance()
        {
            return _instance;
        }

        public MyGame()
        {
            _instance = this;
            Mouse.WindowHandle = Window.Handle;
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
            graphics.PreferMultiSampling = true;
            graphics.PreferredBackBufferWidth = 800;
            graphics.PreferredBackBufferHeight = 600;
        }

        public GraphicsDeviceManager Graphics { get { return graphics; } }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            WidgetFactory.CreateInstance(Content);
            ScreenStackBuilder.BuildScreenStack();
            _screenStack = ScreenStackBuilder.GetScreenStack();
            _mainScreen = (engine.api.IScreen)_screenStack;
            _mainScreen.Initialize(
                Content,
                GraphicsDevice.PresentationParameters.BackBufferWidth,
                GraphicsDevice.PresentationParameters.BackBufferHeight
                );


            _screenStack.Push(new MainMenu());

            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            spriteBatch = new SpriteBatch(GraphicsDevice);
            _fpsFont = Content.Load<SpriteFont>("fonts/fps");
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
            _instance = null;
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            InputsHandler.GetInstance().Update();
            _mainScreen.Update((float) gameTime.ElapsedGameTime.TotalSeconds, (float) gameTime.TotalGameTime.TotalSeconds);
            if (_mainScreen.IsOpened() == false)
                this.Exit();

            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
#if DEBUG
            ++_nbFrames;
            _elapsedTime += (float) gameTime.ElapsedGameTime.TotalSeconds;
            if (_elapsedTime >= 1f)
            {
                _fps = _nbFrames;
                _elapsedTime -= 1f;
                _nbFrames = 0;
            }
#endif

            _mainScreen.Draw(this.spriteBatch);
#if DEBUG
            spriteBatch.Begin();
            spriteBatch.DrawString(_fpsFont, "FPS " + _fps, new Vector2(10, 10), Color.White * 0.5f);
            spriteBatch.End();
#endif

            base.Draw(gameTime);
        }
    }
}
