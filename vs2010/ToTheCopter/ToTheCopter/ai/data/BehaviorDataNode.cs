﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ToTheCopter.ai.data
{
    class BehaviorDataNode : api.IBehaviorDataNode
    {
        private LinkedList<api.IBehaviorDataNode> _children;

        public BehaviorDataNode(Object data)
        {
            _children = new LinkedList<api.IBehaviorDataNode>();
            Data = data;
            Parent = null;
        }

        public object Data { get; set; }

        public api.IBehaviorDataNode Parent { get; set; }

        public IEnumerable<api.IBehaviorDataNode> Children { get { return _children; } }

        public void AddChild(api.IBehaviorDataNode child)
        {
            child.Parent = this;
            _children.AddLast(child);
        }

    }
}
