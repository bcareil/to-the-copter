﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ToTheCopter.ai.data
{
    class WatchDogBehaviorData
    {
        public api.Status OldSubTreeStatus;
        public bool SubTreeExecuted;

        public WatchDogBehaviorData()
        {
            OldSubTreeStatus = api.Status.SUCCESS;
        }

    }
}
