﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ToTheCopter.ai.data
{
    class BehaviorGlobalData : api.IBehaviorGlobalData
    {

        public BehaviorGlobalData(Object entity, Object commonData)
        {
            Entity = entity;
            Properties = new Dictionary<string, Object>();
            DataTreeRoot = null;
            CommonData = commonData;
        }

        public Object Entity { get; private set; }

        public IDictionary<string, Object> Properties { get; private set; }

        public api.IBehaviorDataNode DataTreeRoot { get; set; }

        public Object CommonData { get; set; }
    }
}
