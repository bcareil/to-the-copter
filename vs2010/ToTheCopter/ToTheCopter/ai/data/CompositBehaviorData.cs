﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ToTheCopter.ai.data
{
    public class CompositBehaviorData
    {
        public IEnumerator<api.IBehavior> runningChild;
        public bool moveNext;

        public CompositBehaviorData()
        {
            this.runningChild = null;
            this.moveNext = true;
        }
    }
}
