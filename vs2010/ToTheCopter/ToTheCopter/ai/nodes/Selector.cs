using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ToTheCopter.ai.api;

namespace ToTheCopter.ai.nodes
{
    public class Selector : ACompositBehavior
    {

        public Selector(String name) : base(name)
        {
        }

        override public Status Tick()
        {
            Status childStatus;

            // iterate over children
            while (_data.moveNext == false || _data.runningChild.MoveNext())
            {
                // run the current children
                _data.moveNext = true;
                _data.runningChild.Current.SetUp();
                childStatus = _data.runningChild.Current.Tick();
                _data.runningChild.Current.TearDown(childStatus);
                if (childStatus == Status.SUCCESS)
                {
                    // if it succeed, we succeed, reseting the running child
                    Reset();
                    return Status.SUCCESS;
                }
                else if (childStatus == Status.RUNNING)
                {
                    // if it still run, we do so
                    _data.moveNext = false;
                    return Status.RUNNING;
                }
            }
            // if all children failed, reset the running child
            // and return with failure code.
            Reset();
            return Status.FAILURE;
        }

        override public void TearDown(Status status)
        {
            if (status == Status.FAILURE && _data.runningChild != null)
            {
                _data.runningChild.Current.TearDown(Status.FAILURE);
                Reset();
            }
            else if (status == Status.SUCCESS)
            {
                Reset();
            }
            else if (status == Status.RUNNING)
            {
                _data.moveNext = false;
            }
        }
    }
}
