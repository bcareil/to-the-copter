﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ToTheCopter.ai.nodes
{
    class WatchDogBehavior : ABehavior, api.IWatchDogBehavior
    {

        private data.WatchDogBehaviorData _data;

        public WatchDogBehavior(String name)
            : base(name)
        {
        }

        public api.IBehavior WatchDog { get; set; }
        public api.IBehavior SubTree { get; set; }

        public override api.IBehaviorDataNode FirstSetUp(api.IRootBehavior root)
        {
            api.IBehaviorDataNode mine;

            Root = root;
            mine = new data.BehaviorDataNode(new data.WatchDogBehaviorData());
            mine.AddChild(WatchDog.FirstSetUp(root));
            mine.AddChild(SubTree.FirstSetUp(root));
            return mine;
        }

        public override void InitializeData(api.IBehaviorDataNode dataNode)
        {
            IEnumerator<api.IBehaviorDataNode> dataIterator;

            base.InitializeData(dataNode);
            _data = (data.WatchDogBehaviorData)_dataNode.Data;
            dataIterator = _dataNode.Children.GetEnumerator();
            for (int i = 0; i < 2 && dataIterator.MoveNext(); ++i)
            {
                if (i == 0)
                {
                    WatchDog.InitializeData(dataIterator.Current);
                }
                else
                {
                    SubTree.InitializeData(dataIterator.Current);
                }
            }
        }

        public override void SetUp()
        {
            base.SetUp();
            _data.SubTreeExecuted = false;
        }

        public override api.Status Tick()
        {
            api.Status watchDogStatus;
            api.Status subTreeStatus;

            // run the watch dog
            WatchDog.SetUp();
            watchDogStatus = WatchDog.Tick();
            WatchDog.TearDown(watchDogStatus);
            // depending of the watch dog return value :
            switch (watchDogStatus)
            {
                case api.Status.RUNNING:
                    // we do not run the sub tree
                    //_dataNode.Reset();
                    //return api.Status.RUNNING;
                    throw new Exception("Should not happend (case not handled yet)");
                case api.Status.FAILURE:
                    // we tear down the sub tree if it was running
                    if (_data.OldSubTreeStatus == api.Status.RUNNING)
                    {
                        SubTree.TearDown(api.Status.FAILURE);
                    }
                    _data.OldSubTreeStatus = api.Status.FAILURE;
                    // then return failure
                    return api.Status.FAILURE;
                case api.Status.SUCCESS:
                    // otherwise we run the sub tree
                    SubTree.SetUp();
                    subTreeStatus = SubTree.Tick();
                    _data.SubTreeExecuted = true;
                    // prepare the next loop
                    _data.OldSubTreeStatus = subTreeStatus;
                    // and return the subTreeStatus
                    return subTreeStatus;
                default:
                    throw new Exception("The value return by " + SubTree.Name + " (" + SubTree + ") is invalid : " + watchDogStatus);
            }
        }

        public override void TearDown(api.Status status)
        {
            if (_data.SubTreeExecuted)
            {
                SubTree.TearDown(status);
            }
        }
    }
}
