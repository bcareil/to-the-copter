﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ToTheCopter.ai.nodes
{
    class Negate : api.IBehaviorDecorator
    {
        /*
         * public constructor
         */
        public Negate(api.IBehavior decorated)
        {
            DecoratedBehavior = decorated;
        }

        /*
         * IBehaviorDecorator properties
         */

        public api.IBehavior DecoratedBehavior { get; set; }

        /*
         * IBehavior properties
         */

        public string Name { get { return DecoratedBehavior.Name; } set { DecoratedBehavior.Name = value; } }

        public api.IRootBehavior Root { get { return DecoratedBehavior.Root; } set { DecoratedBehavior.Root = value; } }

        /*
         * IBehavior methods
         */

        public api.IBehaviorDataNode FirstSetUp(api.IRootBehavior root)
        {
            return DecoratedBehavior.FirstSetUp(root);
        }

        public void InitializeData(api.IBehaviorDataNode dataNode)
        {
            DecoratedBehavior.InitializeData(dataNode);
        }

        public void SetUp()
        {
            DecoratedBehavior.SetUp();
        }

        public api.Status Tick()
        {
            api.Status status;

            status = DecoratedBehavior.Tick();
            return NegateStatus(status);
        }

        public void TearDown(api.Status tearDownStatus)
        {
            DecoratedBehavior.TearDown(NegateStatus(tearDownStatus));
        }

        /*
         * private members
         */

        private api.Status NegateStatus(api.Status status)
        {
            switch (status)
            {
                case api.Status.SUCCESS:
                    return api.Status.FAILURE;
                case api.Status.FAILURE:
                    return api.Status.SUCCESS;
                default:
                    return status;
            }
        }
    }
}
