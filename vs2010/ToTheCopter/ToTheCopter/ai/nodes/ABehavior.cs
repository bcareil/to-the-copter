using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ToTheCopter.ai.api;

namespace ToTheCopter.ai.nodes
{
    public abstract class ABehavior : IBehavior
    {
        /*
         * protected attributes
         */

        protected String _name;
        protected IRootBehavior _root;
        protected IBehaviorDataNode _dataNode;

        /*
         * public constructor
         */

        public ABehavior(string name)
        {
            _name = name;
        }

        /*
         * IBehavior implementation
         */

        public String Name
        {
            get { return _name; }
            set { _name = value; }
        }

        public IRootBehavior Root
        {
            get { return _root; }
            set { _root = value; }
        }

        /// <summary>
        /// Provide a default implementation of FirstSetUp creating a 
        /// dataNode for this node and initializing the Root of the tree
        /// </summary>
        public virtual api.IBehaviorDataNode FirstSetUp(IRootBehavior root)
        {
            Root = root;
            return new data.BehaviorDataNode(null);
        }


        /// <summary>
        /// Provide a default implementation of InitializeData
        /// initializing the protected member _dataNode
        /// </summary>
        public virtual void InitializeData(api.IBehaviorDataNode node)
        {
            _dataNode = node;
        }

        /// <summary>
        /// Provide a default and empty implementation of IBehavior.SetUp
        /// </summary>
        public virtual void SetUp()
        {
        }

        abstract public Status Tick();

        /// <summary>
        /// Provide a default and empty implementation of IBehavior.TearDown
        /// </summary>
        public virtual void TearDown(Status status)
        {
            _dataNode = null;
        }
    }
}
