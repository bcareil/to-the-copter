﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ToTheCopter.ai.nodes
{
    class LoopOnSuccess : api.IBehaviorDecorator
    {

        /*
         * private attributes
         */
        
        private api.IBehaviorDataNode _dataNode;
        private data.LoopOnSuccessData _data;

        /*
         * public constructor
         */

        public LoopOnSuccess(api.IBehavior decorated) { DecoratedBehavior = decorated; }

        /*
         * public properties
         */

        public api.IBehavior DecoratedBehavior { get; set; }

        public string Name { get { return DecoratedBehavior.Name; } set { DecoratedBehavior.Name = value; } }

        public api.IRootBehavior Root { get; set; }

        /*
         * public methods
         */

        public api.IBehaviorDataNode FirstSetUp(api.IRootBehavior root)
        {
            api.IBehaviorDataNode myDataNode;

            myDataNode = new data.BehaviorDataNode(new data.LoopOnSuccessData());
            myDataNode.AddChild(DecoratedBehavior.FirstSetUp(root));
            return myDataNode;
        }

        public void InitializeData(api.IBehaviorDataNode dataNode)
        {
            _dataNode = dataNode;
            foreach (api.IBehaviorDataNode childDataNode in _dataNode.Children)
            {
                DecoratedBehavior.InitializeData(childDataNode);
            }
            _data = (data.LoopOnSuccessData)_dataNode.Data;
        }

        public void SetUp()
        {
        }

        public api.Status Tick()
        {
            api.Status status;

            status = api.Status.SUCCESS;
            for (int i = 0; i < 2 && status == api.Status.SUCCESS; ++i)
            {
                DecoratedBehavior.SetUp();
                status = DecoratedBehavior.Tick();
                DecoratedBehavior.TearDown(status);
            }
            _data.LastStatus = status;
            return status;
        }

        public void TearDown(api.Status tearDownStatus)
        {
            if (tearDownStatus != _data.LastStatus)
            {
                DecoratedBehavior.TearDown(tearDownStatus);
            }
        }
    }
}
