using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ToTheCopter.ai.api;

namespace ToTheCopter.ai.nodes
{
    public class RootBehaviorDecorator : IRootBehavior
    {
        private IBehavior _behavior;

        public IBehaviorGlobalData Data { get; set; }

        public RootBehaviorDecorator(IBehavior behavior)
        {
            _behavior = behavior;
        }

        public void Init(IBehaviorGlobalData data)
        {
            data.DataTreeRoot = _behavior.FirstSetUp(this);
        }

        public void Tick(IBehaviorGlobalData data)
        {
            Status status;

            Data = data;
            _behavior.InitializeData(data.DataTreeRoot);
            _behavior.SetUp();
            status = _behavior.Tick();
            _behavior.TearDown(status);
        }
    }
}
