using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ToTheCopter.ai.api;

namespace ToTheCopter.ai.nodes
{
    public abstract class ACompositBehavior : ABehavior, ICompositeBehavior
    {
        /*
         * protected attributes
         */

        private ICollection<IBehavior> _children;
        protected data.CompositBehaviorData _data;

        /*
         * public constructor
         */
        
        public ACompositBehavior(string name)
            : base(name)
        {
            _children = new LinkedList<IBehavior>();
        }

        /*
         * public properties, ICompositBehavior implementation
         */

        public IBehavior RunningChild
        {
            get { return (_data.runningChild == null ? null : _data.runningChild.Current); }
        }

        public void AddChild(IBehavior behavior)
        {
            _children.Add(behavior);
            behavior.Root = Root;
        }

        /*
         * IBehavior implementation/ABehavior override
         */

        public override IBehaviorDataNode FirstSetUp(IRootBehavior root)
        {
            IBehaviorDataNode mine;

            Root = root;
            mine = new data.BehaviorDataNode(new data.CompositBehaviorData());
            foreach (IBehavior child in _children)
            {
                mine.AddChild(child.FirstSetUp(root));
            }
            return mine;
        }

        public override void InitializeData(IBehaviorDataNode dataNode)
        {
            IEnumerator<IBehavior> childIterator;
            IEnumerator<IBehaviorDataNode> dataIterator;

            base.InitializeData(dataNode);
            _data = (data.CompositBehaviorData)_dataNode.Data;
            childIterator = _children.GetEnumerator();
            dataIterator = _dataNode.Children.GetEnumerator();
            while (childIterator.MoveNext() && dataIterator.MoveNext())
            {
                childIterator.Current.InitializeData(dataIterator.Current);
            }
        }

        public override void SetUp()
        {
            if (_data.runningChild == null)
            {
                _data.runningChild = _children.GetEnumerator();
            }
        }

        /*
         * protected method
         */

        protected void Reset()
        {
            _data.runningChild = null;
            _data.moveNext = true;
        }
    }
}
