using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ToTheCopter.ai.api;

namespace ToTheCopter.ai.nodes
{
    public class Sequence : ACompositBehavior
    {

        public Sequence(String name) : base(name)
        {
        }

        override public Status Tick()
        {
            Status childStatus;

            // run all children
            while (_data.moveNext == false || _data.runningChild.MoveNext())
            {
                // run the current children
                _data.moveNext = true;
                _data.runningChild.Current.SetUp();
                childStatus = _data.runningChild.Current.Tick();
                _data.runningChild.Current.TearDown(childStatus);
                if (childStatus == Status.FAILURE)
                {
                    // if it fails, we fail, reseting the running child
                    Reset();
                    return Status.FAILURE;
                }
                else if (childStatus == Status.RUNNING)
                {
                    // if it still run, we do so
                    return Status.RUNNING;
                }
            }
            // if all children succeed, reset the running child
            // and return with success code.
            Reset();
            return Status.SUCCESS;
        }

        override public void TearDown(Status status)
        {
            if (status == Status.FAILURE && _data.runningChild != null)
            {
                // if we are teared down with fail status and
                // we were running, tear down the running child
                // and reset it.
                _data.runningChild.Current.TearDown(Status.FAILURE);
                Reset();
            }
            else if (status == Status.SUCCESS)
            {
                // in case of success, ensure the running child is reseted
                Reset();
            }
            _data.moveNext = (status != Status.RUNNING);
        }
    }
}
