﻿using System;
namespace ToTheCopter.ai.api
{
    public interface IWatchDogBehavior : IBehavior
    {
        IBehavior SubTree { get; set; }
        IBehavior WatchDog { get; set; }
    }
}
