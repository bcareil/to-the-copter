using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ToTheCopter.ai.api
{
    public interface ICompositeBehavior : IBehavior
    {
        void AddChild(IBehavior behavior);
    }
}
