using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ToTheCopter.ai.api
{
    public enum Status
    {
        FAILURE,
        SUCCESS,
        RUNNING
    };

    public interface IBehavior
    {
        /// <summary>
        /// The name of the behavior (for debug and deserialization purpose)
        /// </summary>
        String Name { get; set; }

        /// <summary>
        /// The root of the behavior tree in which this node is.
        /// </summary>
        IRootBehavior Root { get; set; }

        /// <summary>
        /// Called the first time an entity run the tree to create the data structure.
        /// </summary>
        /// <returns>The behaviour data node to add to the data structure</returns>
        IBehaviorDataNode FirstSetUp(IRootBehavior root);

        /// <summary>
        /// Define the data to use for the next execution of SetUp, Tick and/or TearDown
        /// </summary>
        /// <param name="dataNode">The data to use</param>
        void InitializeData(IBehaviorDataNode dataNode);

        /// <summary>
        /// Set up the behavior to prepare a call to Tick
        /// </summary>
        /// <remarks>
        /// The SetUp is called before each Tick.
        /// </remarks>
        void SetUp();

        /// <summary>
        /// Run the behavior
        /// </summary>
        /// <returns>The status of the behavior</returns>
        Status Tick();

        /// <summary>
        /// Mainly called after a Tick, it has to free up resources allocated
        /// during the Tick.
        /// </summary>
        /// <param name="tearDownStatus">
        /// The status to consider. May be different of the return value of
        /// the previous Tick.
        /// </param>
        /// NOTE: The main role of this method is to free up resources when
        ///       the behavior returned RUNNING but the parent behavior has
        ///       to stop it. In this case, even if Tick returned RUNNING,
        ///       the parent will call TearDown with FAILURE as argument.
        void TearDown(Status tearDownStatus);
    }
}
