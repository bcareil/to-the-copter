﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ToTheCopter.ai.api
{
    public interface IBehaviorGlobalData
    {
        Object Entity { get; }
        IDictionary<String, Object> Properties { get; }
        IBehaviorDataNode DataTreeRoot { get; set; }
        Object CommonData { get; set; }
    }
}
