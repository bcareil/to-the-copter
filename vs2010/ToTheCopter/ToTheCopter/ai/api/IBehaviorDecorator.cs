using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ToTheCopter.ai.api
{
    public interface IBehaviorDecorator : IBehavior
    {
        IBehavior DecoratedBehavior { get; set; }
    }
}
