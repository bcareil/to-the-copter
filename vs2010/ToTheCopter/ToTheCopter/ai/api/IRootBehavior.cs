﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ToTheCopter.ai.api
{
    public interface IRootBehavior
    {
        IBehaviorGlobalData Data { get; set; }

        void Init(IBehaviorGlobalData data);
        void Tick(IBehaviorGlobalData data);
    }
}
