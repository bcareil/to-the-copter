﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ToTheCopter.ai.api
{
    public interface IBehaviorDataNode
    {
        Object Data { get; set; }
        IBehaviorDataNode Parent { get; set; }
        IEnumerable<IBehaviorDataNode> Children { get; }
        //IEnumerator<IBehaviorDataNode> PersistedEnumerator { get; set; }

        void AddChild(IBehaviorDataNode child);
    }
}
