﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Content;

namespace ToTheCopter.ai.factory
{
    class BhTBuilder
    {
        class DeserializationException : Exception
        {
            public DeserializationException() : base() { }
            public DeserializationException(String message) : base(message) { }
            public DeserializationException(String message, Exception innerException)
                : base(message, innerException) { }
        }

        /*
         * private attributes
         */
        private ContentManager _content;
        private IDictionary<String, Type> _registeredNodes;
        private api.IBehavior _product;

        public BhTBuilder(ContentManager content)
        {
            _content = content;
            _registeredNodes = new Dictionary<String, Type>();
            _product = null;

            _registeredNodes.Add("ai.nodes.Selector", typeof(nodes.Selector));
            _registeredNodes.Add("ai.nodes.Sequence", typeof(nodes.Sequence));
            _registeredNodes.Add("ai.nodes.LoopOnSuccess", typeof(nodes.LoopOnSuccess));
            _registeredNodes.Add("ai.nodes.Negate", typeof(nodes.Negate));
            _registeredNodes.Add("ai.nodes.WatchDog", typeof(nodes.WatchDogBehavior));
        }

        /*
         * public methods
         */

        public void RegisterCustomNode(String name, Type type)
        {
            _registeredNodes.Add(name, type);
        }

        public void Deserialize(String filename)
        {
            XMLDataTypes_AI.nodes.Node node;

            node = _content.Load<XMLDataTypes_AI.nodes.Node>(filename);

#if !DEBUG
            try
            {
#endif
                _product = DeserializeNode(node);
#if !DEBUG
            }
            catch (KeyNotFoundException ex)
            {
                throw new DeserializationException("The XML file reference an unknow type", ex);
            }
#endif
        }

        public api.IBehavior GetProduct()
        {
            return _product;
        }

        /*
         * private methods
         */

        private api.IBehavior DeserializeNode(XMLDataTypes_AI.nodes.Node node)
        {
            if (node is XMLDataTypes_AI.nodes.CustomNode)
            {
                return DeserializeCustomNode((XMLDataTypes_AI.nodes.CustomNode)node);
            }
            if (node is XMLDataTypes_AI.nodes.generic.Composite)
            {
                return DeserializeCompositeNode((XMLDataTypes_AI.nodes.generic.Composite)node);
            }
            if (node is XMLDataTypes_AI.nodes.generic.WatchDog)
            {
                return DeserializeWatchDogNode((XMLDataTypes_AI.nodes.generic.WatchDog)node);
            }
            if (node is XMLDataTypes_AI.nodes.generic.Decorator)
            {
                return DeserializeDecoratorNode((XMLDataTypes_AI.nodes.generic.Decorator)node);
            }
            throw new ArgumentException(this + ".DeserializeNode : invalid node type : " + node);
        }

        private api.IBehavior DeserializeDecoratorNode(XMLDataTypes_AI.nodes.generic.Decorator nodeDecorator)
        {
            api.IBehaviorDecorator decorator;
            api.IBehavior decorated;

            decorated = DeserializeNode(nodeDecorator.Decorated);
            decorator = ((api.IBehaviorDecorator)
                _registeredNodes[nodeDecorator.ClassName]
                    .GetConstructor(new Type[] { typeof(api.IBehavior) })
                    .Invoke(new Object[] {decorated})
                );
            return decorator;
        }

        private api.IBehavior DeserializeWatchDogNode(XMLDataTypes_AI.nodes.generic.WatchDog nodeWatchDog)
        {
            api.IWatchDogBehavior watchDog;
            api.IBehavior condition;
            api.IBehavior subTree;

            condition = DeserializeNode(nodeWatchDog.Condition);
            subTree = DeserializeNode(nodeWatchDog.SubTree);
            watchDog = ((api.IWatchDogBehavior)
                _registeredNodes[nodeWatchDog.ClassName]
                    .GetConstructor(new Type[] { typeof(String) })
                    .Invoke(new Object[] { nodeWatchDog.Name })
                );
            watchDog.WatchDog = condition;
            watchDog.SubTree = subTree;
            return watchDog;
        }

        private api.IBehavior DeserializeCompositeNode(XMLDataTypes_AI.nodes.generic.Composite nodeComposite)
        {
            api.ICompositeBehavior composite;

            composite = ((api.ICompositeBehavior)
                _registeredNodes[nodeComposite.ClassName]
                    .GetConstructor(new Type[] { typeof(String) })
                    .Invoke(new Object[] { nodeComposite.Name })
                );
            foreach (XMLDataTypes_AI.nodes.Node node in nodeComposite.Children)
            {
                composite.AddChild(DeserializeNode(node));
            }
            return composite;
        }

        private api.IBehavior DeserializeCustomNode(XMLDataTypes_AI.nodes.CustomNode customNode)
        {
            return ((api.IBehavior)
                _registeredNodes[customNode.ClassName]
                    .GetConstructor(new Type[] { typeof(String) })
                    .Invoke(new Object[] { customNode.Name })
                );
        }

    }
}
