﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ToTheCopter.ai.api;
using ToTheCopter.ai.nodes;

namespace ToTheCopter.ai.factory
{
    public static class AINodeFactory
    {
        public enum CompositeBehaviorTypes
        {
            SELECTOR,
            SEQUENCE
        }

        public static ICompositeBehavior CreateCompositBehavior(String name, CompositeBehaviorTypes type)
        {
            switch (type)
            {
                case CompositeBehaviorTypes.SELECTOR:
                    return new Selector(name);
                case CompositeBehaviorTypes.SEQUENCE:
                    return new Sequence(name);
                default:
                    throw new ArgumentException("AINodeFactory : Invalid argument for the function CreateCompositeBehavior : " + type);
            }
        }

        public static IWatchDogBehavior CreateWatchDogBehavior(String name)
        {
            return new WatchDogBehavior(name);
        }

        public static IWatchDogBehavior CreateWatchDogBehavior(String name, IBehavior watchDog, IBehavior subTree)
        {
            IWatchDogBehavior product;

            product = CreateWatchDogBehavior(name);
            product.WatchDog = watchDog;
            product.SubTree = subTree;
            return product;
        }

        public static IRootBehavior CreateRootBehaviorFromBehavior(String name, IBehavior behavior)
        {
            return new RootBehaviorDecorator(behavior);
        }

        public static IBehaviorGlobalData CreateBehaviorGlobalData(Object entity, Object commonData)
        {
            return new data.BehaviorGlobalData(entity, commonData);
        }
    }
}
