﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ToTheCopter.engine.utils
{
    public struct OneDimensionalSegmentFloat
    {
        public float X;
        public float Length;

        public OneDimensionalSegmentFloat(float x, float length)
        {
            X = x;
            Length = length;
        }

        public bool Intersects(OneDimensionalSegmentFloat oth)
        {
            return (
                X >= (oth.X + oth.Length)
                || (X + Length) <= oth.X
                ) == false;
        }

    }
}
