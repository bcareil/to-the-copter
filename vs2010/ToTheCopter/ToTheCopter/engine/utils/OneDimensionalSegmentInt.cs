﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ToTheCopter.engine.utils
{
    public struct OneDimensionalSegmentInt
    {
        public int X;
        public int Length;

        public OneDimensionalSegmentInt(int x, int length)
        {
            X = x;
            Length = length;
        }

        public bool Intersects(OneDimensionalSegmentInt oth)
        {
            return (
                X >= (oth.X + oth.Length)
                || (X + Length) <= oth.X
                ) == false;
        }

    }
}
