﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ToTheCopter.engine.utils
{
    class PriorityQueue<TKey, TObject>
    {

        /*
         * private attributes
         */

        private LinkedList<KeyValuePair<TKey, TObject>> _elements;
        private IComparer<TKey> _comparer;

        /*
         * constructors
         */

        public PriorityQueue(IComparer<TKey> comparer)
        {
            _elements = new LinkedList<KeyValuePair<TKey, TObject>>();
            _comparer = comparer;
        }

        /*
         * properties
         */

        public int Count { get { return _elements.Count; } }

        /*
         * public methods
         */

        /// <summary>
        /// Add an element to the priority queue.
        /// </summary>
        /// <param name="key">The key used to sort elements</param>
        /// <param name="obj">The actual element</param>
        /// <remarks>Take O(n) in the worst case.</remarks>
        public void Add(TKey key, TObject obj)
        {
            if (_elements.Count == 0)
            {
                _elements.AddFirst(new KeyValuePair<TKey, TObject>(key, obj));
            }
            else
            {
                LinkedListNode<KeyValuePair<TKey, TObject>> node;

                node = _elements.First;
                while (node != null)
                {
                    if (_comparer.Compare(key, node.Value.Key) <= 0)
                    {
                        _elements.AddBefore(node, new LinkedListNode<KeyValuePair<TKey, TObject>>(new KeyValuePair<TKey, TObject>(key, obj)));
                        return;
                    }
                    node = node.Next;
                }
            }
        }

        /// <summary>
        /// Retrieve the first, so the smallest, element of the collection.
        /// </summary>
        /// <returns>The first and smallest element of the collection</returns>
        /// <remarks>Perform in O(1)</remarks>
        public TObject Pop()
        {
            TObject obj = _elements.Last.Value.Value;
            _elements.RemoveLast();
            return obj;
        }

        /// <summary>
        /// Empty the collection
        /// </summary>
        public void Clear()
        {
            _elements.Clear();
        }

        /// <summary>
        /// Return true if the specified element is in the collection
        /// </summary>
        /// <param name="newNode">The element to check</param>
        /// <returns>True of the collection contains the specified element</returns>
        public bool Contains(TObject element)
        {
            foreach (KeyValuePair<TKey, TObject> pair in _elements)
            {
                if (pair.Value.Equals(element))
                {
                    return true;
                }
            }
            return false;
        }
    }
}
