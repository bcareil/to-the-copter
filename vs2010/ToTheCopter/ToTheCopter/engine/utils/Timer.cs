﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ToTheCopter.engine.utils
{
    class Timer : api.IUpdatable
    {
        private float _delay;

        public Timer()
        {
            Delay = 0f;
        }

        public float Delay { get { return _delay; } set { _delay = value; if (_delay < 0f) _delay = 0f; } }

        public bool Done { get { return Delay == 0f; } }

        public void Reset(float value)
        {
            Delay = value;
        }

        public void Update(float elapsed, float totalElapsed)
        {
            Delay -= elapsed;
        }

        public void Update(float elapsed)
        {
            Delay -= elapsed;
        }

    }
}
