﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace ToTheCopter.engine.utils
{
    public struct Rectf
    {
        /*
         * Public attributes
         */

        public float X;
        public float Y;
        public float Width;
        public float Height;

        /*
         * Constructors
         */

        public Rectf(float x, float y, float width, float height)
        {
            X = x;
            Y = y;
            Width = width;
            Height = height;
        }

        public Rectf(Vector2 position, Vector2 dimensions)
        {
            X = position.X;
            Y = position.Y;
            Width = dimensions.X;
            Height = dimensions.Y;
        }

        public Rectf(Rectf oth)
        {
            X = oth.X;
            Y = oth.Y;
            Width = oth.Width;
            Height = oth.Height;
        }

        /*
         * public static methods
         */

        public static Rectf FromTwoPoints(Vector2 a, Vector2 b)
        {
            Rectf product;

            product = new Rectf();
            product.X = Math.Min(a.X, b.X);
            product.Y = Math.Min(a.Y, b.Y);
            product.Width = Math.Abs(a.X - b.X);
            product.Height = Math.Abs(a.Y - b.Y);
            return product;
        }

        /*
         * Properties
         */

        public Vector2 TopLeftCorner { get { return Location; } }

        public Vector2 TopRightCorner { get { return new Vector2(Right, Top); } }

        public Vector2 BottomLeftCorner { get { return new Vector2(Left, Bottom); } }

        public Vector2 BottomRightCorner { get { return new Vector2(Right, Bottom); } }

        /// <summary>
        /// The absolute abscissa position of the left side of the rectangle
        /// </summary>
        public float Left { get { return X; } }

        /// <summary>
        /// The absolute abscissa position of the right side of the rectangle
        /// </summary>
        public float Right { get { return X + Width; } }

        /// <summary>
        /// The absolute ordinate position of the top side of the rectangle
        /// </summary>
        public float Top { get { return Y; } }

        /// <summary>
        /// The absolute ordinate position of the bottom side of the rectangle
        /// </summary>
        public float Bottom { get { return Y + Height; } }

        /// <summary>
        /// Upper-Left corner of the rectangle
        /// 
        /// Setting this property move the rectangle
        /// </summary>
        public Vector2 Location
        {
            get { return new Vector2(X, Y); }
            set { X = value.X; Y = value.Y; }
        }

        /// <summary>
        /// Center of the rectangle
        /// 
        /// Setting this property move the rectangle
        /// </summary>
        public Vector2 Center
        {
            get { return new Vector2(X + Width / 2f, Y + Height / 2f); }
            set { X = value.X - Width / 2f; Y = value.Y - Height / 2f; }
        }

        /// <summary>
        /// Dimensions of the rectangle
        /// 
        /// Setting this property resize the rectangle
        /// </summary>
        public Vector2 Dimensions
        {
            get { return new Vector2(Width, Height); }
            set { Width = value.X; Height = value.Y; }
        }

        /*
         * public methods
         */

        /// <summary>
        /// True if the intersection of the two rectangle is not empty
        /// </summary>
        /// <param name="oth">The other rectangle to check the collision with</param>
        /// <returns>True if the intersection of the two rectangle is not empty</returns>
        public bool Intersects(Rectf oth)
        {
            return !(
                X > (oth.X + oth.Width) ||
                Y > (oth.Y + oth.Height) ||
                (X + Width) < oth.X ||
                (Y + Height) < oth.Y
                );
        }

    }
}
