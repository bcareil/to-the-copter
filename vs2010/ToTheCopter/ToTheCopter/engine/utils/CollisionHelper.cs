﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace ToTheCopter.engine.utils
{
    public static class CollisionHelper
    {

        /// <summary>
        /// Compute the distance between the point p and the line
        /// defined by the point a and the unit vector n
        /// </summary>
        /// <param name="a">A point on the line</param>
        /// <param name="n">The unit vector of the line</param>
        /// <param name="p">A point to compute the distance to</param>
        /// <returns>The shortest distance between the point p and the line</returns>
        public static float ComputeDistancePointToALine(Vector2 a, Vector2 n, Vector2 p)
        {
            Vector2 pa;

            pa = a - p;
            return (pa - (DotProduct(pa, n) * n)).Length();
        }

        public static float ComputeMinSquaredDistance(Rectf aabb, Vector2 point)
        {
            return (ComputeClosestPoint(aabb, point) - point).LengthSquared();
        }

        public static Vector2 ComputeClosestPoint(Rectf aabb, Vector2 point)
        {
            Vector2 closestPoint;

            closestPoint = point;
            // check the closest point on X axis
            if (point.X < aabb.Left)
                closestPoint.X = aabb.Left;
            else if (point.X > aabb.Right)
                closestPoint.X = aabb.Right;
            // otherwise, closestPoint.X = _origin.X;
            // check closest point on Y axis
            if (point.Y < aabb.Top)
                closestPoint.Y = aabb.Top;
            else if (point.Y > aabb.Bottom)
                closestPoint.Y = aabb.Bottom;
            // otherwise, closestPoint.Y = _origin.Y;
            return closestPoint;
        }

        public static bool CircleIntersectsAABB(Vector2 center, float radius, Rectf aabb)
        {
            return ComputeMinSquaredDistance(aabb, center) < (radius * radius);
        }

        /// <summary>
        /// return true whether or not the obb and aabb intersection is not empty
        /// </summary>
        /// <param name="a">One point defining the segment defining the obb</param>
        /// <param name="b">The other point defining the segment defining the obb</param>
        /// <param name="halfWidth"></param>
        /// <param name="aabb">The aabb bounds</param>
        /// <returns></returns>
        /// <remarks>
        /// The obb is define by a segment defining by the point a and b and the "halfWidth"
        /// of the obb. If a is on top and b on the botom of the obb, we have:
        /// 
        /// tlc = topLeftCornerCoordinates = a - halfWidth
        /// trc = topRightCornerCoordinates = a + halfWidth
        /// blc = bottomLeftCornerCoordinates = b - halfWidth
        /// brc = bottomRightCornerCoordinates = b + halfWidth
        /// 
        /// <![CDATA[
        ///  tlc          a          trc
        ///   |<halfWidth>|           |
        ///   v           v           v
        ///   +-----------+-----------+
        ///   |                       |
        ///   |                       |
        ///   +-----------+-----------+
        ///   ^           ^           ^
        ///  blc          b          brc
        /// ]]>
        /// </remarks>
        public static bool OBBIntersectsAABB(Vector2 a, Vector2 b, float halfWidth, Rectf aabb)
        {
            Vector2 axisAB;
            Vector2 axisABNormalized;
            Vector2 axisNormal;
            Vector2 axisNormalNormalized;
            OneDimensionalSegmentFloat aabbProjection;
            OneDimensionalSegmentFloat obbProjection;

            // compute the two interesting axis
            axisAB = a - b;
            axisNormal = new Vector2(axisAB.Y, -axisAB.X);
            // normalize them
            axisABNormalized = axisAB;
            axisABNormalized.Normalize();
            axisNormalNormalized = axisNormal;
            axisNormalNormalized.Normalize();
            // project aabb and obb on the normal axis to AB
            // we begin by this one first, because it is the one that has the most chances to fail
            //obbProjection = ProjectPointOnAxis(a, axisNormalNormalized);
            obbProjection = ProjectSegmentOnAxis(
                a - axisNormalNormalized * halfWidth,
                a + axisNormalNormalized * halfWidth,
                axisNormalNormalized
                );
            aabbProjection = ProjectAABBOnAxis(aabb, axisNormalNormalized);
            // check for intersection between projections
            if (obbProjection.Intersects(aabbProjection) == false)
            {
                // is there are none, the two shape do not collide
                return false;
            }
            // project aabb and obb on the axis AB
            obbProjection = ProjectSegmentOnAxis(a, b, axisABNormalized);
            aabbProjection = ProjectAABBOnAxis(aabb, axisABNormalized);
            // and return the result of the intersection between the two projections
            // if the projections do not intersect, it means the shapes do not
            // if the projections intersect, since this is the last axis to test, the shapes collide
            return obbProjection.Intersects(aabbProjection);
        }

        private static OneDimensionalSegmentFloat ProjectPointOnAxis(Vector2 a, Vector2 axisNormalNormalized)
        {
            return new OneDimensionalSegmentFloat(DotProduct(a, axisNormalNormalized), 0f);
        }

        private static OneDimensionalSegmentFloat ProjectSegmentOnAxis(Vector2 a, Vector2 b, Vector2 axisABNormalized)
        {
            float dotA, dotB;
            float min, max;

            dotA = DotProduct(a, axisABNormalized);
            dotB = DotProduct(b, axisABNormalized);
            min = Math.Min(dotA, dotB);
            max = Math.Max(dotA, dotB);
            return new OneDimensionalSegmentFloat(min, max - min);
        }

        private static OneDimensionalSegmentFloat ProjectAABBOnAxis(Rectf aabb, Vector2 axisNormalNormalized)
        {
            float a, b, c, d;
            float min, max;

            a = DotProduct(axisNormalNormalized, aabb.TopLeftCorner);
            b = DotProduct(axisNormalNormalized, aabb.TopRightCorner);
            c = DotProduct(axisNormalNormalized, aabb.BottomLeftCorner);
            d = DotProduct(axisNormalNormalized, aabb.BottomRightCorner);
            min = Math.Min(a, Math.Min(b, Math.Min(c, d)));
            max = Math.Max(a, Math.Max(b, Math.Max(c, d)));
            return new OneDimensionalSegmentFloat(min, max - min);
        }

        private static float DotProduct(Vector2 a, Vector2 b)
        {
            return (a.X * b.X) + (a.Y * b.Y);
        }
    }
}
