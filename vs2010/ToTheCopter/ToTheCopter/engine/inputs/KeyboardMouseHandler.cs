﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework;

namespace ToTheCopter.engine.inputs
{
    class KeyboardMouseHandler : IInputHandler
    {

        public MouseMapper _mouseMapper;
        public KeyboardMapper _keyboardMapper;

        public KeyboardMouseHandler()
        {
            _mouseMapper = new MouseMapper();
            _keyboardMapper = new KeyboardMapper();
        }

        public InputsState GetInputsState()
        {
            InputsState inputsState = new InputsState(InputType.MOUSE_AND_KEYBOARD);
            MouseState ms = Mouse.GetState();
            KeyboardState ks = Keyboard.GetState();

            inputsState.PointerPosition = new Point(ms.X, ms.Y);
            for (int i = 0; i < (int)InputAction.COUNT; ++i)
            {
                InputState inputState;

                inputState = _keyboardMapper.GetKeyStateForActionAndState((InputAction)i, ks);
                if (inputState != InputState.NONE)
                {
                    inputsState.InputsStates[i] = inputState;
                }
                else
                {
                    inputsState.InputsStates[i] = _mouseMapper.GetButtonStateForActionAndState((InputAction)i, ms);
                }
            }
            return inputsState;
        }
    }
}
