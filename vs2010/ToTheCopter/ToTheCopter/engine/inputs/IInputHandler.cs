﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ToTheCopter.engine.inputs
{
    interface IInputHandler
    {
        InputsState GetInputsState();
    }
}
