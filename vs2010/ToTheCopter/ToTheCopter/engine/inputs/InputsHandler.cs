﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace ToTheCopter.engine.inputs
{

    /*
     * Public types
     */

    /**
     * <summary>The state of an input</summary>
     */
    public static class InputStateExtension
    {
        public static bool IsDown(this InputState i)
        {
            return (i == InputState.DOWN || i == InputState.PRESSED);
        }

        public static bool IsUp(this InputState i)
        {
            return (i == InputState.UP || i == InputState.RELEASED);
        }
    }

    public enum InputState
    {
        NONE,
        UP,
        PRESSED,
        DOWN,
        RELEASED,

        COUNT
    }

    /**
     * <summary>The input methods</summary>
     */
    public enum InputType
    {
        NONE,
        GLOBAL,
        MOUSE_AND_KEYBOARD,
        GAMEPAD,

        COUNT
    }

    /**
     * <summary>The actions</summary>
     */
    public enum InputAction
    {
        NONE,
        // in game related
        NORTH,
        SOUTH,
        EAST,
        WEST,
        JUMP,
        FIRE,
        SPRINT,
        RELOAD,
        SELECT_WEAPON_START,
        SELECT_WEAPON_MAIN1,
        SELECT_WEAPON_MAIN2,
        SELECT_WEAPON_THROWABLE,
        DROP_ADD_ON,
        DROP_WEAPON,
        // ui related
        MENU_UP,
        MENU_DOWN,
        MENU_SELECT,
        MENU_BACK,

        COUNT
    }

    /**
     * <summary>The state of player's inputs</summary>
     */
    public class InputsState
    {
        public InputType Type;
        public InputState[] InputsStates;
        public Point PointerPosition;

        public InputsState(InputType type)
        {
            Type = type;
            InputsStates = new InputState[(int)InputAction.COUNT];
            PointerPosition = new Point(0, 0);
        }

        public InputState GetAction(InputAction action)
        {
            return InputsStates[(int)action];
        }

        public void SetAction(InputAction action, InputState state)
        {
            InputsStates[(int)action] = state;
        }

    }

    /*
     * InputsHandler class
     */

    public class InputsHandler
    {

        /*
         * private static attributes
         */

        private static InputsHandler _instance;

        /*
         * private attributes
         */

        private KeyboardMouseHandler _keyboardMouseHandler;
        private InputsState _global;
        private InputsState[] _playersInputs;
        private Boolean _addingPlayerInput = false;
        private int _nbPlayers = 0;

        /*
         * private constructors
         */

        private InputsHandler()
        {
            _keyboardMouseHandler = new KeyboardMouseHandler();
            _global = new InputsState(InputType.GLOBAL);
        }

        /*
         * public static methods
         */

        public static InputsHandler GetInstance()
        {
            if (_instance == null)
            {
                _instance = new InputsHandler();
            }
            return _instance;
        }

        /*
         * public methods
         */

        public InputsState GetGlobalInputs()
        {
            return _global;
        }

        public InputsState GetInputsForPlayer(int idx)
        {
            return _global;
            //return _playersInputs[idx];
        }

        public void StartPlayersInputsPolling()
        {
            _nbPlayers = 0;
            _addingPlayerInput = true;
        }

        public int EndPlayersInputsPolling()
        {
            _addingPlayerInput = false;
            return _nbPlayers;
        }

        public void Update()
        {
            UpdateInputState(_global);
        }

        /*
         * private methods
         */

        private void UpdateInputState(InputsState inputs)
        { 
            InputsState keyboardAndMouse = _keyboardMouseHandler.GetInputsState();

            RefreshInputsStates(inputs, keyboardAndMouse);
        }

        private void RefreshInputsStates(InputsState old, InputsState src)
        {
            old.PointerPosition = src.PointerPosition;
            for (int i = 0; i < (int)InputAction.COUNT; ++i)
            {
                if (src.InputsStates[i] == InputState.UP
                    && (old.InputsStates[i] == InputState.DOWN
                        || old.InputsStates[i] == InputState.PRESSED))
                    old.InputsStates[i] = InputState.RELEASED;
                else if (src.InputsStates[i] == InputState.DOWN
                    && (old.InputsStates[i] == InputState.UP
                        || old.InputsStates[i] == InputState.RELEASED))
                    old.InputsStates[i] = InputState.PRESSED;
                else
                    old.InputsStates[i] = src.InputsStates[i];
            }
        }
    }
}
