﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Input;

namespace ToTheCopter.engine.inputs
{
    class KeyboardMapper
    {
        private IDictionary<InputAction, Keys> _mapping;

        public KeyboardMapper()
        {
            _mapping = new Dictionary<InputAction, Keys>((int)InputAction.COUNT);
            // default mapping
            _mapping[InputAction.NORTH] = Keys.W;
            _mapping[InputAction.SOUTH] = Keys.S;
            _mapping[InputAction.EAST] = Keys.D;
            _mapping[InputAction.WEST] = Keys.A;
            _mapping[InputAction.JUMP] = Keys.Space;
            //_mapping[InputAction.FIRE] = Keys.None;
            _mapping[InputAction.SPRINT] = Keys.LeftShift;
            _mapping[InputAction.RELOAD] = Keys.R;
            _mapping[InputAction.SELECT_WEAPON_START] = Keys.D1;
            _mapping[InputAction.SELECT_WEAPON_MAIN1] = Keys.D2;
            _mapping[InputAction.SELECT_WEAPON_MAIN2] = Keys.D3;
            _mapping[InputAction.SELECT_WEAPON_THROWABLE] = Keys.D4;
            _mapping[InputAction.DROP_ADD_ON] = Keys.V;

            _mapping[InputAction.MENU_UP] = Keys.Up;
            _mapping[InputAction.MENU_DOWN] = Keys.Down;
            _mapping[InputAction.MENU_SELECT] = Keys.Enter;
            _mapping[InputAction.MENU_BACK] = Keys.Escape;
        }

        public void MapKeyOnAction(Keys key, InputAction action)
        {
            _mapping[action] = key;
        }

        public Keys GetKeyForAction(InputAction action)
        {
            if (_mapping.ContainsKey(action))
            {
                return _mapping[action];
            }
            else
            {
                return Keys.None;
            }
        }

        public IEnumerator<KeyValuePair<InputAction, Keys>> GetEnumerator()
        {
            return _mapping.GetEnumerator();
        }

        internal InputState GetKeyStateForActionAndState(InputAction inputAction, KeyboardState ks)
        {
            if (_mapping.ContainsKey(inputAction))
            {
                return (ks.IsKeyDown(_mapping[inputAction]) ? InputState.DOWN : InputState.UP);
            }
            else
            {
                return InputState.NONE;
            }
        }
    }
}
