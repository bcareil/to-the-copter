﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Input;

namespace ToTheCopter.engine.inputs
{

    delegate InputState MouseButtonHandler(MouseState ms);

    public enum MouseButton
    {
        NONE,
        LEFT,
        MIDDLE,
        RIGHT,

        COUNT
    }

    class MouseMapper
    {
        private IDictionary<InputAction, MouseButton> _actionMapping;
        private MouseButtonHandler[] _buttonMapping;

        public MouseMapper()
        {
            _actionMapping = new Dictionary<InputAction, MouseButton>((int)InputAction.COUNT);
            // constant
            _buttonMapping = new MouseButtonHandler[(int)MouseButton.COUNT];
            _buttonMapping[(int)MouseButton.NONE] = HandlerButtonNone;
            _buttonMapping[(int)MouseButton.LEFT] = HandlerButtonLeft;
            _buttonMapping[(int)MouseButton.MIDDLE] = HandlerButtonMiddle;
            _buttonMapping[(int)MouseButton.RIGHT] = HandlerButtonRight;
            // default mapping
            //_actionMapping[InputAction.BACWARD] = MouseButton.NONE;
            //_actionMapping[InputAction.DROP_ADD_ON] = MouseButton.NONE;
            _actionMapping[InputAction.FIRE] = MouseButton.LEFT;
            //_actionMapping[InputAction.FORWARD] = MouseButton.NONE;
            //_actionMapping[InputAction.GRENADE] = MouseButton.NONE;
            //_actionMapping[InputAction.JUMP] = MouseButton.NONE;
            //_actionMapping[InputAction.LEFT] = MouseButton.NONE;
            //_actionMapping[InputAction.RELOAD] = MouseButton.NONE;
            //_actionMapping[InputAction.RIGHT] = MouseButton.NONE;
            //_actionMapping[InputAction.SPRINT] = MouseButton.NONE;

            //_actionMapping[InputAction.MENU_DOWN] = MouseButton.NONE;
            //_actionMapping[InputAction.MENU_SELECT] = MouseButton.NONE;
            //_actionMapping[InputAction.MENU_UP] = MouseButton.NONE;
        }

        public void MapKeyOnAction(MouseButton button, InputAction action)
        {
            _actionMapping[action] = button;
        }

        public MouseButton GetButtonForAction(InputAction action)
        {
            return _actionMapping[action];
        }

        public IEnumerator<KeyValuePair<InputAction, MouseButton>> GetEnumerator()
        {
            return _actionMapping.GetEnumerator();
        }

        internal InputState GetButtonStateForActionAndState(InputAction inputAction, MouseState ms)
        {
            if (_actionMapping.ContainsKey(inputAction))
            {
                return _buttonMapping[(int)_actionMapping[inputAction]](ms);
            }
            else
            {
                return InputState.NONE;
            }
        }

        /*
         * Mouse button handlers
         */

        private InputState HandlerButtonNone(MouseState ms)
        {
            return InputState.NONE;
        }

        private InputState HandlerButtonLeft(MouseState ms)
        {
            return (ms.LeftButton == ButtonState.Pressed ? InputState.DOWN : InputState.UP);
        }

        private InputState HandlerButtonMiddle(MouseState ms)
        {
            return (ms.MiddleButton == ButtonState.Pressed ? InputState.DOWN : InputState.UP);
        }

        private InputState HandlerButtonRight(MouseState ms)
        {
            return (ms.RightButton == ButtonState.Pressed ? InputState.DOWN : InputState.UP);
        }
    }
}
