﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ToTheCopter.engine
{
    public abstract class AObservable : api.IObservable
    {
        private ICollection<api.IObserver> _observers;

        public AObservable()
        {
            _observers = new HashSet<api.IObserver>();
        }

        public void AddObserver(api.IObserver observer)
        {
            _observers.Add(observer);
        }

        public void RemoveObserver(api.IObserver observer)
        {
            _observers.Remove(observer);
        }

        protected void Notify()
        {
            foreach (api.IObserver observer in _observers)
            {
                observer.Notify();
            }
        }
    }
}
