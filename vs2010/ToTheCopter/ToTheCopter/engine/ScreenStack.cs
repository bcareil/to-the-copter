﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;

namespace ToTheCopter.engine
{
    public class ScreenStack : api.IScreen, api.IScreenStack
    {
        private Stack<api.IScreen> _screens;
        private ContentManager _content;
        private int _screenWidth;
        private int _screenHeight;

        public ScreenStack()
        {
            _screens = new Stack<api.IScreen>();
        }

        public void Push(api.IScreen screen)
        {
            screen.Initialize(_content, _screenWidth, _screenHeight);
            _screens.Push(screen);
        }

        public void Pop()
        {
            api.IScreen screen;
            
            screen = _screens.Pop();
            screen.TearDown();
        }

        public void Initialize(ContentManager content, int width, int height)
        {
            _content = content;
            _screenWidth = width;
            _screenHeight = height;
        }

        private void DrawRec(SpriteBatch sb, IEnumerator<api.IScreen> screenIt)
        {
            if (screenIt.MoveNext())
            {
                api.IScreen screen = screenIt.Current;

                if (screen.IsBlockingDraw() == false)
                {
                    DrawRec(sb, screenIt);
                }
                screen.Draw(sb);
            }
        }

        public void Draw(SpriteBatch sb)
        {
            DrawRec(sb, _screens.GetEnumerator());
        }

        public void Update(float elapsed, float totalElapsed)
        {
            foreach (api.IScreen screen in _screens)
            {
                screen.Update(elapsed, totalElapsed);
                if (screen.IsBlockingUpdate())
                {
                    break;
                }
            }
            while (_screens.Count > 0 && _screens.Peek().IsOpened() == false)
            {
                Pop();
            }
        }

        public void TearDown()
        {
            // do nothing
        }

        public bool IsOpened()
        {
            return (_screens.Count != 0);
        }

        public bool IsBlockingUpdate()
        {
            return true;
        }

        public bool IsBlockingDraw()
        {
            return true;
        }
    }
}
