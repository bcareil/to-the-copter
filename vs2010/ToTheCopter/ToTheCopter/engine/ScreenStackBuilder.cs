﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ToTheCopter.engine
{
    static class ScreenStackBuilder
    {
        private static api.IScreenStack _instance;

        public static void BuildScreenStack()
        {
            _instance = new ScreenStack();
        }
        
        public static api.IScreenStack GetScreenStack()
        {
            return _instance;
        }
    }
}
