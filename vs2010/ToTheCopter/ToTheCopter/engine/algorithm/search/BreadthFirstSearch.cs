﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ToTheCopter.engine.algorithm.search
{
    class BreadthFirstSearch : api.ISearchAlgorithm
    {
        public ICollection<api.ISearchNode> ComputePath(api.ISearchProblem problem)
        {
            ISet<api.ISearchNode> explored;
            Queue<api.ISearchNode> frontier;
            api.ISearchNode start;

            explored = new HashSet<api.ISearchNode>();
            frontier = new Queue<api.ISearchNode>();
            start = problem.GetStartNode();
            frontier.Enqueue(start);
            if (problem.IsGoal(start))
            {
                return start.GetPathFromRoot();
            }
            while (problem.HasHitIterationLimit() == false)
            {
                api.ISearchNode node;

                if (frontier.Count == 0)
                {
                    return null;
                }
                node = frontier.Dequeue();
                explored.Add(node);
                foreach (api.ISearchNode newNode in problem.ExpandNode(node))
                {
                    if (problem.IsGoal(newNode))
                    {
                        return newNode.GetPathFromRoot();
                    }
                    else if (explored.Contains(newNode) == false && frontier.Contains(newNode) == false)
                    {
                        frontier.Enqueue(newNode);
                    }
                }
            }
            return null;
        }

    }

}
