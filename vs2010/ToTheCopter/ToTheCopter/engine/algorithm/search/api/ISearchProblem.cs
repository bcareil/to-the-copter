﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ToTheCopter.engine.algorithm.search.api
{
    interface ISearchProblem
    {
        bool IsGoal(ISearchNode node);
        ICollection<ISearchNode> ExpandNode(ISearchNode node);
        float NodeHeuristic(ISearchNode node);
        bool HasHitIterationLimit();
        ISearchNode GetStartNode();
    }
}
