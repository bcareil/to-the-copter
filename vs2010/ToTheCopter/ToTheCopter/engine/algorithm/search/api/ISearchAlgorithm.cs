﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ToTheCopter.engine.algorithm.search.api
{
    interface ISearchAlgorithm
    {
        ICollection<api.ISearchNode> ComputePath(api.ISearchProblem problem);
    }
}
