﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ToTheCopter.engine.algorithm.search.api
{
    interface ISearchNode
    {
        int Depth { get; }
        ICollection<ISearchNode> GetPathFromRoot();
    }
}
