﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ToTheCopter.engine.utils;

namespace ToTheCopter.engine.algorithm.search
{
    class AStarSearch : api.ISearchAlgorithm
    {
        public ICollection<api.ISearchNode> ComputePath(api.ISearchProblem problem)
        {
            ISet<api.ISearchNode> explored;
            PriorityQueue<float, api.ISearchNode> frontier;
            api.ISearchNode start; 

            explored = new HashSet<api.ISearchNode>();
            frontier = new PriorityQueue<float, api.ISearchNode>(Comparer<float>.Default);
            start = problem.GetStartNode();
            frontier.Add(problem.NodeHeuristic(start), start);
            if (problem.IsGoal(start))
            {
                return start.GetPathFromRoot();
            }
            while (true)
            {
                api.ISearchNode node;

                if (frontier.Count == 0)
                {
                    return null;
                }
                node = frontier.Pop();
                explored.Add(node);
                foreach (api.ISearchNode newNode in problem.ExpandNode(node))
                {
                    if (problem.IsGoal(newNode))
                    {
                        return newNode.GetPathFromRoot();
                    }
                    else if (explored.Contains(newNode) == false && frontier.Contains(newNode) == false)
                    {
                        frontier.Add(problem.NodeHeuristic(newNode), newNode);
                    }
                }
            }
        }

    }

}
