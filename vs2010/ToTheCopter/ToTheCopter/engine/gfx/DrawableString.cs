﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace ToTheCopter.engine.gfx
{
    public class DrawableString : api.IDrawable
    {

        /*
         * private members
         */

        private String _text;
        private Vector2 _textDimensions;
        private Vector2 _position;

        /*
         * public constructor
         */

        public DrawableString(SpriteFont font, String text, Vector2 center, Color color)
        {
            this.Font = font;
            this.Text = text;
            this.Center = center;
            this.Color = color;
        }

        /*
         * public properties
         */

        public SpriteFont Font { get; set; }

        public String Text
        {
            get { return _text; }
            set
            {
                Vector2 center;

                center = Center;
                _text = value;
                _textDimensions = Font.MeasureString(_text);
                Center = center;
            }
        }

        public Vector2 Center
        {
            get { return _position + _textDimensions / 2f; }
            set { _position = value - _textDimensions / 2f; }
        }

        public Color Color { get; set; }

        /*
         * public methods
         */

        public void Draw(SpriteBatch sb)
        {
            sb.DrawString(this.Font, this.Text, _position, this.Color);
        }
    }
}
