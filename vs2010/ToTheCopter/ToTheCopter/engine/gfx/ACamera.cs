﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using ToTheCopter.engine.utils;

namespace ToTheCopter.engine.gfx
{
    abstract class ACamera : api.ICamera
    {
        protected Vector2 _screenSize;
        protected Vector2 _translation;
        protected Vector2 _zoom;
        protected Matrix _transformMatrix;

        public ACamera(Vector2 screenSize, Vector2 sceneCenter, Vector2 zoom)
        {
            _screenSize = screenSize;
            _translation = new Vector2(0, 0);
            _zoom = zoom;

            SceneCenter = sceneCenter;
            Effect = null;
        }

        /*
         * public properties
         */

        /// <summary>
        /// The coordinates of the center of the screen
        /// </summary>
        public Vector2 ScreenCenter { get { return _screenSize / 2; } }

        /// <summary>
        /// The dimensions of the screen
        /// </summary>
        public Vector2 ScreenSize { get { return _screenSize; } }

        /// <summary>
        /// The point on wich the camera is centred inside the scene
        /// </summary>
        public Vector2 SceneCenter
        {
            get { return _screenSize / (2 * _zoom) + _translation; }
            set { _translation = value - _screenSize / (2 * _zoom); UpdateTransformationMatrix(); }
        }

        /// <summary>
        /// The redered dimensions of the scene
        /// </summary>
        public Vector2 SceneSize
        {
            get { return _screenSize / _zoom; }
            set { _zoom.X = _screenSize.X / value.X; _zoom.Y = _screenSize.Y / value.Y; UpdateTransformationMatrix(); }
        }

        /// <summary>
        /// The redered area of the scene
        /// </summary>
        public Rectf SceneArea
        {
            get { return new Rectf(-_translation, SceneSize); }
        }

        /// <summary>
        /// The position of the upper left corner of the camera inside the scene
        /// </summary>
        public Vector2 Position
        {
            get { return -_translation; }
            set { _translation = -value; UpdateTransformationMatrix(); }
        }

        /// <summary>
        /// The translation vector between an object in the scene and its coordinates on the screen
        /// </summary>
        public Vector2 Translation
        {
            get { return _translation; }
            set { _translation = value; UpdateTransformationMatrix(); }
        }

        /// <summary>
        /// The zoom factor (on x an y axis)
        /// </summary>
        public Vector2 Zoom
        {
            get { return _zoom; }
            set { _zoom = value; UpdateTransformationMatrix(); }
        }

        public Effect Effect { get; set; }

        /*
         * public methods
         */

        public abstract void Begin(SpriteBatch sb);

        public abstract void End(SpriteBatch sb);

        public void Move(Vector2 movement)
        {
            Translation -= movement;
        }

        public void SetZoom(float newZoom)
        {
            Zoom = new Vector2(newZoom, newZoom);
        }

        /*
         * private methods
         */

        private void UpdateTransformationMatrix()
        {
            _transformMatrix =
                Matrix.Identity
                * Matrix.CreateScale(_zoom.X, _zoom.Y, 1)
                * Matrix.CreateTranslation(_translation.X, _translation.Y, 0)
                ;
        }

    }
}
