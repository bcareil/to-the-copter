﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using ToTheCopter.engine.utils;

namespace ToTheCopter.engine.gfx
{
    class Camera : ACamera
    {

        public Camera(Vector2 screenSize, Vector2 sceneCenter, Vector2 zoom)
            : base(screenSize, sceneCenter, zoom)
        {
        }

        public override void Begin(SpriteBatch sb)
        {
            sb.Begin(
                SpriteSortMode.Deferred,
                BlendState.AlphaBlend,
                SamplerState.LinearClamp,
                DepthStencilState.None,
                RasterizerState.CullCounterClockwise,
                Effect,
                _transformMatrix
                );
        }

        public override void End(SpriteBatch sb)
        {
            sb.End();
        }

    }
}
