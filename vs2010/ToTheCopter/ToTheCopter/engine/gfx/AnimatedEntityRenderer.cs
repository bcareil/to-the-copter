﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace ToTheCopter.engine.gfx
{
    class AnimatedEntityRenderer : BasicEntityRenderer
    {

        Point _animationDimensions;
        Point _current;
        float _stepDelay;
        float _nextStepDelay;

        public AnimatedEntityRenderer(
            Texture2D spriteSheet,
            Point animationDimensions,
            Point firstSpriteCoordinates,
            Point spriteDimensions,
            Vector2 spriteCenter,
            Vector2 direction,
            Vector2 center,
            Color overrideColor,
            float executionDelay
            ) : base(spriteSheet, firstSpriteCoordinates, spriteDimensions, spriteCenter, direction, center, overrideColor)
        {
            _animationDimensions = animationDimensions;
            _current = new Point(0, 0);
            _stepDelay = executionDelay / (_animationDimensions.X * _animationDimensions.Y);
            _nextStepDelay = _stepDelay;
        }

        public AnimatedEntityRenderer(AnimatedEntityRenderer oth)
            : base(oth)
        {
            _animationDimensions = oth._animationDimensions;
            _current = oth._current;
            _stepDelay = oth._stepDelay;
            _nextStepDelay = oth._nextStepDelay;
        }

        public override void Update(float elapsedTime)
        {
            _nextStepDelay -= elapsedTime;
            if (_nextStepDelay <= 0f)
            {
                _nextStepDelay += _stepDelay;
                _current.X += 1;
                if (_current.X >= _animationDimensions.X)
                {
                    _current.X = 0;
                    _current.Y += 1;
                    if (_current.Y >= _animationDimensions.Y)
                    {
                        _current.Y = _animationDimensions.Y - 1;
                    }
                }
                SpriteCoordinates = new Point(SpriteDimensionsSource.X * _current.X, SpriteDimensionsSource.Y * _current.Y);
            }
        }

        public override api.IEntityRenderer Clone()
        {
            return new AnimatedEntityRenderer(this);
        }

    }
}
