﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using ToTheCopter.engine.utils;

namespace ToTheCopter.engine.gfx
{
    class BufferedCamera : ACamera
    {

        private LinkedList<Effect> _postEffects;
        private api.IEffectBuffer _sceneBuffer;
        private api.IEffectBuffer _effectBuffer;
        private api.ICamera _effectCamera;
        private Viewport _savedViewport;

        public BufferedCamera(Vector2 screenSize, Vector2 sceneCenter, Vector2 zoom)
            : base(screenSize, sceneCenter, zoom)
        {
            _postEffects = new LinkedList<Effect>();
            _sceneBuffer = new EffectBuffer(
                MyGame.GetInstance().GraphicsDevice,
                (int)screenSize.X,
                (int)screenSize.Y
                );
            _effectBuffer = new EffectBuffer(
                MyGame.GetInstance().GraphicsDevice,
                (int)screenSize.X,
                (int)screenSize.Y
                );
            _effectCamera = new Camera(
                screenSize,
                new Vector2(screenSize.X / 2, screenSize.Y / 2),
                new Vector2(1, 1)
                );
        }

        public override void Begin(SpriteBatch sb)
        {
            _savedViewport = MyGame.GetInstance().Graphics.GraphicsDevice.Viewport;
            _sceneBuffer.Activate();
            _sceneBuffer.Clear();
            sb.Begin(
                SpriteSortMode.Deferred,
                BlendState.AlphaBlend,
                SamplerState.PointClamp,
                DepthStencilState.None,
                RasterizerState.CullCounterClockwise,
                Effect,
                _transformMatrix
                );
        }

        public override void End(SpriteBatch sb)
        {
            // end drawings
            sb.End();
            _sceneBuffer.Deactivate();
            // apply post effects
            foreach (Effect effect in _postEffects)
            {
                api.IEffectBuffer tmp;

                // draw the scene with the desired effect
                _effectBuffer.Activate();
                _effectCamera.Effect = effect;
                _effectCamera.Begin(sb);
                sb.Draw(_sceneBuffer.Texture, new Vector2(0, 0), Color.White);
                _effectCamera.End(sb);
                _effectBuffer.Deactivate();
                // swap buffers
                tmp = _sceneBuffer;
                _sceneBuffer = _effectBuffer;
                _effectBuffer = tmp;
            }
            // draw the scene on the screen
            MyGame.GetInstance().Graphics.GraphicsDevice.Viewport = _savedViewport;
            _effectCamera.Effect = null;
            _effectCamera.Begin(sb);
            sb.Draw(_sceneBuffer.Texture, new Rectangle(0, 0, _savedViewport.Width, _savedViewport.Height), Color.White);
            _effectCamera.End(sb);
        }

        public void AddPostEffect(Effect effect)
        {
            _postEffects.AddLast(effect);
        }

        public void ClearPostEffects()
        {
            _postEffects.Clear();
        }

    }
}
