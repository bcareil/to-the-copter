﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;

namespace ToTheCopter.engine.gfx
{
    class EffectBuffer : api.IEffectBuffer
    {

        private GraphicsDevice _graphicsDevice;
        private RenderTarget2D _buffer;

        public EffectBuffer(GraphicsDevice graphicsDevice, int width, int height)
        {
            _graphicsDevice = graphicsDevice;
            _buffer = new RenderTarget2D(
                graphicsDevice,
                width,
                height
                );
        }

        public Texture2D Texture { get { return _buffer; } }

        public void Activate()
        {
            _graphicsDevice.SetRenderTarget(_buffer);
        }

        public void Deactivate()
        {
            _graphicsDevice.SetRenderTarget(null);
        }

        public void Draw(SpriteBatch sb)
        {
            sb.Draw(_buffer, new Microsoft.Xna.Framework.Vector2(0, 0), Microsoft.Xna.Framework.Color.White);
        }

        public void Clear()
        {
            _graphicsDevice.Clear(Microsoft.Xna.Framework.Color.Black);
        }
    }
}
