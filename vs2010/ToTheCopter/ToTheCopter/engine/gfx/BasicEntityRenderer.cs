﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using ToTheCopter.engine.utils;

namespace ToTheCopter.engine.gfx
{
    public class BasicEntityRenderer : engine.api.IEntityRenderer
    {
        private Texture2D _spriteSheet;
        private Point _spriteCoordinates;
        private Point _spriteDimensionsSource;
        private Point _spriteDimensionsDestination;
        private Vector2 _zoom;
        private Vector2 _spriteCenter;
        private Vector2 _center;
        private Color _color;
        private Rectangle _spriteSource;
        private Rectangle _spriteDestination;
        private float _spriteRotation;

        public BasicEntityRenderer(
            Texture2D spriteSheet,
            Point spriteCoordinates,
            Point spriteDimensions,
            Vector2 spriteCenter,
            Vector2 direction,
            Vector2 center,
            Color overrideColor
            )
        {
            _spriteSheet = spriteSheet;
            _spriteCoordinates = spriteCoordinates;
            _spriteDimensionsSource = spriteDimensions;
            _spriteDimensionsDestination = spriteDimensions;
            _zoom = new Vector2(1, 1);
            _spriteCenter = spriteCenter;
            _center = center;
            _color = overrideColor;
            _spriteSource = new Rectangle();
            _spriteDestination = new Rectangle();
            _spriteRotation = 0f;
            Direction = direction;
            ComputeSource();
            ComputeDestination();
        }

        public BasicEntityRenderer(BasicEntityRenderer oth)
        {
            _spriteSheet = oth._spriteSheet;
            _spriteCoordinates = oth._spriteCoordinates;
            _spriteDimensionsSource = oth._spriteDimensionsSource;
            _spriteDimensionsDestination = oth._spriteDimensionsDestination;
            _zoom = oth._zoom;
            _spriteCenter = oth._spriteCenter;
            _center = oth._center;
            _color = oth._color;
            _spriteSource = oth._spriteSource;
            _spriteDestination = oth._spriteDestination;
            _spriteRotation = oth._spriteRotation;
        }

        public Texture2D SpriteSheet
        {
            get { return _spriteSheet; }
            set { _spriteSheet = value; }
        }

        public Point SpriteCoordinates
        {
            get { return _spriteCoordinates; }
            set { _spriteCoordinates = value; ComputeSource(); }
        }

        public Point SpriteDimensions
        {
            get { return _spriteDimensionsSource; }
            set { SpriteDimensionsSource = value; SpriteDimensionsDestination = value; }
        }

        public Point SpriteDimensionsSource
        {
            get { return _spriteDimensionsSource; }
            set
            {
                Vector2 transform;

                transform = new Vector2(value.X, value.Y) / new Vector2(_spriteDimensionsSource.X, _spriteDimensionsSource.Y);
                _spriteDimensionsSource = value;
                _spriteCenter *= transform;
                ComputeSource();
            }
        }

        public Point SpriteDimensionsDestination
        {
            get { return _spriteDimensionsDestination; }
            set { _spriteDimensionsDestination = value; UpdateZoom(); ComputeDestination(); }
        }

        public Vector2 Zoom
        {
            get { return _zoom; }
            set { _zoom = value; UpdateSpriteDimensionsDestination(); ComputeDestination(); }
        }

        public Vector2 SpriteCenter
        {
            get { return _spriteCenter; }
            set { _spriteCenter = value; }
        }

        public Vector2 Direction
        {
            get { return new Vector2((float)Math.Cos(_spriteRotation), (float)Math.Sin(_spriteRotation)); }
            set { _spriteRotation = (float)Math.Atan2(value.Y, value.X); }
        }

        public float Angle { get { return _spriteRotation; } set { _spriteRotation = value; } }

        public Vector2 Center
        {
            get { return _center; }
            set { _center = value; ComputeDestination(); }
        }

        public Rectf DestinationBounds
        {
            get { return new Rectf(_spriteDestination.X, _spriteDestination.Y, _spriteDestination.Width, _spriteDestination.Height); }
            set
            {
                UpdateSpriteDestination(value);
            }
        }

        private void UpdateSpriteDestination(Rectf value)
        {
            _spriteDestination.X = (int)value.X;
            _spriteDestination.Y = (int)value.Y;
            _spriteDestination.Width = (int)(value.Right - _spriteDestination.X);
            _spriteDestination.Height = (int)(value.Bottom - _spriteDestination.Y);
            _center = new Vector2(_spriteDestination.Center.X, _spriteDestination.Center.Y);
            _spriteDimensionsDestination = new Point(_spriteDestination.Width, _spriteDestination.Height);
            UpdateZoom();
        }

        public Color OverrideColor
        {
            get { return _color; }
            set { _color = value; }
        }

        /*
         */

        public virtual void Update(float elapsedTime)
        {
            // nothing to do
        }

        public virtual api.IEntityRenderer Clone()
        {
            return new BasicEntityRenderer(this);
        }

        public void Draw(SpriteBatch sb)
        {
            sb.Draw(
                _spriteSheet,
                _spriteDestination,
                _spriteSource,
                _color,
                _spriteRotation,
                _spriteCenter,
                SpriteEffects.None,
                0f
                );
        }

        /*
         */

        private void ComputeSource()
        {
            _spriteSource = new Rectangle(
                _spriteCoordinates.X,
                _spriteCoordinates.Y,
                _spriteDimensionsSource.X,
                _spriteDimensionsSource.Y
                );
        }

        private void ComputeDestination()
        {
            _spriteDestination = new Rectangle(
                (int)(_center.X /* o.O - _spriteDimensions.X / 2 */),
                (int)(_center.Y /* o.O - _spriteDimensions.Y / 2 */),
                _spriteDimensionsDestination.X,
                _spriteDimensionsDestination.Y
                );
        }

        private void UpdateZoom()
        {
            if (_spriteDimensionsSource.X != 0)
                _zoom.X = _spriteDimensionsDestination.X / _spriteDimensionsSource.X;
            if (_spriteDimensionsSource.Y != 0)
                _zoom.Y = _spriteDimensionsDestination.Y / _spriteDimensionsSource.Y;
        }

        private void UpdateSpriteDimensionsDestination()
        {
            _spriteDimensionsDestination.X = (int)(_zoom.X * _spriteDimensionsSource.X);
            _spriteDimensionsDestination.Y = (int)(_zoom.Y * _spriteDimensionsSource.Y);
        }

    }
}
