﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;

namespace ToTheCopter.engine.api
{
    public interface IEffectBuffer : api.IDrawable
    {
        Texture2D Texture { get; }

        void Activate();
        void Deactivate();
        void Clear();
    }
}
