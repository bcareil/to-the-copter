﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace ToTheCopter.engine.api
{
    public interface IMoveable
    {
        Vector2 MovingVector { get; set; }
        Vector2 LookingDirection { get; set; }
    }
}
