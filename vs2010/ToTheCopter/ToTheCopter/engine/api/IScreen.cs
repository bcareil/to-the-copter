﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;

namespace ToTheCopter.engine.api
{
    public interface IScreen
    {
        void Initialize(ContentManager content, int screenWidth, int screenHeight);
        void Draw(SpriteBatch sb);
        void Update(float elapsed, float totalElapsed);
        void TearDown();
        bool IsOpened();
        bool IsBlockingUpdate();
        bool IsBlockingDraw();
    }
}
