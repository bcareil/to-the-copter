﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ToTheCopter.engine.api
{
    public interface IUpdatable
    {
        void Update(float elapsed, float totalElapsed);
    }
}
