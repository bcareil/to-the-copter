﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ToTheCopter.engine.api
{
    interface IScreenStack
    {
        void Push(IScreen screen);
        void Pop();
    }
}
