﻿using System;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using ToTheCopter.engine.utils;
namespace ToTheCopter.engine.api
{
    interface ICamera
    {

        Effect Effect { get; set; }

        Vector2 Position { get; set; }
        Rectf SceneArea { get; }
        Vector2 SceneCenter { get; set; }
        Vector2 SceneSize { get; set; }
        Vector2 ScreenCenter { get; }
        Vector2 ScreenSize { get; }
        Vector2 Translation { get; set; }
        Vector2 Zoom { get; set; }

        void Begin(SpriteBatch sb);
        void End(SpriteBatch sb);

        void SetZoom(float newZoom);
        void Move(Vector2 movement);
    }
}
