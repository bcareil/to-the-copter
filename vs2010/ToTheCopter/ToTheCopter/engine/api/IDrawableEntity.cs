﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace ToTheCopter.engine.api
{
    interface IDrawableEntity
    {
        void Draw(SpriteBatch sb, Vector2 translation);
    }
}
