﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ToTheCopter.engine.utils;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace ToTheCopter.engine.api
{
    public interface IEntityRenderer : api.IDrawable
    {
        /// <summary>
        /// The texture
        /// </summary>
        Texture2D SpriteSheet { get; set; }
        
        /// <summary>
        /// The coordinate of the sprite inside the texture
        /// </summary>
        Point SpriteCoordinates { get; set; }

        /// <summary>
        /// Aggregation of SpriteDimensionsSource and SpriteDimensionsDestination.
        /// Setting one set both, Getting the property return the SpriteDimensionsSource
        /// </summary>
        Point SpriteDimensions { get; set; }
        
        /// <summary>
        /// The dimensions of the sprite in the texture
        /// </summary>
        Point SpriteDimensionsSource { get; set; }

        /// <summary>
        /// The display dimensions of the sprite
        /// </summary>
        Point SpriteDimensionsDestination { get; set; }

        /// <summary>
        /// The zoom vector from the SpriteDimensionSource to the
        /// SpriteDimensionDestination
        /// </summary>
        Vector2 Zoom { get; set; }

        /// <summary>
        /// The center of the sprite, for matrix transformation
        /// </summary>
        Vector2 SpriteCenter { get; set; }

        /// <summary>
        /// Used to compute the angle of the rotation of the sprite
        /// </summary>
        Vector2 Direction { get; set; }

        /// <summary>
        /// The angle of the rotation, in radian.
        /// </summary>
        float Angle { get; set; }

        /// <summary>
        /// The center of the sprite at its destination
        /// </summary>
        Vector2 Center { get; set; }

        /// <summary>
        /// Destination rectangle of the sprite
        /// </summary>
        Rectf DestinationBounds { get; set; }

        /// <summary>
        /// The color passed to the Draw method of a SpriteBatch
        /// </summary>
        Color OverrideColor { get; set; }

        void Update(float elapsedTime);

        IEntityRenderer Clone();

    }
}
