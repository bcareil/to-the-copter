﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using ToTheCopter.map.entities;
using ToTheCopter.engine.utils;

namespace ToTheCopter.map
{
    public class MapGenerator
    {
        /*
         * internal types
         */

        enum SpriteSheetType
        {
            ROADS,
            BUILDINGS,

            COUNT
        }

        /*
         * private attributes
         */

        private Map _product;
        private Texture2D[][] _spriteSheets;
        private Texture2D _fallbackTexture;
        private ICollection<areas.IDeserializableArea> _areaDeserializers;


        /*
         * public Constructors
         */

        public MapGenerator(ContentManager content)
        {
            LoadSpriteSheets(content);
            LoadAreaDeserializers();
        }

        /*
         * public methods
         */

        public void LoadFromBmpFile(String filename)
        {
            Bitmap bmp = new Bitmap(filename);

            _product = new Map(bmp.Width, bmp.Height);
            for (int x = 0; x < bmp.Width; ++x)
            {
                for (int y = 0; y < bmp.Height; ++y)
                {
                    InterpretPixel(x, y, bmp.GetPixel(x, y).ToArgb());
                }
            }
        }


        public Map GetMap()
        {
            return _product;
        }

        /*
         * private methods used by the constructor
         */

        private void LoadSpriteSheets(ContentManager content)
        {
            // initialize member
            _spriteSheets = new Texture2D[(int)SpriteSheetType.COUNT][];
            // initialize buildinds
            _spriteSheets[(int)SpriteSheetType.BUILDINGS] = new Texture2D[1];
            _spriteSheets[(int)SpriteSheetType.BUILDINGS][0] = content.Load<Texture2D>("sprites/map/building-0");
            // initialize roads
            _spriteSheets[(int)SpriteSheetType.ROADS] = new Texture2D[] {
                content.Load<Texture2D>("sprites/map/road-0"),
                content.Load<Texture2D>("sprites/map/sidewalk-0"),
                content.Load<Texture2D>("sprites/map/road-tut-0"),
                content.Load<Texture2D>("sprites/map/landing-area-0")
                };
            // initialize fallback texture
            _fallbackTexture = new Texture2D(MyGame.GetInstance().GraphicsDevice, 2, 2);
            _fallbackTexture.SetData(new uint[] {
                    0xffffff00, 0xffff00ff,
                    0xffff00ff, 0xffffff00
                });
        }

        private void LoadAreaDeserializers()
        {
            _areaDeserializers = new List<areas.IDeserializableArea>(
                new areas.IDeserializableArea[] {
                    new areas.MapAreaPlayerSpawnDeserializer(),
                    new areas.MapAreaEnemySpawnDeserializer(),
                    new areas.MapAreaCopterSpawnDeserializer()
                });
        }

        /*
         * other private methods
         */

        private Microsoft.Xna.Framework.Rectangle GetSpriteSheetCoordinatesForColor(int color)
        {
            int idx;
            
            idx = color & 0x0f;
            return new Microsoft.Xna.Framework.Rectangle(
                MapConstants.TILE_WIDTH * (idx % MapConstants.NB_TILE_ON_X),
                MapConstants.TILE_HEIGHT * (idx / MapConstants.NB_TILE_ON_Y),
                MapConstants.TILE_WIDTH,
                MapConstants.TILE_HEIGHT
                );
        }

        private Texture2D GetSpriteSheetForColor(SpriteSheetType type, int color)
        {
            uint spriteSheet;

            spriteSheet = (uint)(((color >> (4 + (8 * (int)type)) & 0x0f) - 1));
            if (spriteSheet >= _spriteSheets[(int)type].Length)
            {
                return null;
            }
            return _spriteSheets[(int)type][spriteSheet];
        }

        private void InterpretPixel(int x, int y, int color)
        {
            if ((color & 0xff000000) != 0xff000000)
            {
                BuildPickUp(x, y, color);
            }
            if ((color & 0x00ff0000) != 0)
            {
                BuildArea(x, y, color);
            }
            if ((color & 0x0000ff00) != 0)
            {
                BuildBuilding(x, y, color);
            }
            if ((color & 0x000000ff) != 0)
            {
                BuildRoad(x, y, color);
            }
        }

        private void BuildPickUp(int x, int y, int color)
        {
            int type;
            int subType;

            type = ((color >> 28) & 0x0f) - 1;
            subType = (color >> 24) & 0x0f;
            _product.AddPickUp(new pickup.MapPickUp(
                new Rectf(
                    x * MapConstants.TILE_WIDTH,
                    y * MapConstants.TILE_HEIGHT,
                    MapConstants.TILE_WIDTH,
                    MapConstants.TILE_HEIGHT
                    ),
                type,
                subType
                ));
        }

        private void BuildArea(int x, int y, int color)
        {
            color = (color >> 16) & 0xff;
            foreach (areas.IDeserializableArea deserializer in _areaDeserializers)
            {
                if (deserializer.CanDeserialize(color))
                {
                    deserializer.Deserialize(_product, x, y, color);
                }
            }
        }

        private void BuildBuilding(int x, int y, int color)
        {
            Tile tile;

            tile = BuildTile(SpriteSheetType.BUILDINGS, x, y, color);
            BuildWall(x, y, (color >> (8 * (int)SpriteSheetType.BUILDINGS)) & 0x0f, tile);
        }

        private void BuildRoad(int x, int y, int color)
        {
            BuildTile(SpriteSheetType.ROADS, x, y, color);
        }

        private Tile BuildTile(SpriteSheetType type, int x, int y, int color)
        {
            Tile tile;
            Texture2D texture;

            texture = GetSpriteSheetForColor(type, color);
            if (texture == null)
            {
                texture = _fallbackTexture;
            }
            color = color >> (8 * ((int)type));
            tile = new Tile(
                    texture,
                    GetSpriteSheetCoordinatesForColor(color),
                    new Microsoft.Xna.Framework.Rectangle(
                        x * MapConstants.TILE_WIDTH,
                        y * MapConstants.TILE_HEIGHT,
                        MapConstants.TILE_WIDTH,
                        MapConstants.TILE_HEIGHT
                ));
            _product.SetTile(x, y, tile);
            return tile;
        }

        private void BuildWall(int x, int y, int wallType, Tile tile)
        {
            MapWall wall;
            engine.gfx.BasicEntityRenderer renderer;
            Microsoft.Xna.Framework.Point[] translations = new Microsoft.Xna.Framework.Point[] {
                new Microsoft.Xna.Framework.Point(0, MapConstants.TILE_HEIGHT / 2),
                new Microsoft.Xna.Framework.Point(0, 0),
                new Microsoft.Xna.Framework.Point(0, 0),
                new Microsoft.Xna.Framework.Point(MapConstants.TILE_WIDTH / 2, 0),
            };
            Microsoft.Xna.Framework.Point[] resize = new Microsoft.Xna.Framework.Point[] {
                new Microsoft.Xna.Framework.Point(0, MapConstants.TILE_HEIGHT / 2),
                new Microsoft.Xna.Framework.Point(MapConstants.TILE_WIDTH / 2, 0),
                new Microsoft.Xna.Framework.Point(0, MapConstants.TILE_HEIGHT / 2),
                new Microsoft.Xna.Framework.Point(MapConstants.TILE_WIDTH / 2, 0),
            };

            renderer = tile.GetRendererCopy();
            if (wallType < 4)
            {
                // resize the renderer source and destination so it display only the wall
                renderer.SpriteCoordinates = new Microsoft.Xna.Framework.Point(
                    renderer.SpriteCoordinates.X + translations[wallType].X,
                    renderer.SpriteCoordinates.Y + translations[wallType].Y
                    );
                renderer.SpriteDimensions = new Microsoft.Xna.Framework.Point(
                    renderer.SpriteDimensions.X - resize[wallType].X,
                    renderer.SpriteDimensions.Y - resize[wallType].Y
                    );
                renderer.Center = new Microsoft.Xna.Framework.Vector2(
                    renderer.Center.X + translations[wallType].X - resize[wallType].X / 2f,
                    renderer.Center.Y + translations[wallType].Y - resize[wallType].Y / 2f
                    );
            }
            // create the wall
            wall = new MapWall(
                renderer,
                new engine.utils.Rectf(
                    renderer.Center.X - renderer.SpriteDimensions.X / 2f,
                    renderer.Center.Y - renderer.SpriteDimensions.Y / 2f,
                    renderer.SpriteDimensions.X,
                    renderer.SpriteDimensions.Y
                ));
            _product.SetWall(x, y, wall);
        }
    }
}
