﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace ToTheCopter.map
{
    class MapRenderer : engine.api.IDrawableEntity
    {
        private Point _dimensions;
        private Map _map;

        public MapRenderer(Point dimensions, Map map)
        {
            _dimensions = new Point(
                dimensions.X / MapConstants.TILE_WIDTH,
                dimensions.Y / MapConstants.TILE_HEIGHT
                );
            _map = map;
        }

        public void Draw(SpriteBatch sb, Vector2 translation)
        {
            Point startCoords = new Point(-(int)translation.X, -(int)translation.Y);
            int maxX, maxY;

            startCoords.X = Math.Max(0, startCoords.X / MapConstants.TILE_WIDTH);
            startCoords.Y = Math.Max(0, startCoords.Y / MapConstants.TILE_HEIGHT - 2);
            maxX = Math.Min(_map.Widht, startCoords.X + _dimensions.X + 2);
            maxY = Math.Min(_map.Height, startCoords.Y + _dimensions.Y + 4);
            for (int x = startCoords.X; x < maxX; ++x)
            {
                for (int y = startCoords.Y; y < maxY; ++y)
                {
                    _map.GetTile(x, y).Draw(sb);
                }
            }
        }
    }
}
