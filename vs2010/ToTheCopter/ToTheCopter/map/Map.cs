﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using ToTheCopter.map.entities;

namespace ToTheCopter.map
{
    public class Map
    {
        private int _width;
        private int _height;
        private Tile[] _tiles;
        private Point[] _playersStartPosition;
        private MapWall[] _walls;
        private ICollection<MapAreaEnemySpawn> _enemySpawnArea;
        private ICollection<pickup.MapPickUp> _pickUp;
        private Rectangle _copterArea;

        public int Widht { get { return _width; } }
        public int Height { get { return _height; } }

        public Map(int width, int height)
        {
            _width = width;
            _height = height;
            _tiles = new Tile[_width * _height];
            _walls = new MapWall[_width * _height];
            _playersStartPosition = new Point[MyGame.NB_MAX_PLAYERS];
            _enemySpawnArea = new LinkedList<MapAreaEnemySpawn>();
            _pickUp = new LinkedList<pickup.MapPickUp>();
        }

        /*
         * public accessors
         */

        public Tile GetTile(int x, int y)
        {
            return _tiles[x + y * _width];
        }

        public IEnumerable<Tile> GetTiles()
        {
            return _tiles;
        }

        internal void SetTile(int x, int y, Tile tile)
        {
            _tiles[x + y * _width] = tile;
        }

        public MapWall GetWall(int x, int y)
        {
            return _walls[x + y * _width];
        }

        public IEnumerable<MapWall> GetWalls()
        {
            return _walls;
        }

        public void SetWall(int x, int y, MapWall wall)
        {
            _walls[x + y * _width] = wall;
        }

        public Point GetPlayerStartPosition(int playerNo)
        {
            return _playersStartPosition[playerNo];
        }

        public void SetPlayerStartPosition(int playerNo, Point pos)
        {
            _playersStartPosition[playerNo] = pos;
        }

        public IEnumerable<MapAreaEnemySpawn> GetEnemySpawnAreas()
        {
            return _enemySpawnArea;
        }

        internal void AddEnemySpawnArea(MapAreaEnemySpawn area)
        {
            _enemySpawnArea.Add(area);
        }

        public IEnumerable<pickup.MapPickUp> GetPickUp()
        {
            return _pickUp;
        }

        internal void AddPickUp(pickup.MapPickUp pickUp)
        {
            _pickUp.Add(pickUp);
        }

        public Rectangle GetCopterArea()
        {
            return _copterArea;
        }

        internal void SetCopterArea(Rectangle copterArea)
        {
            _copterArea = copterArea;
        }
    }
}
