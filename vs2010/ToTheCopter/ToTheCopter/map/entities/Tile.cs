﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace ToTheCopter.map.entities
{
    public class Tile : engine.api.IDrawable
    {
        private engine.gfx.BasicEntityRenderer _renderer;

        public Tile(Texture2D spriteSheet, Rectangle spriteSource, Rectangle spriteDestination)
        {
            _renderer = new engine.gfx.BasicEntityRenderer(
                spriteSheet,
                spriteSource.Location,
                new Point(spriteSource.Width, spriteSource.Height),
                new Vector2(spriteSource.Width / 2, spriteSource.Height / 2),
                new Vector2(1, 0),
                new Vector2(
                    spriteDestination.X + spriteDestination.Width / 2,
                    spriteDestination.Y + spriteDestination.Height / 2
                    ),
                Color.White
                );
        }

        public void Draw(SpriteBatch sb)
        {
            _renderer.Draw(sb);
        }

        public engine.gfx.BasicEntityRenderer GetRendererCopy()
        {
            return new engine.gfx.BasicEntityRenderer(_renderer);
        }
    }
}
