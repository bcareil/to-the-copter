﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ToTheCopter.engine.utils;

namespace ToTheCopter.map.entities
{
    public class MapWall
    {
        public engine.gfx.BasicEntityRenderer Renderer { get; set; }
        public Rectf CollideRect { get; set; }

        public MapWall(engine.gfx.BasicEntityRenderer renderer, Rectf collideRect)
        {
            Renderer = renderer;
            CollideRect = collideRect;
        }
    }
}
