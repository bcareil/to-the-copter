﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ToTheCopter.map.entities
{
    public class MapAreaEnemySpawn
    {
        public engine.utils.Rectf Area { get; private set; }
        public int AmountPer8Second { get; private set; }

        public MapAreaEnemySpawn(engine.utils.Rectf area, int amountPer8Second)
        {
            Area = area;
            AmountPer8Second = amountPer8Second;
        }

    }
}
