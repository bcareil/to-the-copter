﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ToTheCopter.map
{
    public static class MapConstants
    {
        public const int TILE_WIDTH = 128;
        public const int TILE_HEIGHT = 128;
        public const int NB_TILE_ON_X = 4;
        public const int NB_TILE_ON_Y = 4;
    }
}
