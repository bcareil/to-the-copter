﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using ToTheCopter.engine.utils;

namespace ToTheCopter.map.pickup
{
    public class MapPickUp
    {

        public MapPickUp(Rectf area, int type, int subType)
        {
            this.Area = new Rectf(
                area.X + area.Width / 4,
                area.Y + area.Height / 4,
                area.Width / 2,
                area.Height / 2
                );
            this.Type = type;
            this.SubType = subType;
        }

        public Rectf Area { get; private set; }
        public int Type { get; private set; }
        public int SubType { get; private set; }

    }
}
