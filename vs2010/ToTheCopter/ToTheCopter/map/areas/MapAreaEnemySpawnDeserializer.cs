﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ToTheCopter.map.areas
{
    class MapAreaEnemySpawnDeserializer : IDeserializableArea
    {
        public bool CanDeserialize(int color)
        {
            return (((color >> 4) & 0x0f) == 2);
        }

        public void Deserialize(Map map, int x, int y, int color)
        {
            map.AddEnemySpawnArea(new entities.MapAreaEnemySpawn(
                new engine.utils.Rectf(
                    x * MapConstants.TILE_WIDTH,
                    y * MapConstants.TILE_HEIGHT,
                    MapConstants.TILE_WIDTH,
                    MapConstants.TILE_HEIGHT
                    ),
                color & 0x0f
                ));
        }
    }
}
