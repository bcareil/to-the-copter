﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ToTheCopter.map.areas
{
    interface IDeserializableArea
    {
        bool CanDeserialize(int color);
        void Deserialize(Map map, int x, int y, int color);
    }
}
