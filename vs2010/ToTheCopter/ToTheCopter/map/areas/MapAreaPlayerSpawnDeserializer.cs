﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ToTheCopter.map.areas
{
    class MapAreaPlayerSpawnDeserializer : IDeserializableArea
    {
        public bool CanDeserialize(int color)
        {
            return (((color >> 4) & 0x0f) == 1);
        }

        public void Deserialize(Map map, int x, int y, int color)
        {
            int playerNo;

            playerNo = (color & 0x0f);
            map.SetPlayerStartPosition(
                playerNo,
                new Microsoft.Xna.Framework.Point(
                    x * MapConstants.TILE_WIDTH + MapConstants.TILE_WIDTH / 2,
                    y * MapConstants.TILE_HEIGHT + MapConstants.TILE_HEIGHT / 2
                    ));
        }
    }
}
