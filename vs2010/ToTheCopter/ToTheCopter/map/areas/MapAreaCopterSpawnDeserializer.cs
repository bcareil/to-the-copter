﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ToTheCopter.map.areas
{
    class MapAreaCopterSpawnDeserializer : IDeserializableArea
    {
        public bool CanDeserialize(int color)
        {
            return (((color >> 4) & 0x0f) == 3);
        }

        public void Deserialize(Map map, int x, int y, int color)
        {
            map.SetCopterArea(
                new Microsoft.Xna.Framework.Rectangle(
                    x * MapConstants.TILE_WIDTH,
                    y * MapConstants.TILE_HEIGHT,
                    MapConstants.TILE_WIDTH * 4,
                    MapConstants.TILE_HEIGHT * 4
                    ));
        }
    }
}
