﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using ToTheCopter.engine.utils;
using Microsoft.Xna.Framework.Audio;

namespace ToTheCopter.game.entities
{

    delegate void StateUpdate(float elapsed, float totalElapsed);

    class Copter : api.IAliveEntity
    {

        private const float BLADES_ROTATING_SPEED_LANDING = 26f;
        private const float BLADES_ROTATING_SPEED_IDLE = 12f;
        private const float BLADES_ROTATING_SPEED_TAKING_OF = 29f;

        private const float DELAY_LANDING = 5f;
        private const float DELAY_IDLE = 30f;
        private const float DELAY_TAKING_OF = 8f;

        private StateUpdate _stateUpdate;
        private engine.api.IEntityRenderer _copterRenderer;
        private engine.api.IEntityRenderer _copterBladesRenderer;
        private SoundEffect _soundLanding;
        private SoundEffect _soundTakeOff;
        private SoundEffect _soundIdle;
        private float _bladesSpeed;
        private float _altitude;
        private Timer _timer;
        private Rectf _loadingCollideRect;
        private api.IPlayer _loadedPlayer;

        public Copter(
            engine.api.IEntityRenderer copterRenderer,
            engine.api.IEntityRenderer copterBladesRenderer,
            SoundEffect soundLanding,
            SoundEffect soundTakeOff,
            SoundEffect soundIdle,
            Rectf collideRect
            )
        {
            _stateUpdate = this.UpdateLanding;
            _copterRenderer = copterRenderer;
            _copterBladesRenderer = copterBladesRenderer;
            _soundLanding = soundLanding;
            _soundTakeOff = soundTakeOff;
            _soundIdle = soundIdle;
            _bladesSpeed = 0f;
            _altitude = 0f;
            _timer = null;
            _loadingCollideRect = collideRect;
            _loadedPlayer = null;
            this.CollideRect = collideRect;

            _copterRenderer.Center = this.CollideRect.Center;
            _copterRenderer.OverrideColor = Color.Transparent;
            _copterBladesRenderer.Center = this.CollideRect.Center;
            _copterBladesRenderer.OverrideColor = Color.Transparent;
            _loadingCollideRect.X -= 5;
            _loadingCollideRect.Y -= 5;
            _loadingCollideRect.Width += 10;
            _loadingCollideRect.Height += 10;
        }

        public float MaxHP { get; set; }

        public float HP { get; set; }

        public bool IsAlive { get { return HP != 0; } }

        public engine.utils.Rectf CollideRect { get; set; }

        public api.ICollisionMediator CollisionMediator { get; set; }

        public engine.utils.OneDimensionalSegmentInt ZAxis
        {
            get { return new OneDimensionalSegmentInt((int)_altitude, (int)api.ZAxisValue.H10); }
            set { _altitude = value.X; }
        }

        public bool Remove { get; set; }

        public api.EntityType Type { get { return api.EntityType.COPTER; } }

        public Vector2 MovingVector { get { return new Vector2(); } set { } }

        public Vector2 LookingDirection { get { return MovingVector; } set { } }

        public void TakeDamage(float amount)
        {
            HP -= amount;
        }

        public void Collide(api.IGameEntity oth)
        {
            // should not happend
            throw new NotImplementedException();
        }

        public bool IsCollidingWith(api.IGameEntity oth)
        {
            // should not happend
            throw new NotImplementedException();
        }

        public void Update(float elapsed, float totalElapsed)
        {
            Game.GetInstance().AIMapInst.PropagateTrack(
                CollideRect.Center,
                1000f,
                0.75f
                );
            _stateUpdate(elapsed, totalElapsed);
            _copterBladesRenderer.Angle += _bladesSpeed * elapsed;
        }

        public void Draw(Microsoft.Xna.Framework.Graphics.SpriteBatch sb)
        {
            _copterRenderer.Draw(sb);
            _copterBladesRenderer.Draw(sb);
        }

        /*
         * private members
         */

        private void UpdateLanding(float elapsed, float totalElapsed)
        {
            if (_timer == null)
            {
                _timer = new Timer();
                _timer.Reset(DELAY_LANDING);
                _bladesSpeed = BLADES_ROTATING_SPEED_LANDING;
                _altitude = (int)api.ZAxisValue.H15;
                _copterRenderer.OverrideColor = Color.Transparent;
                _copterBladesRenderer.OverrideColor = Color.Transparent;
                Game.GetInstance().AIMapInst.VoidArea(this.CollideRect);
            }
            else if (_timer.Done)
            {
                _timer = null;
                _stateUpdate = this.UpdateIdle;
            }
            else
            {
                _timer.Update(elapsed);
                _altitude = (_timer.Delay * _timer.Delay) / (DELAY_LANDING * DELAY_LANDING) * (int)api.ZAxisValue.H15;
                _copterRenderer.Zoom = AltitudeToZoom();
                _copterBladesRenderer.Zoom = _copterRenderer.Zoom;
                _copterRenderer.OverrideColor = Color.White * AltitudeToAlpha();
                _copterBladesRenderer.OverrideColor = _copterRenderer.OverrideColor;
            }
        }

        private void UpdateIdle(float elapsed, float totalElapsed)
        {
            if (_timer == null)
            {
                _timer = new Timer();
                _timer.Reset(DELAY_IDLE);
                _altitude = 0f;
                _copterRenderer.Zoom = AltitudeToZoom();
                _copterBladesRenderer.Zoom = AltitudeToZoom();
            }
            else if (_timer.Done)
            {
                _timer = null;
                _stateUpdate = this.UpdateTakingOf;
            }
            else
            {
                _timer.Update(elapsed);
                if (_bladesSpeed != BLADES_ROTATING_SPEED_IDLE)
                {
                    _bladesSpeed =
                        (BLADES_ROTATING_SPEED_LANDING - BLADES_ROTATING_SPEED_IDLE)
                        * (_timer.Delay - (DELAY_IDLE - 3f)) / (DELAY_IDLE - 3f)
                        + BLADES_ROTATING_SPEED_IDLE;
                    if (_bladesSpeed < BLADES_ROTATING_SPEED_IDLE)
                    {
                        _bladesSpeed = BLADES_ROTATING_SPEED_IDLE;
                    }
                }
                // load player
                foreach (api.IGameEntity player in CollisionMediator.GetEntitiesInArea(_loadingCollideRect, new api.EntityType[] { api.EntityType.PLAYER }))
                {
                    _loadedPlayer = (api.IPlayer)player;
                    _loadedPlayer.Remove = true;
                    _timer = null;
                    _stateUpdate = this.UpdateTakingOf;
                    break;
                }
            }
        }

        private void UpdateTakingOf(float elapsed, float totalElapsed)
        {
            if (_timer == null)
            {
                _timer = new Timer();
                _timer.Reset(DELAY_TAKING_OF);
            }
            else if (_timer.Done)
            {
                Game.GetInstance().AIMapInst.UnvoidArea(this.CollideRect);
                Remove = true;
                if (_loadedPlayer != null)
                {
                    _loadedPlayer.Succeed();
                }
            }
            else
            {
                _timer.Update(elapsed);
                _altitude = (1f - (_timer.Delay * _timer.Delay) / (DELAY_TAKING_OF * DELAY_TAKING_OF)) * (int)api.ZAxisValue.H15;
                _copterRenderer.Zoom = AltitudeToZoom();
                _copterBladesRenderer.Zoom = _copterRenderer.Zoom;
                _copterRenderer.OverrideColor = Color.White * AltitudeToAlpha();
                _copterBladesRenderer.OverrideColor = _copterRenderer.OverrideColor;
                if (_bladesSpeed != BLADES_ROTATING_SPEED_TAKING_OF)
                {
                    _bladesSpeed =
                        (BLADES_ROTATING_SPEED_TAKING_OF - BLADES_ROTATING_SPEED_IDLE)
                        * (1f - (_timer.Delay - (DELAY_IDLE - 3f)) / (DELAY_IDLE - 3f))
                        + BLADES_ROTATING_SPEED_IDLE;
                }
            }
        }

        private float AltitudeToAlpha()
        {
            if (_altitude > (int)api.ZAxisValue.H7)
            {
                return 1f - ((_altitude - (int)api.ZAxisValue.H7) / ((int)api.ZAxisValue.H15 - (int)api.ZAxisValue.H7));
            }
            return 1f;
        }

        private Vector2 AltitudeToZoom()
        {
            return new Vector2(1.5f + _altitude / 50f);
        }

    }
}
