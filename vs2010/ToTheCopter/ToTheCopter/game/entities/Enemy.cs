﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using ToTheCopter.ai.api;
using ToTheCopter.game.ai.data;
using ToTheCopter.ai.factory;
using ToTheCopter.game.ai.factory;
using ToTheCopter.engine.utils;

namespace ToTheCopter.game.entities
{
    class Enemy : MovingEntity
    {
        private properties.EnemyProperties _properties;
        private IRootBehavior _ai;
        private IBehaviorGlobalData _aiData;
        private AIEnemyCommonData _commonAIData;
        private AIEnemySensorVector _sensors;

        public Enemy(properties.EnemyProperties enemyProperties, IRootBehavior ai, Point center, engine.api.IEntityRenderer renderer)
            : base(enemyProperties.EntityPropertiesInst, api.EntityType.ENEMY, center, renderer)
        {
            _properties = enemyProperties;
            _ai = ai;
            _sensors = new AIEnemySensorVector();
            _commonAIData = new AIEnemyCommonData(_sensors);
            _aiData = AINodeFactory.CreateBehaviorGlobalData(this, _commonAIData);

            _ai.Init(_aiData);
        }

        /*
         * public overriden method
         */

        public override void Collide(api.IGameEntity oth)
        {
            _sensors.Bump = true;
            if (oth.Type == api.EntityType.PLAYER)
            {
                // TODO: a real attack
                _sensors.BumpPlayer = true;
                ((api.IAliveEntity)oth).TakeDamage(_properties.AttackDamage * _sensors.ElapsedTime);
            }
            else if (oth.Type == api.EntityType.ENEMY)
            {
                Enemy enemy = (Enemy)oth;

                if (enemy._sensors.Target == null)
                {
                    enemy._sensors.Target = _sensors.Target;
                }
            }
        }

        public override void Draw(Microsoft.Xna.Framework.Graphics.SpriteBatch sb)
        {
            base.Draw(sb);
            #region DEBUG
#if DEBUG
            Game.GetInstance().RendererFactoryInst.BuildLineRenderer(
                new Vector2(CollideRect.X + CollideRect.Width / 2, CollideRect.Y - 10),
                new Vector2(0, 1),
                CollideRect.Width + 2, 12f,
                Color.Black
                ).Draw(sb);
            Game.GetInstance().RendererFactoryInst.BuildLineRenderer(
                new Vector2(CollideRect.X, CollideRect.Y - 10),
                new Vector2(1, 0),
                2f, CollideRect.Width * HP / MaxHP,
                Color.Red
                ).Draw(sb);
            // sensors debug
            Game.GetInstance().RendererFactoryInst.BuildLineRenderer(
                new Vector2(CollideRect.X + 2, CollideRect.Y - 6),
                new Vector2(0, 1),
                4f, 4f,
                (_sensors.Bump ? Color.Red : Color.Gray)
                ).Draw(sb);
            Game.GetInstance().RendererFactoryInst.BuildLineRenderer(
                new Vector2(CollideRect.X + 2 + 5, CollideRect.Y - 6),
                new Vector2(0, 1),
                4f, 4f,
                (_sensors.Target != null ? Color.Blue : Color.Gray)
                ).Draw(sb);
            Game.GetInstance().RendererFactoryInst.BuildLineRenderer(
                new Vector2(CollideRect.X + 2 + 10, CollideRect.Y - 6),
                new Vector2(0, 1),
                4f, 4f,
                (_sensors.IsTargetClose ? Color.Green : Color.Gray)
                ).Draw(sb);
            Game.GetInstance().RendererFactoryInst.BuildLineRenderer(
                new Vector2(CollideRect.X + 2 + 15, CollideRect.Y - 6),
                new Vector2(0, 1),
                4f, 4f,
                (_commonAIData.Moving ? Color.Yellow : Color.Gray)
                ).Draw(sb);
            if (_commonAIData.Path != null)
            {
                foreach (Vector2 point in _commonAIData.Path)
                {
                    Game.GetInstance().RendererFactoryInst.BuildLineRenderer(
                        new Vector2(point.X - 3, point.Y),
                        new Vector2(0, 1),
                        6, 6,
                        Color.AliceBlue
                        ).Draw(sb);
                }
                foreach (Vector2 point in _commonAIData.DebugPath)
                {
                    Game.GetInstance().RendererFactoryInst.BuildLineRenderer(
                        new Vector2(point.X - 1, point.Y),
                        new Vector2(0, 1),
                        3, 3,
                        Color.Black
                        ).Draw(sb);
                }
            }
            Game.GetInstance().RendererFactoryInst.BuildRectangleRenderer(
                new engine.utils.Rectf(
                    _commonAIData.Target.X - 1,
                    _commonAIData.Target.Y - 1,
                    3, 3
                    ),
                Color.Orange
                ).Draw(sb);
#endif
            #endregion
        }

        /*
         * protected overriden method
         */

        protected override void UpdateMovement(float elapsed, float totalElapsed)
        {
            UpdateTarget();
            SearchForTarget();
            _sensors.ElapsedTime = elapsed;
            _ai.Tick(_aiData);
            _sensors.Bump = false;
            _sensors.BumpPlayer = false;
        }

        protected override void OnDamageTaken()
        {
            // TODO: move in toward the attacker
        } 

        protected override void OnHPHitZero()
        {
            // TODO: launch animation, begin falling.
            Remove = true;
        }

        /*
         * private methods
         */

        private void UpdateTarget()
        {
            float playerSqDistance;

            if (_sensors.Target != null)
            {
                playerSqDistance = (CollideRect.Center - _sensors.Target.CollideRect.Center).LengthSquared();
                if (playerSqDistance > _properties.SqSightRange
                    || IsEntityReachable(_sensors.Target) == false
                    || _sensors.Target.Remove)
                {
                    _sensors.Target = null;
                }
            }
        }

        private void SearchForTarget()
        {
            ICollection<api.IGameEntity> players;
            api.IGameEntity nearestPlayer;
            float minPlayerSqDistance;

            // get players in sight range
            players = CollisionMediator.GetEntitiesInArea(
                new engine.utils.Rectf(
                    this.CollideRect.Center.X - _properties.SightRange / 2,
                    this.CollideRect.Center.Y - _properties.SightRange / 2,
                    _properties.SightRange,
                    _properties.SightRange
                    ),
                new api.EntityType[] { api.EntityType.PLAYER }
                );
            // extract the nearest and reachable one
            nearestPlayer = null;
            minPlayerSqDistance = _properties.SqSightRange;
            foreach (api.IGameEntity player in players)
            {
                // only alive players
                if (((api.IAliveEntity)player).IsAlive)
                {
                    float sqDistance;

                    // compute distance
                    sqDistance = (player.CollideRect.Center - CollideRect.Center).LengthSquared();
                    if (sqDistance <= minPlayerSqDistance)
                    {
                        // check reachability
                        // no entities between the player and the entity ? target aquired :D
                        if (IsEntityReachable(player))
                        {
                            nearestPlayer = player;
                            minPlayerSqDistance = sqDistance;
                        }
                    }
                }
            }
            // if we found one
            if (nearestPlayer != null)
            {
                // if we have not target so far, we now have one
                if (_sensors.Target == null)
                {
                    _sensors.Target = nearestPlayer;
                }
                // otherwise, if we already have a different target
                else if (_sensors.Target != null && _sensors.Target != nearestPlayer)
                {
                    // check how nearest the new target is
                    float oldTargetSqDistance;

                    oldTargetSqDistance = (_sensors.Target.CollideRect.Center - CollideRect.Center).LengthSquared();
                    if (oldTargetSqDistance - minPlayerSqDistance < _properties.TargetSqDistanceDiffThreshold)
                    {
                        _sensors.Target = nearestPlayer;
                    }
                }
            }
        }

        private bool IsEntityReachable(api.IGameEntity entityToReach)
        {
            ICollection<api.IGameEntity> entities;
            Rectf area;

            area = Rectf.FromTwoPoints(CollideRect.Center, entityToReach.CollideRect.Center);
            entities = CollisionMediator.GetEntitiesInArea(
                area,
                new api.EntityType[] {
                                api.EntityType.WALL,
                                api.EntityType.ENEMY,
                                api.EntityType.COPTER
                                }
                );
            foreach (api.IGameEntity entity in entities)
            {
                if (entity != this
                    && CollisionHelper.OBBIntersectsAABB(
                        CollideRect.Center,
                        entityToReach.CollideRect.Center,
                        CollideRect.Width, // FIXME: not exact, put a more accurate value if needed
                        entity.CollideRect
                        )
                    )
                {
                    return false;
                }
            }
            return true;
        }

    }
}
