﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ToTheCopter.game.entities.properties
{
    class EnemyProperties
    {

        public EntityProperties EntityPropertiesInst { get; set; }
        public int AttackDamage { get; set; }
        public float SightRange { get; set; }
        public float SqSightRange { get { return SightRange * SightRange; } }
        public float TargetDistanceDiffThreshold { get; set; }
        public float TargetSqDistanceDiffThreshold { get { return TargetDistanceDiffThreshold * TargetDistanceDiffThreshold; } }

        public EnemyProperties(
            EntityProperties entityProperties,
            int attackDamange,
            float sightRange,
            float targetDistanceDiffThreshold
            )
        {
            this.EntityPropertiesInst = entityProperties;
            this.AttackDamage = attackDamange;
            this.SightRange = sightRange;
            this.TargetDistanceDiffThreshold = targetDistanceDiffThreshold;
            this.EntityPropertiesInst = entityProperties;
        }
    }
}
