﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ToTheCopter.game.entities.properties
{
    class PlayerProperties
    {
        public EntityProperties EntityPropertiesInst { get; set; }
        public float RegenReduction { get; set; }
        public float OverallNoiseReduction { get; set; }
        public float WeaponNoiseReduction { get; set; }
        public float MovementNoiseReduction { get; set; }
        public float BaseNoise { get; set; }
        public float MaxStamina { get; set; }
        public float StaminaRegenRate { get; set; }
        public int MaxThrowable { get; set; }

        public PlayerProperties(
            EntityProperties entityPropertiesInst,
            float regenReduction,
            float overallNoiseReduction,
            float weaponNoiseReduction,
            float movementNoiseReduction,
            float baseNoise,
            float maxStamina,
            float staminaRegenRate,
            int maxThrowable
            )
        {
            this.EntityPropertiesInst = entityPropertiesInst;
            this.RegenReduction = regenReduction;
            this.OverallNoiseReduction = overallNoiseReduction;
            this.WeaponNoiseReduction = weaponNoiseReduction;
            this.MovementNoiseReduction = movementNoiseReduction;
            this.BaseNoise = baseNoise;
            this.MaxStamina = maxStamina;
            this.StaminaRegenRate = staminaRegenRate;
            this.MaxThrowable = maxThrowable;
        }


    }
}
