﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ToTheCopter.game.entities.properties
{
    class EntityProperties
    {
        public float MaxHP { get; set; }
        public float DamageReduction { get; set; }
        public float WalkSpeed { get; set; }
        public float RunSpeed { get; set; }

        public EntityProperties(float maxHP, float damageReduction, float walkSpeed, float runSpeed)
        {
            this.MaxHP = maxHP;
            this.DamageReduction = damageReduction;
            this.WalkSpeed = walkSpeed;
            this.RunSpeed = runSpeed;
        }
    }
}
