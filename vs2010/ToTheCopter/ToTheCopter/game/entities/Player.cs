﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ToTheCopter.engine.utils;
using Microsoft.Xna.Framework;
using ToTheCopter.engine.inputs;
using ToTheCopter.game.api;

namespace ToTheCopter.game.entities
{

    delegate void OnPlayerSuccess(api.IPlayer player);

    class Player : MovingEntity, api.IPlayer
    {

        /*
         * private attributes
         */

        private OnPlayerSuccess _onPlayerSuccess;
        private properties.PlayerProperties _properties;
        private int _playerNo;
        private IWeapon[] _weapons;
        private int _selectedWeapon;
        private Vector2 _screenDimensions;
        private ai.data.AIMap _aiMap;
        private float _stamina;

        /*
         * public events
         */

        public event OnWeaponChanged WeaponChanged;

        /*
         * constructors
         */

        public Player(
            OnPlayerSuccess onPlayerSuccess,
            properties.PlayerProperties playerPorperties,
            Point screenDimensions,
            Point center,
            int playerNo,
            engine.api.IEntityRenderer renderer,
            ai.data.AIMap aiMap
            )
            : base(playerPorperties.EntityPropertiesInst, EntityType.PLAYER, center, renderer)
        {
            _onPlayerSuccess = onPlayerSuccess;
            _properties = playerPorperties;
            _playerNo = playerNo;
            _weapons = new IWeapon[4];
            _screenDimensions = new Vector2(screenDimensions.X, screenDimensions.Y);
            _aiMap = aiMap;
            _stamina = _properties.MaxStamina;
            LastTimeHurt = 1f;
        }

        /*
         * properties
         */

        public int PlayerNo
        {
            get { return _playerNo; }
        }

        public IWeapon Weapon
        {
            get { return _weapons[_selectedWeapon]; }
        }

        public IWeapon[] WeaponList
        {
            get { return _weapons; }
        }

        public float LastTimeHurt { get; private set; }

        public float Stamina
        {
            get { return _stamina; }
            set
            {
                _stamina = Math.Max(0, Math.Min(_properties.MaxStamina, value));
                if (_stamina >= _properties.MaxStamina * 0.3f)
                {
                    Exhausted = false;
                }
            }
        }

        public float MaxStamina
        {
            get { return _properties.MaxStamina; }
            set { _properties.MaxStamina = value; }
        }

        public bool Exhausted { get; set; }

        /*
         * public IPlayer method impelementation
         */

        public bool PickUp(IWeapon weapon)
        {
            int weaponIdx;

            if (weapon.IsThrowable)
            {
                if (_weapons[api.PlayerConstants.WEAPON_IDX_THROWABLE] == null
                    || _weapons[api.PlayerConstants.WEAPON_IDX_THROWABLE].IsDepleted)
                {
                    weaponIdx = api.PlayerConstants.WEAPON_IDX_THROWABLE;
                }
                else if (_weapons[api.PlayerConstants.WEAPON_IDX_THROWABLE].Type == weapon.Type
                        && ((api.IThrowableWeapon)_weapons[api.PlayerConstants.WEAPON_IDX_THROWABLE]).Count < _properties.MaxThrowable)
                {
                    weaponIdx = api.PlayerConstants.WEAPON_IDX_THROWABLE;
                    ((api.IThrowableWeapon)_weapons[weaponIdx]).Count =
                        Math.Min(
                            ((api.IThrowableWeapon)weapon).Count + ((api.IThrowableWeapon)_weapons[weaponIdx]).Count,
                            _properties.MaxThrowable
                            );
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else if (_weapons[api.PlayerConstants.WEAPON_IDX_MAIN1] == null)
            {
                weaponIdx = api.PlayerConstants.WEAPON_IDX_MAIN1;
            }
            else if (_weapons[api.PlayerConstants.WEAPON_IDX_MAIN2] == null)
            {
                weaponIdx = api.PlayerConstants.WEAPON_IDX_MAIN2;
            }
            else
            {
                // no available slots
                return false;
            }
            _weapons[weaponIdx] = weapon;
            OnWeaponChanged(weaponIdx);
            return true;
        }

        public void Regen(float hp)
        {
            HP += hp * _properties.RegenReduction;
        }

        public void SetWeapon(int idx, IWeapon weapon)
        {
            _weapons[idx] = weapon;
            OnWeaponChanged(idx);
        }

        public void Succeed()
        {
            _onPlayerSuccess(this);
        }

        /*
         * public overriden method
         */

        public override void Collide(IGameEntity oth)
        {
            // nothing to do yet
        }

        /*
         * protected overriden method
         */

        protected override void UpdateMovement(float elapsed, float totalElapsed)
        {
            InputsState inputsState;

            Stamina += elapsed * _properties.StaminaRegenRate;
            inputsState = InputsHandler.GetInstance().GetInputsForPlayer(_playerNo);
            UpdateLookingDirection(inputsState);
            UpdateMovementDirection(inputsState, elapsed);
            UpdateSelectedWeapon(inputsState);
            _weapons[_selectedWeapon].Update(inputsState, CollideRect.Center + LookingDirection * CollideRect.Width, LookingDirection, elapsed);
            LastTimeHurt += elapsed;
            _aiMap.PropagateTrack(
                CollideRect.Center,
                _properties.OverallNoiseReduction
                * Math.Max(
                    _properties.BaseNoise
                        + _properties.MovementNoiseReduction * this.MovingVector.Length() / 2,
                    _properties.WeaponNoiseReduction * _weapons[_selectedWeapon].Noise
                    ),
                0f
                );
            if (_weapons[_selectedWeapon].IsDepleted)
            {
                OnWeaponChanged(api.PlayerConstants.WEAPON_IDX_START);
            }
        }

        protected override void OnDamageTaken()
        {
            LastTimeHurt = 0f;
        }

        protected override void OnHPHitZero()
        {
            Remove = true;
        }

        /*
         * private method
         */

        private void UpdateLookingDirection(InputsState inputsState)
        {
            Vector2 newDirection;

            newDirection = (
                new Vector2(
                    inputsState.PointerPosition.X,
                    inputsState.PointerPosition.Y
                    )
                - _screenDimensions / 2f
                );
            if (newDirection.X == 0f && newDirection.Y == 0f)
            {
                return;
            }
            newDirection.Normalize();
            LookingDirection = newDirection;
        }

        private void UpdateMovementDirection(InputsState inputsState, float elapsed)
        {
            float speed;
            Vector2 movingDirection = new Vector2(0, 0);

            // update movement speed
            speed = Properties.WalkSpeed;
            if (inputsState.GetAction(InputAction.SPRINT).IsDown()
                && Stamina > (elapsed * _properties.StaminaRegenRate)
                && Exhausted == false)
            {
                speed = Properties.RunSpeed;
            }
            // update movement direction
            if (inputsState.GetAction(InputAction.NORTH).IsDown())
            {
                movingDirection += new Vector2(0, -1);
            }
            if (inputsState.GetAction(InputAction.SOUTH).IsDown())
            {
                movingDirection += new Vector2(0, 1);
            }
            if (inputsState.GetAction(InputAction.WEST).IsDown())
            {
                movingDirection += new Vector2(-1, 0);
            }
            if (inputsState.GetAction(InputAction.EAST).IsDown())
            {
                movingDirection += new Vector2(1, 0);
            }
            if (movingDirection.X == 0f && movingDirection.Y == 0f)
            {
                return;
            }
            movingDirection.Normalize();
            MovingVector += movingDirection * speed;
            if (speed == Properties.RunSpeed)
            {
                Stamina -= elapsed * (_properties.StaminaRegenRate + 1f);
                if (Stamina == 0f)
                {
                    Exhausted = true;
                }
            }
        }   

        private void UpdateSelectedWeapon(InputsState inputsState)
        {
            int newlySelectedWeapon;

            newlySelectedWeapon = _selectedWeapon;
            if (inputsState.GetAction(InputAction.SELECT_WEAPON_START).IsDown())
                newlySelectedWeapon = api.PlayerConstants.WEAPON_IDX_START;
            if (inputsState.GetAction(InputAction.SELECT_WEAPON_MAIN1).IsDown())
                newlySelectedWeapon = api.PlayerConstants.WEAPON_IDX_MAIN1;
            if (inputsState.GetAction(InputAction.SELECT_WEAPON_MAIN2).IsDown())
                newlySelectedWeapon = api.PlayerConstants.WEAPON_IDX_MAIN2;
            if (inputsState.GetAction(InputAction.SELECT_WEAPON_THROWABLE).IsDown())
                newlySelectedWeapon = api.PlayerConstants.WEAPON_IDX_THROWABLE;
            if (newlySelectedWeapon != _selectedWeapon && _weapons[newlySelectedWeapon] != null)
            {
                OnWeaponChanged(newlySelectedWeapon);
            }
        }

        private void OnWeaponChanged(int newlySelectedWeapon)
        {
            if (_weapons[_selectedWeapon] != null)
            {
                _weapons[_selectedWeapon].OnWeaponUnselected();
            }
            _selectedWeapon = newlySelectedWeapon;
            _weapons[_selectedWeapon].Owner = this;
            if (WeaponChanged != null)
            {
                WeaponChanged(this, _weapons[_selectedWeapon]);
            }
        }

    }
}
