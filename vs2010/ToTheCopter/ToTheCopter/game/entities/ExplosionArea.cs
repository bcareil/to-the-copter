﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace ToTheCopter.game.entities
{
    class ExplosionArea : api.IGameEntity
    {

        private float _damage;
        private float _radius;
        private float _radiusSq;

        public ExplosionArea(Vector2 center, float radius, float damage)
        {
            _damage = damage;
            _radius = radius;
            _radiusSq = radius * radius;
            CollideRect = new engine.utils.Rectf(center.X - radius, center.Y - radius, radius * 2f, radius * 2f);
            Remove = false;
        }

        public engine.utils.Rectf CollideRect { get; set; }

        public api.ICollisionMediator CollisionMediator { get; set; }

        public engine.utils.OneDimensionalSegmentInt ZAxis { get; set; }

        public bool Remove { get; set; }

        public api.EntityType Type { get { return api.EntityType.BULLET; } }

        public Microsoft.Xna.Framework.Vector2 MovingVector { get { return new Microsoft.Xna.Framework.Vector2(0, 0); } set { } }

        public Microsoft.Xna.Framework.Vector2 LookingDirection { get { return MovingVector; } set { } }
        
        public void Collide(api.IGameEntity oth)
        {
            float sqDist;
            float damageDiv;

            sqDist = engine.utils.CollisionHelper.ComputeMinSquaredDistance(oth.CollideRect, CollideRect.Center);
            damageDiv = (sqDist / _radiusSq) * 5f;
            ((api.IAliveEntity)oth).TakeDamage(
                _damage * (1f / (1f + damageDiv * damageDiv))
                );
        }

        public bool IsCollidingWith(api.IGameEntity oth)
        {
            return (oth.Remove == false
                && Remove == false
                && engine.utils.CollisionHelper.CircleIntersectsAABB(CollideRect.Center, _radius, oth.CollideRect)
                );
        }

        public void Update(float elapsed, float totalElapsed)
        {
            Game.GetInstance().EntityManagerInst.AddEntity(
                Game.GetInstance().EntityFactoryInst.CreateExplosionParticle(CollideRect.Center, _radius, 1.2f)
                );
            CollisionMediator.OrderedCollisionCheck(
                this,
                CollideRect,
                CollideRect.Center,
                new api.EntityType[] {
                    api.EntityType.PLAYER,
                    api.EntityType.ENEMY
                }
                );
            Remove = true;
        }


        public void Draw(Microsoft.Xna.Framework.Graphics.SpriteBatch sb)
        {
            // nothing to do
        }
    }
}
