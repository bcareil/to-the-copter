﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using ToTheCopter.engine.utils;

namespace ToTheCopter.game.entities
{
    class EnemySpawner : api.IGameEntity
    {

        private Random _rand;
        private float _spawnInterval;
        private float _delayUntilNextSpawn;
        private LinkedList<api.IGameEntity> _spawnedEnemies;
        private int _spawnLimit;

        public EnemySpawner(engine.utils.Rectf area, int amountPer8Seconds)
        {
            _rand = new Random();
            _spawnInterval = 8f / (float)amountPer8Seconds;
            _delayUntilNextSpawn = 0f;
            _spawnedEnemies = new LinkedList<api.IGameEntity>();
            _spawnLimit = amountPer8Seconds << 1;
            CollideRect = area;
            Remove = false;
        }

        public engine.utils.Rectf CollideRect { get; set; }

        public api.ICollisionMediator CollisionMediator { get; set; }

        public OneDimensionalSegmentInt ZAxis { get { return new OneDimensionalSegmentInt(0, 16); } set { } }

        public bool Remove { get; set; }

        public api.EntityType Type { get { return api.EntityType.AREA; } }

        public Microsoft.Xna.Framework.Vector2 MovingVector { get { return new Vector2(0f, 0f); } set { } }

        public Vector2 LookingDirection { get { return new Vector2(1f, 0f); } set { } }

        public void Collide(api.IGameEntity oth)
        {
            // should not be called
            throw new NotImplementedException();
        }

        public bool IsCollidingWith(api.IGameEntity oth)
        {
            // should not be called
            throw new NotImplementedException();
        }

        public void Update(float elapsed, float totalElapsed)
        {
            RemoveDeadSpawnedEnemies();
            if (_spawnedEnemies.Count < _spawnLimit && InPlayerSightRange() == false)
            {
                _delayUntilNextSpawn -= elapsed;
                if (_delayUntilNextSpawn <= 0f)
                {
                    _delayUntilNextSpawn += _spawnInterval;
                    SpawnEnemy();
                }
            }
        }

        public void Draw(Microsoft.Xna.Framework.Graphics.SpriteBatch sb)
        {
#if DEBUG && FALSE
            Game.GetInstance().RendererFactoryInst.BuildLineRenderer(
                CollideRect.Location + new Microsoft.Xna.Framework.Vector2(0, CollideRect.Height / 2f),
                new Microsoft.Xna.Framework.Vector2(1, 0),
                CollideRect.Height,
                CollideRect.Width,
                Microsoft.Xna.Framework.Color.FromNonPremultiplied(255, 0, 0, 128)
                ).Draw(sb);
#endif
        }

        /*
         * private methods
         */

        private void SpawnEnemy()
        {
            Rectf enemyCollideRect;
            api.IGameEntity enemy;
            ICollection<api.IGameEntity> entitiesOnArea;

            enemy = Game.GetInstance().EntityFactoryInst.CreateEnemy(new Point((int)CollideRect.Center.X, (int)CollideRect.Center.Y));
            entitiesOnArea = CollisionMediator.GetEntitiesInArea(
                CollideRect,
                new api.EntityType[] { api.EntityType.ENEMY, api.EntityType.PLAYER, api.EntityType.WALL }
                );
            for (int i = 0; i < 10; ++i)
            {
                if (IsEnemyCollidingWithAnyOf(enemy, entitiesOnArea) == false)
                {
                    _spawnedEnemies.AddLast(enemy);
                    Game.GetInstance().EntityManagerInst.AddEntity(enemy);
                    return;
                }
                enemyCollideRect = enemy.CollideRect;
                enemyCollideRect.Location = new Vector2(
                    CollideRect.X + _rand.Next((int)(CollideRect.Width - enemy.CollideRect.Width)),
                    CollideRect.Y + _rand.Next((int)(CollideRect.Height - enemy.CollideRect.Height))
                    );
                enemy.CollideRect = enemyCollideRect;
            }
        }

        private bool IsEnemyCollidingWithAnyOf(api.IGameEntity enemy, IEnumerable<api.IGameEntity> entitiesOnArea)
        {
            foreach (api.IGameEntity entity in entitiesOnArea)
            {
                if (enemy.IsCollidingWith(entity))
                {
                    return true;
                }
            }
            return false;
        }

        private void RemoveDeadSpawnedEnemies()
        {
            LinkedListNode<api.IGameEntity> node;

            node = _spawnedEnemies.First;
            while (node != null)
            {
                if (node.Value.Remove)
                {
                    _spawnedEnemies.Remove(node);
                }
                node = node.Next;
            }
        }

        private bool InPlayerSightRange()
        {
            foreach (api.IPlayerScreen playerScreen in Game.GetInstance().PlayerScreens)
            {
                if (CollideRect.Intersects(playerScreen.SceneArea))
                {
                    return true;
                }
            }
            return false;
        }

    }
}
