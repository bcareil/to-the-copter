﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using ToTheCopter.ai.factory;
using ToTheCopter.ai.api;
using ToTheCopter.engine.utils;
using Microsoft.Xna.Framework.Audio;

namespace ToTheCopter.game.entities
{
    class EntityFactory
    {

        /*
         * private members
         */

        private ContentManager _content;
        private BhTBuilder _bhtBuilder;
        private IRootBehavior _enemyAI;

        /*
         * public constructor
         */

        public EntityFactory(ContentManager content)
        {
            _content = content;
            _bhtBuilder = new BhTBuilder(content);
            // load custom behavior
            _bhtBuilder.RegisterCustomNode("ExpressionHasValidPath", typeof(ai.nodes.expressions.AIBExpressionHasValidPath));
            _bhtBuilder.RegisterCustomNode("SensorHasDirectPath", typeof(ai.nodes.sensors.AIBSensorHasDirectPath));
            _bhtBuilder.RegisterCustomNode("SensorHasTarget", typeof(ai.nodes.sensors.AIBSensorHasTarget));
            _bhtBuilder.RegisterCustomNode("SensorHasTrack", typeof(ai.nodes.sensors.AIBSensorHasTrack));
            _bhtBuilder.RegisterCustomNode("SensorBump", typeof(ai.nodes.sensors.AIBSensorBump));
            _bhtBuilder.RegisterCustomNode("SensorBumpPlayer", typeof(ai.nodes.sensors.AIBSensorBumpPlayer));
            _bhtBuilder.RegisterCustomNode("ComputePath", typeof(ai.nodes.AIBComputePath));
            _bhtBuilder.RegisterCustomNode("DirectPath", typeof(ai.nodes.AIBDirectPath));
            _bhtBuilder.RegisterCustomNode("FollowPath", typeof(ai.nodes.AIBFollowPath));
            _bhtBuilder.RegisterCustomNode("HillClimbing", typeof(ai.nodes.AIBHillClimbing));
            _bhtBuilder.RegisterCustomNode("Move", typeof(ai.nodes.AIBMove));
            _bhtBuilder.RegisterCustomNode("MoveOnTarget", typeof(ai.nodes.AIBMoveOnTarget));
            _bhtBuilder.RegisterCustomNode("RandomMovement", typeof(ai.nodes.AIBRandomMovement));
            // load enemy AI
            _bhtBuilder.Deserialize("xml/ai-enemy");
            _enemyAI = AINodeFactory.CreateRootBehaviorFromBehavior("Root", _bhtBuilder.GetProduct());
        }

        /*
         * public properties
         */

        public properties.EnemyProperties EnemyPropertiesInst { get; set; }
        public properties.PlayerProperties PlayerPropertiesInst { get; set; }

        /*
         * public factory method
         */

        public api.IPlayer CreatePlayer(entities.OnPlayerSuccess onPlayerSuccess, Point screenDimensions, Point position, int playerNo)
        {
            return new entities.Player(
                onPlayerSuccess,
                PlayerPropertiesInst,
                screenDimensions,
                position,
                playerNo,
                Game.GetInstance().RendererFactoryInst.CreatePlayerRenderer(),
                Game.GetInstance().AIMapInst
                );
        }

        public api.IGameEntity CreateEnemy(Point position)
        {
            return new entities.Enemy(
                EnemyPropertiesInst,
                _enemyAI,
                position,
                Game.GetInstance().RendererFactoryInst.CreateEnemyRenderer()
                );
        }


        internal EnemySpawner CreateEnemySpawner(engine.utils.Rectf area, int amountPer8Seconds)
        {
            return new entities.EnemySpawner(area, amountPer8Seconds);
        }

        internal api.IGameEntity CreateBullet(
            api.IPlayer owner,
            Vector2 origin,
            Vector2 direction,
            float damage,
            float speed,
            float radius,
            float range,
            int maxPiercingThrough
            )
        {
            return new Bullet(owner, origin, direction, damage, speed, radius, range, maxPiercingThrough);
        }

        internal api.IGameEntity CreatePickUp(engine.utils.Rectf collideRect, int type, int subType)
        {
            return pickup.PickUpFactory.CreatePickUp(collideRect, type, subType);
        }

        internal api.IGameEntity CreateHandGrenade(Vector2 origin, Vector2 direction, float speed)
        {
            return new HandGrenadeEntity(
                Game.GetInstance().RendererFactoryInst.CreateHandGrenadeEntityRenderer(),
                _content.Load<SoundEffect>("sounds/entities/hand-grenade-bounce"),
                _content.Load<SoundEffect>("sounds/entities/hand-grenade-explode"),
                origin,
                direction,
                speed
                );
        }

        internal api.IGameEntity CreateHelicopter(Vector2 center)
        {
            engine.api.IEntityRenderer copter;
            engine.api.IEntityRenderer blades;
            Rectf collideRect;

            copter = Game.GetInstance().RendererFactoryInst.CreateHelicopterRenderer();
            blades = Game.GetInstance().RendererFactoryInst.CreateHelicopterBladesRenderer();
            copter.Zoom = new Vector2(1.5f, 1.5f);
            blades.Zoom = new Vector2(1.5f, 1.5f);
            collideRect = new Rectf(0, 0, 175, 32);
            collideRect.Center = center;
            return new Copter(
                copter,
                blades,
                _content.Load<SoundEffect>("sounds/entities/copter-landing"),
                _content.Load<SoundEffect>("sounds/entities/copter-take-off"),
                _content.Load<SoundEffect>("sounds/entities/copter-idle"),
                collideRect
                );
        }

        internal api.IGameEntity CreateExplosionParticle(Vector2 center, float radius, float life)
        {
            engine.api.IEntityRenderer renderer;

            renderer = Game.GetInstance().RendererFactoryInst.CreateExplosionRenderer(life);
            renderer.Center = center;
            renderer.SpriteDimensionsDestination = new Point((int)radius * 2, (int)radius * 2);
            return new Particle(
                renderer,
                life
                );
        }

        internal api.IGameEntity CreateLandingArea(Rectangle rectangle)
        {
            return new LandingArea(
                _content.Load<SpriteFont>("fonts/landing-area"),
                new Rectf(rectangle.X, rectangle.Y, rectangle.Width, rectangle.Height)
                );
        }

        internal api.IGameEntity CreateExplosionEntity(Vector2 center, float radius, float damage)
        {
            return new ExplosionArea(center, radius, damage);
        }
    }
}
