﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Content;
using ToTheCopter.engine.utils;

namespace ToTheCopter.game.entities.pickup
{
    static class PickUpFactory
    {
        public static api.IGameEntity CreatePickUp(Rectf collideRect, int type, int subType)
        {
            switch (type)
            {
                case 0:
                    return CreateWeaponPickUp(collideRect, subType);
                case 1:
                    return CreateHealthKitPickUp(collideRect, subType);
                default:
                    throw new Exception("Unknown pick up type : " + type);
            }
        }

        private static api.IGameEntity CreateWeaponPickUp(Rectf collideRect, int subType)
        {
            return new PickUpWeapon(
                collideRect,
                Game.GetInstance().WeaponFactoryInst.CreateWeapon((api.WeaponType)subType)
                );

        }

        private static api.IGameEntity CreateHealthKitPickUp(Rectf collideRect, int subType)
        {
            float[] healthPercentForSubtype = {
                0.15f,
                1f
                };

            return new PickUpHealthKit(
                Game.GetInstance().RendererFactoryInst.BuildHealthKitPickUpRenderer(subType),
                collideRect,
                healthPercentForSubtype[subType]
                );
        }
    }
}
