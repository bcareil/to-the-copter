﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ToTheCopter.engine.utils;

namespace ToTheCopter.game.entities.pickup
{
    class PickUpHealthKit : APickUp
    {

        private float _percentRegen;

        public PickUpHealthKit(engine.api.IEntityRenderer renderer, Rectf collideRect, float percentRengen)
            : base(renderer, collideRect)
        {
            _percentRegen = percentRengen;
        }

        protected override void OnEntityEntred(api.IGameEntity entity)
        {
            api.IPlayer player;

            player = (api.IPlayer)entity;
            if (player.HP < player.MaxHP)
            {
                player.Regen(player.MaxHP * _percentRegen);
                Remove = true;
            }
        }
    }
}
