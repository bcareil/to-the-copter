﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ToTheCopter.engine.utils;

namespace ToTheCopter.game.entities.pickup
{
    class PickUpWeapon : APickUp
    {

        private api.IWeapon _weapon;

        public PickUpWeapon(Rectf collideRect, api.IWeapon weapon)
            : base(weapon.Icon.Clone(), collideRect)
        {
            _weapon = weapon;
        }

        protected override void OnEntityEntred(api.IGameEntity entity)
        {
            if (_weapon != null && ((api.IPlayer)entity).PickUp(_weapon))
            {
                _weapon = null;
                Remove = true;
            }
        }
    }
}
