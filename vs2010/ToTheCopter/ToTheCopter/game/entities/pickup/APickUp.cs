﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ToTheCopter.engine.utils;

namespace ToTheCopter.game.entities.pickup
{
    abstract class APickUp : api.IGameEntity
    {

        private engine.api.IEntityRenderer _renderer;
        private ICollection<api.IGameEntity> _collidingEntities;

        public APickUp(engine.api.IEntityRenderer renderer, Rectf collideRect)
        {
            _renderer = renderer;
            _collidingEntities = new LinkedList<api.IGameEntity>();
            this.CollideRect = collideRect;
            this.Remove = false;

            _renderer.Center = this.CollideRect.Center;
        }

        public engine.utils.Rectf CollideRect { get; set; }

        public api.ICollisionMediator CollisionMediator { get; set; }

        public engine.utils.OneDimensionalSegmentInt ZAxis { get { return new engine.utils.OneDimensionalSegmentInt(0, 1); } set { } }

        public bool Remove { get; set; }

        public api.EntityType Type { get { return api.EntityType.PICKUP; } }

        public Microsoft.Xna.Framework.Vector2 MovingVector { get { return new Microsoft.Xna.Framework.Vector2(0, 0); } set { } }

        public Microsoft.Xna.Framework.Vector2 LookingDirection { get { return new Microsoft.Xna.Framework.Vector2(0, 0); } set { } }

        public void Collide(api.IGameEntity oth)
        {
            // nothing to do here
        }

        public bool IsCollidingWith(api.IGameEntity oth)
        {
            return (
                oth is api.IPlayer
                && ((api.IPlayer)oth).IsAlive
                && oth.CollideRect.Intersects(this.CollideRect)
                && oth.ZAxis.Intersects(this.ZAxis)
                );
        }

        public void Update(float elapsed, float totalElapsed)
        {
            ICollection<api.IGameEntity> entities;

            entities = CollisionMediator.GetEntitiesInArea(this.CollideRect, new api.EntityType[] { api.EntityType.PLAYER });
            foreach (api.IGameEntity entity in entities)
            {
                if (_collidingEntities.Contains(entity) == false)
                {
                    OnEntityEntred(entity);
                }
            }
            _collidingEntities = entities;
        }

        public void Draw(Microsoft.Xna.Framework.Graphics.SpriteBatch sb)
        {
            _renderer.Draw(sb);
#if DEBUG && FALSE
            Game.GetInstance().RendererFactoryInst.BuildLineRenderer(
                new Microsoft.Xna.Framework.Vector2(CollideRect.X, CollideRect.Y + CollideRect.Height / 2),
                new Microsoft.Xna.Framework.Vector2(0, 1),
                CollideRect.Width,
                CollideRect.Height,
                Microsoft.Xna.Framework.Color.FromNonPremultiplied(0, 255, 0, 128)
                ).Draw(sb);
#endif
        }

        protected abstract void OnEntityEntred(api.IGameEntity entity);

    }
}
