﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ToTheCopter.game.entities
{
    class Wall : api.IGameEntity
    {
        private engine.utils.Rectf _collideRect;
        private engine.gfx.BasicEntityRenderer _renderer;

        public Wall(engine.utils.Rectf collideRect, engine.gfx.BasicEntityRenderer renderer)
        {
            _collideRect = collideRect;
            _renderer = renderer;
            Remove = false;
        }

        public engine.utils.Rectf CollideRect
        {
            get { return _collideRect; }
            set { _collideRect = value; }
        }

        public api.ICollisionMediator CollisionMediator { get; set; }

        public engine.utils.OneDimensionalSegmentInt ZAxis { get { return new engine.utils.OneDimensionalSegmentInt(0, 16); } set { } }

        public bool Remove { get; set; }

        public api.EntityType Type { get { return api.EntityType.WALL; } }

        public void Update(float elapsed, float totalElapsed)
        {
            // nothing to do
        }

        public Microsoft.Xna.Framework.Vector2 MovingVector
        {
            get { return new Microsoft.Xna.Framework.Vector2(0, 0); }
            set { }
        }

        public Microsoft.Xna.Framework.Vector2 LookingDirection
        {
            get { return new Microsoft.Xna.Framework.Vector2(1, 0); }
            set { }
        }

        public float Speed
        {
            get { return 0f; }
            set { }
        }

        public void Draw(Microsoft.Xna.Framework.Graphics.SpriteBatch sb)
        {
            _renderer.Draw(sb);
#if DEBUG && FALSE
            Game.GetInstance().RendererFactoryInst.BuildLineRenderer(
                CollideRect.Location,
                new Microsoft.Xna.Framework.Vector2(1, 0),
                1,
                CollideRect.Width,
                Microsoft.Xna.Framework.Color.White
                ).Draw(sb);
            Game.GetInstance().RendererFactoryInst.BuildLineRenderer(
                CollideRect.Location,
                new Microsoft.Xna.Framework.Vector2(0, 1),
                1,
                CollideRect.Height,
                Microsoft.Xna.Framework.Color.Blue
                ).Draw(sb);
#endif
        }

        void api.IGameEntity.Collide(api.IGameEntity oth)
        {
            // should not happend since the wall never ask for collision check
            throw new NotImplementedException();
        }

        public bool IsCollidingWith(api.IGameEntity oth)
        {
            // should not happend since the wall never ask for collision check
            throw new NotImplementedException();
        }

        public void TakeDamage(int amount)
        {
            // nothing to do
        }

    }
}
