﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ToTheCopter.game.entities
{
    class Particle : api.IGameEntity
    {

        private engine.api.IEntityRenderer _renderer;
        private float _life;

        public Particle(engine.api.IEntityRenderer renderer, float life)
        {
            _renderer = renderer;
            _life = life;
        }

        public engine.utils.Rectf CollideRect { get { return _renderer.DestinationBounds; } set { _renderer.DestinationBounds = value; } }

        public api.ICollisionMediator CollisionMediator { get; set; }

        public engine.utils.OneDimensionalSegmentInt ZAxis { get; set; }

        public bool Remove { get { return _life <= 0f; } set { if (value) _life = 0f; } }

        public api.EntityType Type { get { return api.EntityType.PARTICLE; } }

        public Microsoft.Xna.Framework.Vector2 MovingVector { get; set; }

        public Microsoft.Xna.Framework.Vector2 LookingDirection { get { return MovingVector; } set { } }

        public void Collide(api.IGameEntity oth)
        {
            // should not happend
            throw new NotImplementedException();
        }

        public bool IsCollidingWith(api.IGameEntity oth)
        {
            // shoudl not happend
            throw new NotImplementedException();
        }

        public void Update(float elapsed, float totalElapsed)
        {
            _life -= elapsed;
            _renderer.Update(elapsed);
        }

        public void Draw(Microsoft.Xna.Framework.Graphics.SpriteBatch sb)
        {
            _renderer.Draw(sb);
        }
    }
}
