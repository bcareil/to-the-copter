﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using ToTheCopter.engine.utils;
using ToTheCopter.game.api;

namespace ToTheCopter.game.entities
{
    abstract class MovingEntity : api.IAliveEntity
    {

        private properties.EntityProperties _properties;
        private api.EntityType _entityType;
        private float _hp;
        private Rectf _collideRect;
        private OneDimensionalSegmentInt _zAxis;
        private Vector2 _movingVector;
        private Vector2 _lookingDirection;
        private float _damageReduction;
        private engine.api.IEntityRenderer _renderer;

        public MovingEntity(properties.EntityProperties properties, api.EntityType entityType, Point center, engine.api.IEntityRenderer renderer)
        {
            _properties = properties;
            _entityType = entityType;
            _hp = MaxHP;
            _collideRect = new Rectf(0, 0, 30, 30);
            _zAxis = new OneDimensionalSegmentInt(0, 6);
            _movingVector = new Vector2(0, 0);
            _lookingDirection = new Vector2(1, 0);
            _damageReduction = 1f;
            _renderer = renderer;

            _collideRect.Center = new Vector2(center.X, center.Y);
            _renderer.Center = _collideRect.Center;

            Remove = false;
        }

        public entities.properties.EntityProperties Properties { get { return _properties; } }

        public float MaxHP
        {
            get { return _properties.MaxHP; }
            set { _properties.MaxHP = value; }
        }

        public float HP
        {
            get { return _hp; }
            set { _hp = Math.Max(0, Math.Min(MaxHP, value)); }
        }

        public bool IsAlive
        {
            get { return (_hp != 0); }
        }

        public bool Remove { get; set; }

        public Rectf CollideRect
        {
            get { return _collideRect; }
            set { _collideRect = value; OnCenterChanged(); }
        }

        public ICollisionMediator CollisionMediator { get; set; }

        public OneDimensionalSegmentInt ZAxis { get { return _zAxis; } set { _zAxis = value; } }

        public Microsoft.Xna.Framework.Vector2 MovingVector
        {
            get { return _movingVector; }
            set { _movingVector = value;}
        }

        public Microsoft.Xna.Framework.Vector2 LookingDirection
        {
            get { return _lookingDirection; }
            set { _lookingDirection = value; OnLookingDirectionChanged(); }
        }

        public EntityType Type { get { return _entityType; } }

        public virtual void Draw(Microsoft.Xna.Framework.Graphics.SpriteBatch sb)
        {
            _renderer.Draw(sb);
        }

        public void Update(float elapsed, float totalElapsed)
        {
            UpdateMovement(elapsed, totalElapsed);
            UpdatePosition(elapsed);
            _renderer.Update(elapsed);
        }

        abstract public void Collide(IGameEntity oth);

        public bool IsCollidingWith(IGameEntity oth)
        {
            return (
                // check if the entity is not marked for removal
                oth.Remove == false
                // check height
                && ZAxis.Intersects(oth.ZAxis)
                // check collide rect
                && _collideRect.Intersects(oth.CollideRect)
                );
        }

        public void TakeDamage(float amount)
        {
            _hp -= amount / _damageReduction;
            if (_hp <= 0f)
            {
                _hp = 0f;
                OnHPHitZero();
            }
            OnDamageTaken();
        }

        /*
         * private method
         */

        private void OnLookingDirectionChanged()
        {
            _renderer.Direction = this.LookingDirection;
        }

        private void OnCenterChanged()
        {
            _renderer.Center = this.CollideRect.Center;
        }

        private void UpdatePosition(float elapsedTime)
        {
            Rectf collideRect;
            Vector2 movement;

            if (MovingVector.X == 0f && MovingVector.Y == 0f)
            {
                return;
            }
            collideRect = CollideRect;
            movement = MovingVector * elapsedTime;
            CollisionMediator.HandleMovementFor(
                this,
                movement,
                new EntityType[] {
                    EntityType.WALL,
                    EntityType.PLAYER,
                    EntityType.ENEMY,
                    EntityType.COPTER
                });
            MovingVector /= 2f;
        }

        /*
         * protected abstract methods
         */

        protected abstract void UpdateMovement(float elapsed, float totalElapsed);
        protected abstract void OnDamageTaken();
        protected abstract void OnHPHitZero();

    }
}
