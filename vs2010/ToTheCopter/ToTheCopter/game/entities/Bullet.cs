﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using ToTheCopter.engine.utils;

namespace ToTheCopter.game.entities
{
    class Bullet : api.IGameEntity
    {

        private api.IPlayer _owner;
        private Vector2 _position;
        private Vector2 _nextPosition;
        private Vector2 _direction;
        private float _damage;
        private float _speed;
        private float _radius;
        private float _distanceToTravel;
        private float _remainingPiercingThrough;
        private ISet<api.IGameEntity> _blacklist;
        private engine.api.IEntityRenderer _renderer;

        public Bullet(
            api.IPlayer owner,
            Vector2 position,
            Vector2 direction,
            float damage,
            float speed,
            float radius,
            float maxTravelingDistance,
            int maxPiercingThrough
            )
        {
            _owner = owner;
            _position = position;
            _nextPosition = position;
            _direction = direction;
            _damage = damage;
            _speed = speed;
            _radius = radius;
            _distanceToTravel = maxTravelingDistance;
            _remainingPiercingThrough = maxPiercingThrough;
            _blacklist = new HashSet<api.IGameEntity>();
            _renderer = Game.GetInstance().RendererFactoryInst.CreateRectangleRenderer();

            _blacklist.Add(_owner);
        }

        public engine.utils.Rectf CollideRect
        {
            // TODO: improve ?
            get { return new engine.utils.Rectf(_position.X - _radius, _position.Y - _radius, _radius, _radius); }
            set { _position = value.Center; }
        }

        public api.ICollisionMediator CollisionMediator { get; set; }

        public OneDimensionalSegmentInt ZAxis { get { return new OneDimensionalSegmentInt(3, 1); } set { } }

        public bool Remove { get; set; }

        public api.EntityType Type { get { return api.EntityType.BULLET; } }

        public Vector2 MovingVector
        {
            get { return _direction; }
            set { _direction = value; }
        }

        public Vector2 LookingDirection
        {
            get { return MovingVector; }
            set { }
        }

        public void Collide(api.IGameEntity oth)
        {
            if (oth.Type == api.EntityType.WALL)
            {
                // we hit a wall, ask for removing
                Remove = true;
            }
            else
            {
                // otherwise, inflict damage to the entity
                api.IAliveEntity aliveEntity;

                aliveEntity = (api.IAliveEntity)oth;
                aliveEntity.TakeDamage(_damage);
                // then black list it so we do not collide it again
                _blacklist.Add(oth);
                // decrease the remaining piercing through
                _remainingPiercingThrough--;
                // if it goes under zero, ask for remove
                if (_remainingPiercingThrough < 0)
                {
                    Remove = true;
                }
            }
        }

        public bool IsCollidingWith(api.IGameEntity oth)
        {
            return (
                // if we are marked for remove, we do not collide anything else
                Remove == false
                // check height and altitude
                && ZAxis.Intersects(oth.ZAxis)
                // check blacklist
                && _blacklist.Contains(oth) == false
                // check collideRect
                && engine.utils.CollisionHelper.OBBIntersectsAABB(_position, _nextPosition, _radius, oth.CollideRect)
                );
        }

        public void Update(float elapsed, float totalElapsed)
        {
            Vector2 movement;
            float travelledDistance;

            // compute the movement vector
            movement = _direction * (_speed * elapsed);
            travelledDistance = movement.Length();
            // compute the remaining distance to travel
            _distanceToTravel -= travelledDistance;
            if (_distanceToTravel < 0f)
            {
                movement = (movement / travelledDistance) * (travelledDistance + _distanceToTravel);
                _distanceToTravel = 0f;
                Remove = true;
            }
            // compute our next position using the movement vector
            _nextPosition = movement + _position;
            // ask for collision checking
            CollisionMediator.OrderedCollisionCheck(
                this,
                new Rectf(
                    Math.Min(_position.X, _nextPosition.X),
                    Math.Min(_position.Y, _nextPosition.Y),
                    Math.Abs(movement.X),
                    Math.Abs(movement.Y)
                    ),
                _position,
                new api.EntityType[] { api.EntityType.PLAYER, api.EntityType.ENEMY, api.EntityType.WALL, api.EntityType.COPTER }
                );
            // build renderer
            _renderer = Game.GetInstance().RendererFactoryInst.BuildLineRenderer(_position, movement, _radius * 2, movement.Length(), Color.Wheat);
            // move to the next position
            _position = _nextPosition;
        }

        public void Draw(Microsoft.Xna.Framework.Graphics.SpriteBatch sb)
        {
            _renderer.Draw(sb);
        }
    }
}
