﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Audio;
using ToTheCopter.engine.utils;

namespace ToTheCopter.game.entities
{
    class HandGrenadeEntity : api.IGameEntity
    {

        /*
         * private constants
         */

        private const float ROTATING_SPEED = 3.14f;
        private const float EXPLODE_DELAY = 3f;

        /*
         * private attributes
         */

        private engine.api.IEntityRenderer _renderer;
        private SoundEffect _soundBounce;
        private SoundEffect _soundExplode;
        private Rectf _collideRect;
        private float _zAxisVelocity;
        private float _zAxisPosition;
        private Timer _explodeTimer;
        private Vector2 _actualMovingVector;
        private float _reportedVelocity;

        /*
         * public constructor
         */

        public HandGrenadeEntity(
            engine.api.IEntityRenderer renderer,
            SoundEffect soundBounce,
            SoundEffect soundExplode,
            Vector2 origin,
            Vector2 direction,
            float speed
            )
        {
            _renderer = renderer;
            _soundBounce = soundBounce;
            _soundExplode = soundExplode;
            _collideRect = new engine.utils.Rectf(
                origin.X - renderer.SpriteDimensionsSource.X / 2,
                origin.Y - renderer.SpriteDimensionsSource.Y / 2,
                renderer.SpriteDimensionsSource.X,
                renderer.SpriteDimensionsSource.Y
                );
            _zAxisVelocity = 3f;
            _zAxisPosition = (int)api.ZAxisValue.HUMAN_HEIGHT;
            _explodeTimer = new Timer();
            _actualMovingVector = new Vector2(0, 0);
            _reportedVelocity = 0f;
            Remove = false;
            MovingVector = direction * speed;

            _renderer.Center = CollideRect.Center;
            _explodeTimer.Reset(EXPLODE_DELAY);
        }

        /*
         * public properties
         */

        public engine.utils.Rectf CollideRect { get { return _collideRect; } set { _collideRect = value; } }

        public api.ICollisionMediator CollisionMediator { get; set; }

        public engine.utils.OneDimensionalSegmentInt ZAxis { get; set; }

        public bool Remove { get; set; }

        public api.EntityType Type { get { return api.EntityType.BULLET; } }

        public Vector2 MovingVector { get; set; }

        public Vector2 LookingDirection { get { return MovingVector; } set { } }

        /*
         * public methods
         */

        public void Collide(api.IGameEntity oth)
        {
            Vector2 closestPoint;

            closestPoint = engine.utils.CollisionHelper.ComputeClosestPoint(oth.CollideRect, CollideRect.Center);
            _actualMovingVector.Normalize();
            _actualMovingVector *=
                (closestPoint - CollideRect.Center).Length()
                - CollideRect.Width / 2f - 0.01f;
            ;
            if (oth.Type == api.EntityType.WALL || oth.Type == api.EntityType.COPTER)
            {
                OnBounce();
                if (closestPoint.X == oth.CollideRect.X || closestPoint.X == oth.CollideRect.Right)
                {
                    // mirror on x
                    MovingVector = new Vector2(-MovingVector.X, MovingVector.Y);
                }
                else
                {
                    // mirror on y
                    MovingVector = new Vector2(MovingVector.X, -MovingVector.Y);
                }
            }
            else
            {
                MovingVector = new Vector2(0f, 0f);
            }
        }

        public bool IsCollidingWith(api.IGameEntity oth)
        {
            return (
                this.Remove == false
                && oth.Remove == false
                && (engine.utils.CollisionHelper.OBBIntersectsAABB(
                    CollideRect.Center,
                    CollideRect.Center + _actualMovingVector,
                    CollideRect.Width / 2f,
                    oth.CollideRect
                    )
                    || engine.utils.CollisionHelper.CircleIntersectsAABB(
                        CollideRect.Center,
                        CollideRect.Width / 2f,
                        oth.CollideRect
                        )
                    || engine.utils.CollisionHelper.CircleIntersectsAABB(
                        CollideRect.Center + _actualMovingVector,
                        CollideRect.Width / 2f,
                        oth.CollideRect
                        )
                    )
                );
        }

        public void Update(float elapsed, float totalElapsed)
        {
            Vector2 movement;
            Vector2 normalizedMovement;
            Rectf collisionCheckArea;

            // update timer
            _explodeTimer.Update(elapsed);
            if (_explodeTimer.Done)
            {
                Remove = true;
                _soundExplode.Play();
                Game.GetInstance().EntityManagerInst.AddEntity(
                    Game.GetInstance().EntityFactoryInst.CreateExplosionEntity(CollideRect.Center, 150f, 400f)
                );
                Game.GetInstance().AIMapInst.PropagateTrack(CollideRect.Center, 400f, 0.5f);
                return;
            }
            // update z position
            if (_zAxisVelocity != 0f)
            {
                _zAxisVelocity -= 9f * elapsed;
                _zAxisPosition += _zAxisVelocity;
                if (_zAxisPosition <= 0f)
                {
                    OnBounce();
                    _zAxisVelocity = (_zAxisVelocity - _zAxisPosition) / -2f;
                    if (_zAxisVelocity < 0.1f)
                    {
                        _zAxisVelocity = 0f;
                    }
                    _zAxisPosition = 0f;
                }
            }
            _renderer.Zoom = new Vector2(0.9f + _zAxisPosition / 75f, 0.9f + _zAxisPosition / 75f);
            // update x,y position
            if (Math.Abs(MovingVector.X) > 0.01f && Math.Abs(MovingVector.Y) > 0.01f)
            {
                movement = MovingVector * elapsed;
                normalizedMovement = movement;
                normalizedMovement.Normalize();
                movement = normalizedMovement * (movement.Length() + _reportedVelocity);
                _actualMovingVector = movement;
                collisionCheckArea = Rectf.FromTwoPoints(CollideRect.Center, CollideRect.Center + movement);
                CollisionMediator.OrderedCollisionCheck(
                    this,
                    collisionCheckArea,
                    CollideRect.Center,
                    new api.EntityType[] {
                        api.EntityType.PLAYER,
                        api.EntityType.ENEMY,
                        api.EntityType.WALL
                    }
                );
                _reportedVelocity = (movement.Length() - _actualMovingVector.Length()) / 2f;
                _collideRect.Center += _actualMovingVector;
            }
            // update renderer
            _renderer.Center = _collideRect.Center;
            _renderer.Angle += ROTATING_SPEED * elapsed * (_explodeTimer.Delay / EXPLODE_DELAY);
        }

        public void Draw(SpriteBatch sb)
        {
            _renderer.Draw(sb);
#if DEBUG && FALSE
            Game.GetInstance().RendererFactoryInst.BuildRectangleRenderer(
                CollideRect,
                Color.Green * 0.5f
                ).Draw(sb);
#endif
        }

        /*
         * private methods
         */

        private void OnBounce()
        {
            _soundBounce.Play();
            MovingVector /= 2f;
            Game.GetInstance().AIMapInst.PropagateTrack(
                CollideRect.Center,
                300f,
                0.5f
                );
        }

    }
}
