﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ToTheCopter.engine.utils;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace ToTheCopter.game.entities
{
    class LandingArea : api.IGameEntity
    {

        public const float COPTER_RESPAWN_DELAY = 30f;

        private api.IGameEntity _copter;
        private Timer _respawnTimer;
        private engine.gfx.DrawableString _textMessage;
        private engine.gfx.DrawableString _textTimer;

        public LandingArea(SpriteFont messageFont, engine.utils.Rectf area)
        {
            _copter = null;
            _respawnTimer = null;
            _textMessage = new engine.gfx.DrawableString(messageFont, "", area.Center + new Vector2(-5, -75), Color.Red);
            _textTimer = new engine.gfx.DrawableString(messageFont, "", area.Center + new Vector2(-5, 50), Color.Red);
            CollideRect = area;
            Remove = false;

            SetDefaultMessage();
        }

        public engine.utils.Rectf CollideRect { get; set; }

        public api.ICollisionMediator CollisionMediator { get; set; }

        public engine.utils.OneDimensionalSegmentInt ZAxis { get { return new engine.utils.OneDimensionalSegmentInt(0, 1); } set { } }

        public bool Remove { get; set; }

        public api.EntityType Type { get { return api.EntityType.AREA; } }

        public Microsoft.Xna.Framework.Vector2 MovingVector { get { return new Microsoft.Xna.Framework.Vector2(0, 0); } set { } }

        public Microsoft.Xna.Framework.Vector2 LookingDirection { get { return MovingVector; } set { } }

        public void Collide(api.IGameEntity oth)
        {
            // should not happend
            throw new NotImplementedException();
        }

        public bool IsCollidingWith(api.IGameEntity oth)
        {
            // should not happend
            throw new NotImplementedException();
        }

        public void Update(float elapsed, float totalElapsed)
        {
            if (_copter == null)
            {
                if (_respawnTimer == null)
                {
                    // waiting for someone to enter the area
                    foreach (api.IGameEntity player in CollisionMediator.GetEntitiesInArea(CollideRect, new api.EntityType[] { api.EntityType.PLAYER }))
                    {
                        _respawnTimer = new Timer();
                        _respawnTimer.Reset(COPTER_RESPAWN_DELAY);
                        break;
                    }
                }
                else
                {
                    // waiting for the respawn timer to time out.
                    _respawnTimer.Update(elapsed);
                    SetETAMessage();
                    if (_respawnTimer.Done)
                    {
                        _copter = Game.GetInstance().EntityFactoryInst.CreateHelicopter(CollideRect.Center + new Vector2(-30, -20));
                        Game.GetInstance().EntityManagerInst.AddEntity(_copter);
                        ClearMessage();
                    }
                }
            }
            else if (_copter.Remove)
            {
                // copter left/destroyed
                _copter = null;
                _respawnTimer = null;
                SetDefaultMessage();
            }
        }

        public void Draw(Microsoft.Xna.Framework.Graphics.SpriteBatch sb)
        {
            _textMessage.Draw(sb);
            _textTimer.Draw(sb);
#if DEBUG && FALSE
            Game.GetInstance().RendererFactoryInst.BuildRectangleRenderer(
                CollideRect,
                Microsoft.Xna.Framework.Color.Green * 0.5f
                ).Draw(sb);
#endif
        }

        /*
         * private methods
         */

        private void ClearMessage()
        {
            _textMessage.Text = "";
            _textTimer.Text = "";
        }

        private void SetDefaultMessage()
        {
            _textMessage.Text = "Call the copter";
            _textTimer.Text = "";
        }

        private void SetETAMessage()
        {
            _textMessage.Text = "ETA";
            _textTimer.Text = ((int)_respawnTimer.Delay).ToString();
        }

    }
}
