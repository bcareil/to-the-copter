﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using ToTheCopter.engine.utils;

namespace ToTheCopter.game
{
    public delegate void OnLevelTerminated();

    public class Game : engine.api.IUpdatable, engine.api.IDrawable
    {
        /*
         * public types
         */

        public enum DifficultyLevel
        {
            EASY,
            NORMAL,
            HARD,

            COUNT
        }

        /*
         * private types
         */

        private struct DifficultyLevelProperties
        {
            public entities.properties.EnemyProperties enemyProperties;
            public entities.properties.PlayerProperties playerProperties;

            public DifficultyLevelProperties(
                entities.properties.EnemyProperties enemyProperties,
                entities.properties.PlayerProperties playerProperties
                )
            {
                this.enemyProperties = enemyProperties;
                this.playerProperties = playerProperties;
            }
        }


        /*
         * private static attributes
         */

        private static Game _instance;

        /*
         * private attributes
         */

        private api.IPlayerScreen[] _playerScreens;
        private map.Map _map;
        private OnLevelTerminated _onSuccess;
        private OnLevelTerminated _onDefeat;

        /*
         * properties
         */

        //
        public bool IsRunning { get; private set; }
        // managers
        internal EntityManager EntityManagerInst { get; set; }
        // factories
        internal entities.EntityFactory EntityFactoryInst { get; set; }
        internal hud.HudFactory HudFactoryInst { get; set; }
        internal RendererFactory RendererFactoryInst { get; set; }
        internal weapons.WeaponFacotry WeaponFactoryInst { get; set; }
        // other
        internal ai.data.AIMap AIMapInst { get; set; }
        internal IEnumerable<api.IPlayerScreen> PlayerScreens { get { return _playerScreens; } }


        /*
         * public static functions
         */

        public static Game GetInstance()
        {
            return _instance;
        }

        /*
         * public constructor
         */

        public Game(
            OnLevelTerminated onSucces,
            OnLevelTerminated onDefeat,
            ContentManager content,
            Point screenDimensions,
            String mapName,
            DifficultyLevel difficultyLevel,
            int nbPlayers)
        {
            _instance = this;
            this.IsRunning = true;
            // load map
            _map = LoadMap(content, mapName);
            _onSuccess = onSucces;
            _onDefeat = onDefeat;
            // instantiate entity manager
            this.EntityManagerInst = new EntityManager(new Point(_map.Widht, _map.Height));
            // instantiate factories
            this.HudFactoryInst = new hud.HudFactory(content, screenDimensions);
            this.EntityFactoryInst = new entities.EntityFactory(content);
            this.RendererFactoryInst = new RendererFactory(content);
            this.WeaponFactoryInst = new weapons.WeaponFacotry(content);
            // load difficulty level
            LoadDifficultyLevel(difficultyLevel);
            // instantiate ai map
#if DEBUG
            if (map.MapConstants.TILE_WIDTH / 2 != 64)
                throw new Exception("/!\\ Actualize ai map size");
#endif
            this.AIMapInst = new ai.data.AIMap(_map.Widht * 2, _map.Height * 2, 6);
            // load entities from the map
            LoadMapEntities();
            // initialize player screens
            _playerScreens = new api.IPlayerScreen[nbPlayers];
            for (int i = 0; i < nbPlayers; ++i)
            {
                _playerScreens[i] = new PlayerScreen(
                    this.OnPlayerSuccess,
                    this.OnPlayerDeath,
                    content,
                    _map,
                    new Rectangle(0, 0, screenDimensions.X, screenDimensions.Y) /* screenDimensions TODO: change when multiplayer is implemented */,
                    i
                    );
                this.EntityManagerInst.AddEntity(_playerScreens[i].Player);
            }
        }

        /*
         * Interfaces implementation
         */

        public void Update(float elapsed, float totalElapsed)
        {
            this.AIMapInst.Update(elapsed, totalElapsed);
            this.EntityManagerInst.Update(elapsed, totalElapsed);
            for (int i = 0; i < _playerScreens.Length; ++i)
            {
                _playerScreens[i].Update(elapsed, totalElapsed);
            }
        }

        public void Draw(Microsoft.Xna.Framework.Graphics.SpriteBatch sb)
        {
            for (int i = 0; i < _playerScreens.Length; ++i)
            {
                _playerScreens[i].Draw(sb, this.EntityManagerInst);
            }
        }

        /*
         * private methods
         */
        private map.Map LoadMap(ContentManager content, String mapName)
        {
            map.MapGenerator mapGenerator = new map.MapGenerator(content);

            mapGenerator.LoadFromBmpFile(mapName);
            return mapGenerator.GetMap();
        }

        private void LoadMapEntities()
        {
            // load walls
            foreach (map.entities.MapWall mapWall in _map.GetWalls())
            {
                entities.Wall wall;

                if (mapWall != null)
                {
                    wall = new entities.Wall(mapWall.CollideRect, mapWall.Renderer);
                    EntityManagerInst.AddEntity(wall);
                    // also, blacklist this location on the ai map
                    AIMapInst.BlacklistArea(mapWall.CollideRect);
                }
            }
            // load enemy spawners
            foreach (map.entities.MapAreaEnemySpawn mapAreaEnemySpawn in _map.GetEnemySpawnAreas())
            {
                entities.EnemySpawner enemySpawner;

                enemySpawner = EntityFactoryInst.CreateEnemySpawner(mapAreaEnemySpawn.Area, mapAreaEnemySpawn.AmountPer8Second);
                EntityManagerInst.AddEntity(enemySpawner);
            }
            // load pick up emplacements
            foreach (map.pickup.MapPickUp mapPickUp in _map.GetPickUp())
            {
                EntityManagerInst.AddEntity(EntityFactoryInst.CreatePickUp(
                    mapPickUp.Area,
                    mapPickUp.Type,
                    mapPickUp.SubType)
                    );
            }
            // load landing area
            EntityManagerInst.AddEntity(
                EntityFactoryInst.CreateLandingArea(_map.GetCopterArea())
                );
            // commit static area so they are maped once for all in the collision mediator.
            EntityManagerInst.CommitStaticEntities();
        }

        private void LoadDifficultyLevel(DifficultyLevel difficultyLevel)
        {
            // initialize entities properties per difficulty level
            DifficultyLevelProperties[] difficultyLevelProperties = new DifficultyLevelProperties[] {
                // Level Easy
                new DifficultyLevelProperties(
                    new entities.properties.EnemyProperties(
                        new entities.properties.EntityProperties(
                            150f,   // MaxHP
                            1.5f,   // DamageReduction
                            25f,    // WalkSpeed
                            80f     // RunSpeed
                            ),
                        15,     // AttackDamage
                        256f,   // SightRange
                        128f    // TargetDistanceDiffThreshold
                        ),
                    new entities.properties.PlayerProperties(
                        new entities.properties.EntityProperties(
                            250f,   // MaxHP
                            0.8f,   // DamageReduction
                            75f,    // WalkSpeed
                            125f    // RunSpeed
                            ),
                        1.2f,   // RegenReduction
                        0.8f,   // OverallNoiseReduction
                        1f,     // WeaponNoiseReduction
                        1f,     // MovementNoiseReduction
                        150f,   // BaseNoise
                        15f,    // MaxStamina
                        1f,     // StaminaRegenRate
                        5       // MaxThrowable
                        )
                    ),
                // Level Normal
                new DifficultyLevelProperties(
                    new entities.properties.EnemyProperties(
                        new entities.properties.EntityProperties(
                            250f,   // MaxHP
                            1f,      // DamageReduction
                            35f,    // WalkSpeed
                            90f     // RunSpeed
                            ),
                        20,     // AttackDamage
                        256f,   // SightRange
                        64f     // TargetDistanceDiffThreshold
                        ),
                    new entities.properties.PlayerProperties(
                        new entities.properties.EntityProperties(
                            200f,   // MaxHP
                            1f,   // DamageReduction
                            75f,    // WalkSpeed
                            125f    // RunSpeed
                            ),
                        1f,     // RegenReduction
                        1f,     // OverallNoiseReduction
                        1f,     // WeaponNoiseReduction
                        1f,     // MovementNoiseReduction
                        150f,   // BaseNoise
                        12f,    // MaxStamina
                        0.75f,  // StaminaRegenRate
                        4       // MaxThrowable
                        )
                    ),
                // Level Hard
                new DifficultyLevelProperties(
                    new entities.properties.EnemyProperties(
                        new entities.properties.EntityProperties(
                            300f,   // MaxHP
                            0.9f,   // DamageReduction
                            25f,    // WalkSpeed
                            100f    // RunSpeed
                            ),
                        25,     // AttackDamage
                        256f,   // SightRange
                        32f     // TargetDistanceDiffThreshold
                        ),
                    new entities.properties.PlayerProperties(
                        new entities.properties.EntityProperties(
                            150f,   // MaxHP
                            1f,     // DamageReduction
                            65f,    // WalkSpeed
                            110f    // RunSpeed
                            ),
                        0.8f,   // RegenReduction
                        1.1f,   // OverallNoiseReduction
                        1f,     // WeaponNoiseReduction
                        1f,     // MovementNoiseReduction
                        150f,   // BaseNoise
                        8f,     // MaxStamina
                        0.5f,   // StaminaRegenRate
                        3       // MaxThrowable
                        )
                    )
                };
            // load the entities property depending of the chosen difficulty level
            EntityFactoryInst.EnemyPropertiesInst = difficultyLevelProperties[(int)difficultyLevel].enemyProperties;
            EntityFactoryInst.PlayerPropertiesInst = difficultyLevelProperties[(int)difficultyLevel].playerProperties;
        }

        private void OnPlayerSuccess(api.IPlayerScreen playerScreen)
        {
            this._onSuccess();
        }

        private void OnPlayerDeath(api.IPlayerScreen playerScreen)
        {
            this._onDefeat();
        }

    }
}
