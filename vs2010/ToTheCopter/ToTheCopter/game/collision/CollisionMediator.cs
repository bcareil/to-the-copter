﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ToTheCopter.engine.utils;
using Microsoft.Xna.Framework;
using System.Collections;

namespace ToTheCopter.game.collision
{
    class CollisionMediator : api.ICollisionMediator
    {
        /*
         * public constants
         */

        public const float COLLISION_PADDING = 0.2f;
        public const float COLLISION_PADDING_EXT = COLLISION_PADDING + 0.001f;

        /*
         * private types
         */

        private enum MovementCorrectionType
        {
            X,
            Y,
            MIN,

            COUNT
        }

        private class OrderedCollisionComparator : IComparer<api.IGameEntity>
        {

            private Vector2 _origin;

            public OrderedCollisionComparator(Vector2 origin)
            {
                _origin = origin;
            }

            public int Compare(api.IGameEntity a, api.IGameEntity b)
            {
                float ad;
                float bd;

                if (a == b) return 0;
                ad = engine.utils.CollisionHelper.ComputeMinSquaredDistance(a.CollideRect, _origin);
                bd = engine.utils.CollisionHelper.ComputeMinSquaredDistance(b.CollideRect, _origin);
                return (ad == bd ? 0 : (ad < bd ? -1 : 1));
            }

        }

        /*
         * private attributes
         */

        private ICollection<api.IGameEntity>[] _entitiesByType;
        private api.ICollisionPartitioner[] _collisionPartitioners;

        /*
         * public constructor
         */

        public CollisionMediator(Point mapDimensions)
        {
            _entitiesByType = new ICollection<api.IGameEntity>[(int)api.EntityType.COUNT];
            _collisionPartitioners = new api.ICollisionPartitioner[(int)api.EntityType.COUNT];

            _collisionPartitioners[(int)api.EntityType.BULLET] = new NotAPartitioner();
            _collisionPartitioners[(int)api.EntityType.ENEMY] = new CollisionGrid(mapDimensions.X, mapDimensions.Y);
            _collisionPartitioners[(int)api.EntityType.PARTICLE] = new PartitionerPlaceHolder();
            _collisionPartitioners[(int)api.EntityType.PICKUP] = new NotAPartitioner();
            _collisionPartitioners[(int)api.EntityType.PLAYER] = new NotAPartitioner();
            _collisionPartitioners[(int)api.EntityType.WALL] = new CollisionGrid(mapDimensions.X, mapDimensions.Y);
            _collisionPartitioners[(int)api.EntityType.AREA] = new CollisionGrid(mapDimensions.X, mapDimensions.Y);
            _collisionPartitioners[(int)api.EntityType.COPTER] = new NotAPartitioner();
        }

        /*
         * interface implementation
         */

        public void UpdateEntities(api.EntityType entityType, ICollection<api.IGameEntity> entities)
        {
            _entitiesByType[(int)entityType] = entities;
            _collisionPartitioners[(int)entityType].Clear();
            _collisionPartitioners[(int)entityType].Feed(entities);
        }

        public List<api.IGameEntity> GetEntitiesInArea(Rectf area, api.EntityType[] collidingEntitieTypes)
        {
            List<api.IGameEntity> collidingEntities;
            
            collidingEntities = new List<api.IGameEntity>();
            for (int i = 0; i < collidingEntitieTypes.Length; ++i)
            {
                collidingEntities.AddRange(
                    _collisionPartitioners[(int)collidingEntitieTypes[i]].GetEntitiesInArea(area)
                );
            }
            return collidingEntities;
        }

        public Vector2 HandleMovementFor(api.IGameEntity entity, Vector2 movement, api.EntityType[] collidingEntitieTypes)
        {
            List<api.IGameEntity> collidingEntities;
            Vector2? solution;
            Rectf collisionResolutionArea;
            Rectf entityCollideRect;
            Rectf newCollideRect;
            Rectf entityCollisionArea;

            // compute the new collide rect according to the movement
            entityCollideRect = entity.CollideRect;
            newCollideRect = entityCollideRect;
            newCollideRect.Location += movement;
            // set the collision area : the smallest rectangle containing both the
            // current collide rect of the entity end the collide rect of the entity
            // after the movement.
            collisionResolutionArea = new Rectf(
                Math.Min(entityCollideRect.X, newCollideRect.X),
                Math.Min(entityCollideRect.Y, newCollideRect.Y),
                Math.Max(
                    entityCollideRect.Right - newCollideRect.Left,
                    newCollideRect.Right - entityCollideRect.Left
                    ),
                Math.Max(
                    entityCollideRect.Bottom - newCollideRect.Top,
                    newCollideRect.Bottom - entityCollideRect.Top
                    )
                );
            // get all colliding entity in this area
            collidingEntities = GetEntitiesInArea(collisionResolutionArea, collidingEntitieTypes);
            collidingEntities.Remove(entity);
            // compute the movement solution
            solution = ResolveMovementRec(
                entity,
                collidingEntities,
                0,
                newCollideRect,
                movement,
                movement
                );
            // no solution, no movement
            if (solution == null)
            {
                return new Vector2(0, 0);
            }
            // otherwise move the entity
            newCollideRect = entityCollideRect;
            newCollideRect.Location += (Vector2)solution;
            entity.CollideRect = newCollideRect;
            // create a collide rect including collision padding
            entityCollisionArea = new Rectf(
                entity.CollideRect.X - COLLISION_PADDING_EXT,
                entity.CollideRect.Y - COLLISION_PADDING_EXT,
                entity.CollideRect.Width + COLLISION_PADDING_EXT * 2f,
                entity.CollideRect.Height + COLLISION_PADDING_EXT * 2f
                );
            // then check for alter the entity of the occured collision
            foreach (api.IGameEntity collidingEntity in collidingEntities)
            {
                if (collidingEntity.CollideRect.Intersects(entityCollisionArea))
                {
                    entity.Collide(collidingEntity);
                }
            }
            return (Vector2)solution;
        }

        public void OrderedCollisionCheck(api.IGameEntity entity, Rectf area, Vector2 origin, api.EntityType[] collidingEntitieTypes)
        {
            List<api.IGameEntity> collidingEntities;

            // get the entities in the area
            collidingEntities = GetEntitiesInArea(area, collidingEntitieTypes);
            // sort them using the direction
            collidingEntities.Sort(new OrderedCollisionComparator(origin));
            // then check for actual collision
            foreach (api.IGameEntity collidingEntity in collidingEntities)
            {
                if (entity.IsCollidingWith(collidingEntity))
                {
                    entity.Collide(collidingEntity);
                }
            }
        }

        /*
         * private methods
         */

        private Vector2? ResolveMovementRec(
            api.IGameEntity entity,
            List<api.IGameEntity> collidingEntities,
            int idxCe,
            Rectf newCollideRect,
            Vector2 choosenMovementCorrection,
            Vector2 minMovementCorrection
            )
        {
            SortedList movementSolutions;
            Vector2[] movementCorrections;
            int idxMc;

            // get the index of the next colliding entity
            idxCe = GetNextCollidingEntity(collidingEntities, newCollideRect, idxCe, collidingEntities.Count);
            // if none of the entity in the movement area collide with the new
            // collide rect, it's fine, let's return the new movement
            if (idxCe == collidingEntities.Count)
            {
                return choosenMovementCorrection;
            }
            // otherwise, compute the movement correction for the colliding entity
            movementCorrections = new Vector2[(int)MovementCorrectionType.COUNT];
            movementCorrections[2] = minMovementCorrection;
            ComputeMovementCorrection(entity, collidingEntities[idxCe], choosenMovementCorrection, movementCorrections);
            // for each movement correction, see if one satisfy the predecessors
            movementSolutions = new SortedList();
            for (idxMc = 0; idxMc < movementCorrections.Length; ++idxMc)
            {
                int j;

                newCollideRect = entity.CollideRect;
                choosenMovementCorrection = movementCorrections[idxMc];
                newCollideRect.Location += choosenMovementCorrection;
                j = GetNextCollidingEntity(collidingEntities, newCollideRect, 0, idxCe + 1);
                // check whether the predecessors are satisfied
                if (j == idxCe + 1)
                {
                    Vector2? solution;

                    // we have a solution satifying the predecessors, keep it
                    if (idxCe + 1 >= collidingEntities.Count)
                    {
                        // if we were processing the last colliding entity, this is our solution
                        solution = choosenMovementCorrection;
                    }
                    else
                    {
                        // otherwise, use this local solution to find if it satisfy the other
                        // entities or if we can have a better one
                        solution = ResolveMovementRec(
                            entity,
                            collidingEntities,
                            idxCe + 1,
                            newCollideRect,
                            choosenMovementCorrection,
                            minMovementCorrection
                            );
                    }
                    // check whether our local solution has satified everybody
                    if (solution != null)
                    {
                        Vector2 nonNullSolution;
                        float key;

                        // then add it to the potential solution, sorted by the movement distance
                        nonNullSolution = (Vector2)solution;
                        key = nonNullSolution.LengthSquared();
                        if (movementSolutions.ContainsKey(key) == false)
                        {
                            movementSolutions.Add(key, nonNullSolution);
                        }
                    }
                }
            }
            // now, get the best solution, if we have none, return null
            if (movementSolutions.Count == 0)
            {
                return null;
            }
            // the best solution is the one which satisfy every body AND has the bigger movement
            return (Vector2)movementSolutions.GetByIndex(movementSolutions.Count - 1);
        }

        private void ComputeMovementCorrection(api.IGameEntity entity, api.IGameEntity collidingEntity, Vector2 choosentMovementCorrection, Vector2[] movementCorrections)
        {
            movementCorrections[(int)MovementCorrectionType.X].X = 0;
            movementCorrections[(int)MovementCorrectionType.X].Y = choosentMovementCorrection.Y;
            movementCorrections[(int)MovementCorrectionType.Y].X = choosentMovementCorrection.X;
            movementCorrections[(int)MovementCorrectionType.Y].Y = 0;
            // compute correction on x
            if (choosentMovementCorrection.X > 0)
            {
                movementCorrections[(int)MovementCorrectionType.X].X = Math.Max(
                    0f,
                    collidingEntity.CollideRect.Left - entity.CollideRect.Right - COLLISION_PADDING
                    );
            }
            else
            {
                movementCorrections[(int)MovementCorrectionType.X].X = Math.Min(
                    0f,
                    collidingEntity.CollideRect.Right - entity.CollideRect.X + COLLISION_PADDING
                    );
            }
            // compute correction on y
            if (choosentMovementCorrection.Y > 0)
            {
                movementCorrections[(int)MovementCorrectionType.Y].Y = Math.Max(
                    0f,
                    collidingEntity.CollideRect.Top - entity.CollideRect.Bottom - COLLISION_PADDING
                    );
            }
            else
            {
                movementCorrections[(int)MovementCorrectionType.Y].Y = Math.Min(
                    0f,
                    collidingEntity.CollideRect.Bottom - entity.CollideRect.Top + COLLISION_PADDING
                    );
            }
            // adjust minimum on x
            if (Math.Abs(movementCorrections[(int)MovementCorrectionType.X].X) < Math.Abs(movementCorrections[(int)MovementCorrectionType.MIN].X))
            {
                movementCorrections[(int)MovementCorrectionType.MIN].X = movementCorrections[(int)MovementCorrectionType.X].X;
            }
            // adjust minimum on y
            if (Math.Abs(movementCorrections[(int)MovementCorrectionType.Y].Y) < Math.Abs(movementCorrections[(int)MovementCorrectionType.MIN].Y))
            {
                movementCorrections[(int)MovementCorrectionType.MIN].Y = movementCorrections[(int)MovementCorrectionType.Y].Y;
            }
        }

        private int GetNextCollidingEntity(List<api.IGameEntity> collidingEntities, Rectf newCollideRect, int start, int max)
        {
            for (int i = start; i < max; ++i)
            {
                if (collidingEntities[i].CollideRect.Intersects(newCollideRect))
                {
                    return i;
                }
            }
            return max;
        }

    }
}
