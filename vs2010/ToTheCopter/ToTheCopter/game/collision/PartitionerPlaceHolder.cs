﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ToTheCopter.game.collision
{
    /// <summary>
    /// Unimplemented partitioner (all method's bodies are empty)
    /// </summary>
    class PartitionerPlaceHolder : api.ICollisionPartitioner
    {
        public void Feed(ICollection<api.IGameEntity> entities)
        {
        }

        public void AddEntity(api.IGameEntity entity)
        {
        }

        public void RemoveEntity(api.IGameEntity entity)
        {
        }

        public void Clear()
        {
        }

        public ICollection<api.IGameEntity> GetEntitiesInArea(engine.utils.Rectf area)
        {
            return new List<api.IGameEntity>();
        }
    }
}
