﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ToTheCopter.game.collision
{
    class NotAPartitioner : api.ICollisionPartitioner
    {
        private ICollection<api.IGameEntity> _entities;

        public NotAPartitioner()
        {
            _entities = new List<api.IGameEntity>();
        }

        public void Feed(ICollection<api.IGameEntity> entities)
        {
            _entities = new List<api.IGameEntity>(entities);
        }

        public void AddEntity(api.IGameEntity entity)
        {
            _entities.Add(entity);
        }

        public void RemoveEntity(api.IGameEntity entity)
        {
            _entities.Remove(entity);
        }

        public void Clear()
        {
            _entities.Clear();
        }

        public ICollection<api.IGameEntity> GetEntitiesInArea(engine.utils.Rectf area)
        {
            ICollection<api.IGameEntity> collidingEntities;

            collidingEntities = new LinkedList<api.IGameEntity>();
            foreach (api.IGameEntity entity in _entities)
            {
                if (area.Intersects(entity.CollideRect))
                {
                    collidingEntities.Add(entity);
                }
            }
            return collidingEntities;
        }

    }
}
