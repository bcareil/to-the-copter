﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ToTheCopter.game.api;
using Microsoft.Xna.Framework;

namespace ToTheCopter.game.collision
{
    class CollisionGrid : ICollisionPartitioner
    {

        public const int CELL_EDGE_POWER = 7;
        public const int CELL_EDGE_LENGTH = 1 << 7;
        
        private ICollection<IGameEntity>[] _cells;
        private int _width;
        private int _height;

        public CollisionGrid(int width, int height)
        {
            _width = width;
            _height = height;
            _cells = null;
            Init();
        }
        
        /*
         * interface implementation
         */

        public void Feed(ICollection<IGameEntity> entities)
        {
            foreach (IGameEntity entity in entities)
            {
                AddEntity(entity);
            }
        }

        public void AddEntity(IGameEntity entity)
        {
            Rectangle gridRect;

            gridRect = MapRectToGridRect(entity.CollideRect);
            for (int x = gridRect.X; x < gridRect.Right; ++x)
            {
                for (int y = gridRect.Y; y < gridRect.Bottom; ++y)
                {
                    GetCell(x, y).Add(entity);
                }
            }
        }

        public void RemoveEntity(IGameEntity entity)
        {
            Rectangle gridRect;

            gridRect = MapRectToGridRect(entity.CollideRect);
            for (int x = gridRect.X; x < gridRect.Right; ++x)
            {
                for (int y = gridRect.Y; y < gridRect.Bottom; ++y)
                {
                    GetCell(x, y).Remove(entity);
                }
            }
        }

        public void Clear()
        {
            Init();
        }

        public ICollection<IGameEntity> GetEntitiesInArea(engine.utils.Rectf area)
        {
            ISet<IGameEntity> entitySet;
            LinkedList<IGameEntity> collidingEntities;
            LinkedListNode<IGameEntity> collidingEntityNode;
            Rectangle gridRect;

            // get all potentially colliding entities according to the grid
            entitySet = new HashSet<IGameEntity>();
            gridRect = MapRectToGridRect(area);
            for (int x = gridRect.X; x < gridRect.Right; ++x)
            {
                for (int y = gridRect.Y; y < gridRect.Bottom; ++y)
                {
                    entitySet.UnionWith(GetCell(x, y));
                }
            }
            // keep only the entities realy colliding
            collidingEntities = new LinkedList<IGameEntity>(entitySet);
            collidingEntityNode = collidingEntities.First;
            while (collidingEntityNode != null)
            {
                if (collidingEntityNode.Value.CollideRect.Intersects(area) == false)
                {
                    collidingEntities.Remove(collidingEntityNode);
                }
                collidingEntityNode = collidingEntityNode.Next;
            }
            return collidingEntities;
        }

        /*
         * private methods
         */

        private void Init()
        {
            _cells = new ICollection<IGameEntity>[_width * _height];
            for (int i = 0; i < _cells.Length; ++i)
            {
                _cells[i] = new LinkedList<IGameEntity>();
            }
        }

        private Rectangle MapRectToGridRect(engine.utils.Rectf area)
        {
            int x;
            int y;
            int w;
            int h;

            x = ((int)area.X) >> CELL_EDGE_POWER;
            y = ((int)area.Y) >> CELL_EDGE_POWER;
            w = (((int)Math.Ceiling(area.Right) + (CELL_EDGE_LENGTH - 1)) >> CELL_EDGE_POWER) - x;
            h = (((int)Math.Ceiling(area.Bottom) + (CELL_EDGE_LENGTH - 1)) >> CELL_EDGE_POWER) - y;
            return new Rectangle(
                Math.Max(0, x),
                Math.Max(0, y),
                Math.Min(_width - x, w),
                Math.Min(_height - y, h)
                );
        }

        private ICollection<IGameEntity> GetCell(int x, int y)
        {
            return _cells[x + y * _width];
        }

    }
}
