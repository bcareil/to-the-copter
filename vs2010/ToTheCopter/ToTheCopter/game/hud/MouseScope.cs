﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using ToTheCopter.engine.inputs;
using ToTheCopter.game.api;
using ToTheCopter.engine.api;

namespace ToTheCopter.game.hud
{
    class MouseScope : IScope
    {
        /*
         * private attributes
         */

        IEntityRenderer _scopeRenderer;
        IEntityRenderer _spreadIndicatorRenderer;
        HealthGauge _healthGauge;
        AmmoGauge _ammoGauge;
        StaminaGauge _staminaGauge;
        Point _position;
        float _spreadAngle;
        float _spreadWidth2;
        api.IPlayerScreen _playerScreen;

        /*
         * constructors
         */

        public MouseScope(api.IPlayerScreen playerScreen)
        {
            _scopeRenderer = null;
            _spreadIndicatorRenderer = null;
            _healthGauge = new HealthGauge(playerScreen.Player);
            _ammoGauge = new AmmoGauge(playerScreen.Player);
            _staminaGauge = new StaminaGauge(playerScreen.Player);
            _playerScreen = playerScreen;

            Remove = false;
            Hide = false;

            _playerScreen.Player.WeaponChanged += new OnWeaponChanged(OnPlayerWeaponChanged);
            _healthGauge.Direction = new Vector2(-1, 0);
        }

        /*
         * public properties
         */

        public bool Remove { get; set; }
        public bool Hide { get; set; }

        /*
         * public implementation
         */

        public void Update(float elapsedTime, float totalElapsedTime)
        {
            Update();
            // update gauges
            _healthGauge.Center = _scopeRenderer.Center + new Vector2(-32, 0);
            _healthGauge.Update(elapsedTime, totalElapsedTime);
            _ammoGauge.Center = _scopeRenderer.Center + new Vector2(32, 0);
            _ammoGauge.Update(elapsedTime, totalElapsedTime);
            _staminaGauge.Center = _healthGauge.Center + new Vector2(-10, 0);
            _staminaGauge.Update(elapsedTime, totalElapsedTime);
        }

        public void Draw(SpriteBatch sb)
        {
            if (Hide == true)
                return ;
            _scopeRenderer.Draw(sb);
            // draw spread indicator
            // right
            _spreadIndicatorRenderer.Direction = new Vector2(-1, 0);
            _spreadIndicatorRenderer.Center = _scopeRenderer.Center + new Vector2(_spreadIndicatorRenderer.SpriteDimensionsDestination.X / 2 + _spreadWidth2 + 1, 0);
            _spreadIndicatorRenderer.Draw(sb);
            // left
            _spreadIndicatorRenderer.Direction = new Vector2(1, 0);
            _spreadIndicatorRenderer.Center = _scopeRenderer.Center - new Vector2(_spreadIndicatorRenderer.SpriteDimensionsDestination.X / 2 + _spreadWidth2, 0);
            _spreadIndicatorRenderer.Draw(sb);
            // top
            _spreadIndicatorRenderer.Direction = new Vector2(0, 1);
            _spreadIndicatorRenderer.Center = _scopeRenderer.Center - new Vector2(0, _spreadIndicatorRenderer.SpriteDimensionsDestination.Y / 2 + _spreadWidth2);
            _spreadIndicatorRenderer.Draw(sb);
            // bottom
            _spreadIndicatorRenderer.Direction = new Vector2(0, -1);
            _spreadIndicatorRenderer.Center = _scopeRenderer.Center + new Vector2(0, _spreadIndicatorRenderer.SpriteDimensionsDestination.Y / 2 + _spreadWidth2 + 1);
            _spreadIndicatorRenderer.Draw(sb);
            // gauge
            _staminaGauge.Draw(sb);
            _healthGauge.Draw(sb);
            _ammoGauge.Draw(sb);
        }

        /*
         * private methods
         */

        private void Update()
        {
            _position = InputsHandler.GetInstance().GetInputsForPlayer(_playerScreen.Player.PlayerNo).PointerPosition;
            _spreadAngle = _playerScreen.Player.Weapon.SpreadAngle;
            // _spreadWidth2 (read spread width divided by 2) is the distance between the center of the spread indicator
            // and the center of the scope. This value is directly influenced by the spread angle of the weapon of the
            // player and is equal to : 
            // w/2 = d * tan(a / 2)
            // w : spread width
            // d : distance between the player and the scope
            // a : spread angle
            _spreadWidth2 =
                // distance between the player and the cursor position (h)
                (new Vector2(_position.X, _position.Y) - _playerScreen.Player.CollideRect.Center - _playerScreen.Translation).Length() *
                // tan(a / 2)
                (float)Math.Tan(_spreadAngle / 2)
                ;
            // update scope renderer
            _scopeRenderer.Center = new Vector2(_position.X, _position.Y);
        }

        private void OnPlayerWeaponChanged(IPlayer player, IWeapon newWeapon)
        {
            _scopeRenderer = new engine.gfx.BasicEntityRenderer(
                newWeapon.ScopeSprite,
                new Point(0, 0),
                new Point(64, 64),
                new Vector2(32, 32),
                new Vector2(1, 0),
                new Vector2(_position.X, _position.Y),
                Color.FromNonPremultiplied(255, 255, 255, 128)
                );
            _spreadIndicatorRenderer = new engine.gfx.BasicEntityRenderer(
                newWeapon.ScopeSprite,
                new Point(64, 16),
                new Point(32, 32),
                new Vector2(16, 16),
                new Vector2(1, 0),
                new Vector2(0, 0),
                Color.FromNonPremultiplied(255, 255, 255, 128)
                );
        }

    }
}
