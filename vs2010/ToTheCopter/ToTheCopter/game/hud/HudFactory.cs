﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace ToTheCopter.game.hud
{
    class HudFactory
    {

        /*
         * private attributes
         */

        private ContentManager _content;
        private Point _screenDimensions;

        /*
         * private constructor
         */

        public HudFactory(ContentManager content, Point screenDimensions)
        {
            _content = content;
            _screenDimensions = screenDimensions;
        }

        /*
         * public factory methods
         */

        public api.IScope CreateScope(api.IPlayerScreen playerScreen, api.ScopeType type)
        {
            if (type == api.ScopeType.MOUSE)
                return new MouseScope(playerScreen);
            throw new ArgumentException("Invalid argment " + type);
        }

        public api.IHudEntity CreateHealthBar(api.IAliveEntity entity)
        {
            return new HealthBar(_screenDimensions, entity);
        }

        internal api.IHudEntity CreateWeaponList(api.IPlayer player)
        {
            return new hud.WeaponList(
                _screenDimensions,
                player,
                Game.GetInstance().RendererFactoryInst.CreateRectangleRenderer(),
                Game.GetInstance().RendererFactoryInst.CreateHudWeaponSlotRenderer(),
                _content.Load<SpriteFont>("fonts/weapon-list")
                );
        }

    }
}
