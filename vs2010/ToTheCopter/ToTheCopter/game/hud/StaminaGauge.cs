﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace ToTheCopter.game.hud
{
    class StaminaGauge : AGauge
    {
        private api.IPlayer _player;

        public StaminaGauge(api.IPlayer player)
            : base()
        {
            _player = player;
            _renderer.Direction = new Vector2(-1, 0);
        }

        public override void Update(float elapsed, float totalElapsed)
        {
            float percent;

            if (_player.Exhausted)
            {
                Hide = !Hide;
            }
            else
            {
                Hide = false;
            }
            percent = _player.Stamina / _player.MaxStamina;
            if (percent < 0.25f || _player.Exhausted)
            {
                _renderer.OverrideColor = Color.Turquoise * 0.5f;
            }
            else
            {
                _renderer.OverrideColor = Color.Blue * 0.33f;
                if (percent == 1f)
                {
                    Hide = true;
                }
            }
            _renderer.SpriteDimensions = new Point(24, (int)(_renderer.SpriteSheet.Height * percent));
            _renderer.SpriteCoordinates = new Point(0, (int)(_renderer.SpriteSheet.Height / 2 - (_renderer.SpriteSheet.Height / 2) * percent));
            _renderer.SpriteCenter = new Vector2(_renderer.SpriteDimensions.X / 2f, _renderer.SpriteDimensions.Y / 2f);
        }
    }
}
