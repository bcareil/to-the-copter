﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace ToTheCopter.game.hud
{
    abstract class AGauge : api.IHudEntity
    {

        protected engine.api.IEntityRenderer _renderer;

        public AGauge()
        {
            _renderer = Game.GetInstance().RendererFactoryInst.CreateGaugeRenderer();
        }

        public Vector2 Center { get { return _renderer.Center; } set { _renderer.Center = value; } }

        public Vector2 Direction { get { return _renderer.Direction; } set { _renderer.Direction = value; } }

        public bool Hide { get; set; }

        public bool Remove { get; set; }

        public void Draw(Microsoft.Xna.Framework.Graphics.SpriteBatch sb)
        {
            if (Hide == false)
            {
                _renderer.Draw(sb);
            }
        }

        public abstract void Update(float elapsed, float totalElapsed);

    }
}
