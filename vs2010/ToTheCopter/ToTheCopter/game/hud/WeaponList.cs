﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using ToTheCopter.engine.utils;

namespace ToTheCopter.game.hud
{
    class WeaponList : api.IHudEntity
    {

        private Point _screenDimensions;
        private AWeaponSlot[] _weaponSlots;

        public WeaponList(
            Point screenDimensions,
            api.IPlayer player,
            engine.api.IEntityRenderer squareRenderer,
            engine.api.IEntityRenderer weaponSlotRenderer,
            SpriteFont font
            )
        {
            _screenDimensions = screenDimensions;
            _weaponSlots = new AWeaponSlot[api.PlayerConstants.NB_WEAPON_SLOTS];

            for (int i = 0; i < _weaponSlots.Length; ++i)
            {
                if (i == api.PlayerConstants.WEAPON_IDX_THROWABLE)
                {
                    _weaponSlots[i] = new ThrowableWeaponSlot(font, squareRenderer.Clone(), weaponSlotRenderer.Clone(), player, i);
                }
                else
                {
                    _weaponSlots[i] = new WeaponSlot(font, squareRenderer.Clone(), weaponSlotRenderer.Clone(), player, i);
                }
            }
            InitializeWeaponSlotsPosition(weaponSlotRenderer);
        }

        /*
         * public properties
         */

        public bool Hide { get; set; }

        public bool Remove { get; set; }

        /*
         * public methods
         */

        public void Draw(Microsoft.Xna.Framework.Graphics.SpriteBatch sb)
        {
            foreach (AWeaponSlot weaponSlot in _weaponSlots)
            {
                weaponSlot.Draw(sb);
            }
        }

        public void Update(float elapsed, float totalElapsed)
        {
            foreach (AWeaponSlot weaponSlot in _weaponSlots)
            {
                weaponSlot.Update(elapsed, totalElapsed);
            }
        }

        /*
         * private methods
         */

        private void InitializeWeaponSlotsPosition(engine.api.IEntityRenderer weaponSlotRenderer)
        {
            for (int i = _weaponSlots.Length - 1; i >= 0; --i)
            {
                _weaponSlots[i].UpperLeftCorner = new Vector2(
                    _screenDimensions.X - weaponSlotRenderer.SpriteDimensionsSource.X,
                    _screenDimensions.Y - weaponSlotRenderer.SpriteDimensionsSource.Y * (_weaponSlots.Length - i)
                    );
            }
        }

    }
}
