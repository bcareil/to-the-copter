﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace ToTheCopter.game.hud
{
    class AmmoGauge : AGauge
    {

        private Color DEFAULT_COLOR = Color.Wheat * 0.33f;
        private Color RELOADING_COLOR = Color.Red * 0.5f;

        private api.IPlayer _player;

        public AmmoGauge(api.IPlayer player)
            : base()
        {
            _player = player;
            _renderer.OverrideColor = DEFAULT_COLOR;
        }

        public override void Update(float elapsed, float totalElapsed)
        {
            float reloadingPercent;

            reloadingPercent = _player.Weapon.ReloadingRemainingPercent;
            if (reloadingPercent == 0f)
            {
                _renderer.SpriteDimensions = new Point(24, (int)(_renderer.SpriteSheet.Height * _player.Weapon.Ammo / _player.Weapon.MaxAmmo));
                _renderer.SpriteCoordinates = new Point(0, (int)(_renderer.SpriteSheet.Height / 2 - (_renderer.SpriteSheet.Height / 2) * _player.Weapon.Ammo / _player.Weapon.MaxAmmo));
                _renderer.SpriteCenter = new Vector2(_renderer.SpriteDimensions.X / 2f, _renderer.SpriteDimensions.Y / 2f);
                _renderer.OverrideColor = DEFAULT_COLOR;
                Hide = false;
            }
            else
            {
                _renderer.SpriteDimensions = new Point(24, (int)(_renderer.SpriteSheet.Height - _renderer.SpriteSheet.Height * reloadingPercent));
                _renderer.SpriteCoordinates = new Point(0, (int)((_renderer.SpriteSheet.Height / 2) * reloadingPercent));
                _renderer.SpriteCenter = new Vector2(_renderer.SpriteDimensions.X / 2f, _renderer.SpriteDimensions.Y / 2f);
                _renderer.OverrideColor = RELOADING_COLOR;
                Hide = !Hide;
            }
        }

    }
}
