﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using ToTheCopter.engine.gfx;
using Microsoft.Xna.Framework.Graphics;

namespace ToTheCopter.game.hud
{
    abstract class AWeaponSlot : api.IHudEntity
    {

        /*
         * private constants
         */

        private Vector2 TRANSLATION_DEPLETED_OVERLAY = new Vector2(16, -14);
        private Vector2 TRANSLATION_WEAPON_ICON = new Vector2(16, -14);
        private Vector2 TRANSLATION_WEAPON_IDX = new Vector2(-46, 0);

        /*
         * private attributes
         */

        private Vector2 _upperLeftCorner;
        private SpriteFont _font;
        private engine.api.IEntityRenderer _weaponIcon;
        protected engine.api.IEntityRenderer _weaponSlotRenderer;
        private engine.api.IEntityRenderer _depletedOverlay;
        protected api.IPlayer _player;
        protected int _weaponIdx;
        private DrawableString _textWeaponIdx;
        private Vector2 _weaponIconCenter;

        /*
         * public constructor
         */

        public AWeaponSlot(
            SpriteFont font,
            engine.api.IEntityRenderer squareRenderer,
            engine.api.IEntityRenderer weaponSlotRenderer,
            api.IPlayer player,
            int weaponIdx
            )
        {
            _font = font;
            _depletedOverlay = squareRenderer;
            _weaponSlotRenderer = weaponSlotRenderer;
            _weaponIcon = null;
            _player = player;
            _weaponIdx = weaponIdx;
            _textWeaponIdx = new DrawableString(_font, "0", new Vector2(), Color.White);
            _weaponIconCenter = new Vector2();

            _depletedOverlay.OverrideColor = Color.Transparent;
            _depletedOverlay.SpriteDimensionsDestination = new Point(64, 64);
            _textWeaponIdx.Text = (_weaponIdx + 1).ToString();
        }

        /*
         * public properties
         */

        public bool Hide { get; set; }

        public bool Remove { get; set; }

        public Vector2 UpperLeftCorner { get { return _upperLeftCorner; } set { _upperLeftCorner = value; OnPositionUpdated(); } }

        /*
         * public methods
         */

        public void Draw(Microsoft.Xna.Framework.Graphics.SpriteBatch sb)
        {
            _weaponSlotRenderer.Draw(sb);
            if (_weaponIcon != null)
            {
                _weaponIcon.Draw(sb);
            }
            _depletedOverlay.Draw(sb);
            _textWeaponIdx.Draw(sb);
            DrawAmmunition(sb);
        }

        public void Update(float elapsed, float totalElapsed)
        {
            api.IWeapon weapon;

            weapon = _player.WeaponList[_weaponIdx];
            if (weapon == null)
            {
                _weaponIcon = null;
                _depletedOverlay.OverrideColor = Color.Black * 0.5f;
                UpdateNoWeapon(elapsed, totalElapsed);
            }
            else if (weapon.IsDepleted)
            {
                _weaponIcon = weapon.Icon.Clone();
                _weaponIcon.Center = _weaponIconCenter;
                _depletedOverlay.OverrideColor = Color.Red * 0.5f;
                UpdateWeaponDepleted(elapsed, totalElapsed);
            }
            else
            {
                _weaponIcon = weapon.Icon.Clone();
                _weaponIcon.Center = _weaponIconCenter;
                _depletedOverlay.OverrideColor = Color.Transparent;
                UpdateWeaponAmmunition(elapsed, totalElapsed);
            }
            if (_player.Weapon == weapon)
            {
                _weaponSlotRenderer.OverrideColor = Color.Blue;
            }
            else
            {
                _weaponSlotRenderer.OverrideColor = Color.White * 0.5f;
            }
        }

        /*
         * abstract methods
         */

        protected abstract void DrawAmmunition(SpriteBatch sb);
        protected abstract void UpdateNoWeapon(float elapsed, float totalElapsed);
        protected abstract void UpdateWeaponDepleted(float elapsed, float totalElapsed);
        protected abstract void UpdateWeaponAmmunition(float elapsed, float totalElapsed);
        protected abstract void UpdateOtherPositions();

        /*
         * private methods
         */

        private void OnPositionUpdated()
        {
            _weaponSlotRenderer.Center = UpperLeftCorner
                + new Vector2(_weaponSlotRenderer.SpriteDimensionsSource.X / 2f, _weaponSlotRenderer.SpriteDimensionsSource.Y / 2f);
            _depletedOverlay.Center = _weaponSlotRenderer.Center + TRANSLATION_DEPLETED_OVERLAY;
            _textWeaponIdx.Center = _weaponSlotRenderer.Center + TRANSLATION_WEAPON_IDX;
            _weaponIconCenter = _weaponSlotRenderer.Center + TRANSLATION_WEAPON_ICON;
            UpdateOtherPositions();
        }

    }
}
