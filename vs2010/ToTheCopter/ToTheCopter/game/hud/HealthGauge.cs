﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace ToTheCopter.game.hud
{
    class HealthGauge : AGauge
    {
        private api.IPlayer _player;

        public HealthGauge(api.IPlayer player)
            : base()
        {
            _player = player;
        }

        public override void Update(float elapsed, float totalElapsed)
        {
            if (_player.HP / _player.MaxHP < 0.25f)
            {
                _renderer.OverrideColor = Color.Red * 0.5f;
            }
            else
            {
                _renderer.OverrideColor = Color.Green * 0.33f;
            }
            _renderer.SpriteDimensions = new Point(24, (int)(_renderer.SpriteSheet.Height * _player.HP / _player.MaxHP));
            _renderer.SpriteCoordinates = new Point(0, (int)(_renderer.SpriteSheet.Height / 2 - (_renderer.SpriteSheet.Height / 2) * _player.HP / _player.MaxHP));
            _renderer.SpriteCenter = new Vector2(_renderer.SpriteDimensions.X / 2f, _renderer.SpriteDimensions.Y / 2f);
        }
    }
}
