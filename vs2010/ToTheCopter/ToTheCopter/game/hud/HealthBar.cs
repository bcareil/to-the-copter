﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace ToTheCopter.game.hud
{
    class HealthBar : api.IHudEntity
    {

        private api.IAliveEntity _entity;
        private Point _screenDimensions;
        private engine.api.IEntityRenderer _healthBarPadding;
        private engine.api.IEntityRenderer _currentHealthBarRenderer;

        public HealthBar(Point screenDimensions, api.IAliveEntity entity)
        {
            _entity = entity;
            _screenDimensions = screenDimensions;
            _healthBarPadding = Game.GetInstance().RendererFactoryInst.BuildLineRenderer(
                new Vector2(7, _screenDimensions.Y - 20f),
                new Vector2(1, 0),
                21f, 206f,
                Color.Gray
                );
        }

        public bool Hide { get; set; }

        public bool Remove { get; set; }

        public void Draw(Microsoft.Xna.Framework.Graphics.SpriteBatch sb)
        {
            _healthBarPadding.Draw(sb);
            _currentHealthBarRenderer.Draw(sb);
        }

        public void Update(float elapsed, float totalElapsed)
        {
            _currentHealthBarRenderer = Game.GetInstance().RendererFactoryInst.BuildLineRenderer(
                new Vector2(10, _screenDimensions.Y - 20),
                new Vector2(1, 0),
                15f, (_entity.HP / _entity.MaxHP) * 200f,
                Color.Red
                );
        }
    }
}
