﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using ToTheCopter.engine.gfx;

namespace ToTheCopter.game.hud
{
    class WeaponSlot : AWeaponSlot
    {

        private Vector2 TRANSLATION_AMMO = new Vector2(-7, 35);
        private Vector2 TRANSLATION_SPARE_AMMO = new Vector2(41, 35);

        private DrawableString _textAmmo;
        private DrawableString _textSpareAmmo;

        public WeaponSlot(
            SpriteFont font,
            engine.api.IEntityRenderer squareRenderer,
            engine.api.IEntityRenderer weaponSlotRenderer,
            api.IPlayer player,
            int weaponIdx
            ) : base(font, squareRenderer, weaponSlotRenderer, player, weaponIdx)
        {
            _textAmmo = new DrawableString(font, "", new Vector2(), Color.White);
            _textSpareAmmo = new DrawableString(font, "", new Vector2(), Color.White);
        }

        protected override void DrawAmmunition(SpriteBatch sb)
        {
            _textAmmo.Draw(sb);
            _textSpareAmmo.Draw(sb);
        }

        protected override void UpdateNoWeapon(float elapsed, float totalElapsed)
        {
            _textAmmo.Text = "";
            _textSpareAmmo.Text = "";
        }

        protected override void UpdateWeaponDepleted(float elapsed, float totalElapsed)
        {
            _textAmmo.Text = "0";
            _textAmmo.Color = Color.Red;
            _textSpareAmmo.Text = "0";
            _textSpareAmmo.Color = Color.Red;
        }

        protected override void UpdateWeaponAmmunition(float elapsed, float totalElapsed)
        {
            _textAmmo.Text = _player.WeaponList[_weaponIdx].Ammo.ToString();
            _textAmmo.Color = Color.White;
            _textSpareAmmo.Text = "\u221e";
            _textSpareAmmo.Color = Color.White;
        }

        protected override void UpdateOtherPositions()
        {
            _textAmmo.Center = _weaponSlotRenderer.Center + TRANSLATION_AMMO;
            _textSpareAmmo.Center = _weaponSlotRenderer.Center + TRANSLATION_SPARE_AMMO;
        }
    }
}
