﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using ToTheCopter.engine.gfx;
using Microsoft.Xna.Framework;

namespace ToTheCopter.game.hud
{
    class ThrowableWeaponSlot : AWeaponSlot
    {

        private Vector2 TRANSLATION_COUNT = new Vector2(16, 35);

        private DrawableString _textCount;

        public ThrowableWeaponSlot(
            SpriteFont font,
            engine.api.IEntityRenderer squareRenderer,
            engine.api.IEntityRenderer weaponSlotRenderer,
            api.IPlayer player,
            int weaponIdx
            ) : base(font, squareRenderer, weaponSlotRenderer, player, weaponIdx)
        {
            _textCount = new DrawableString(font, "", new Vector2(), Color.White);
        }

        protected override void DrawAmmunition(Microsoft.Xna.Framework.Graphics.SpriteBatch sb)
        {
            _textCount.Draw(sb);
        }

        protected override void UpdateNoWeapon(float elapsed, float totalElapsed)
        {
            _textCount.Text = "";
        }

        protected override void UpdateWeaponDepleted(float elapsed, float totalElapsed)
        {
            _textCount.Text = "0";
            _textCount.Color = Color.Red;
        }

        protected override void UpdateWeaponAmmunition(float elapsed, float totalElapsed)
        {
            _textCount.Text = ((api.IThrowableWeapon)_player.WeaponList[_weaponIdx]).Count.ToString();
            _textCount.Color = Color.White;
        }

        protected override void UpdateOtherPositions()
        {
            _textCount.Center = _weaponSlotRenderer.Center + TRANSLATION_COUNT;
        }
    }
}
