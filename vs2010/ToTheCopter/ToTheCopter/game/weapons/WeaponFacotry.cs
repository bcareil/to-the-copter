﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Audio;

namespace ToTheCopter.game.weapons
{
    class WeaponFacotry
    {
        ContentManager _content;

        public WeaponFacotry(ContentManager content)
        {
            _content = content;
        }

        public api.IWeapon CreateWeapon(api.WeaponType type)
        {
            switch (type)
            {
                case api.WeaponType.PISTOL:
                    return new Pistol(
                        _content.Load<Texture2D>("sprites/hud/scopes/pistol"),
                        Game.GetInstance().RendererFactoryInst.BuildWeaponPickUpRenderer(type),
                        _content.Load<SoundEffect>("sounds/weapons/pistol-reload"),
                        _content.Load<SoundEffect>("sounds/weapons/pistol-fire"),
                        _content.Load<SoundEffect>("sounds/weapons/empty-magazine")
                        );
                case api.WeaponType.ASSAULT_RIFLE:
                    return new AssaultRifle(
                        _content.Load<Texture2D>("sprites/hud/scopes/pistol"),
                        Game.GetInstance().RendererFactoryInst.BuildWeaponPickUpRenderer(type),
                        _content.Load<SoundEffect>("sounds/weapons/assault-rifle-reload"),
                        _content.Load<SoundEffect>("sounds/weapons/assault-rifle-fire"),
                        _content.Load<SoundEffect>("sounds/weapons/empty-magazine")
                        );
                case api.WeaponType.SNIPER_RIFLE:
                    return new SniperRifle(
                        _content.Load<Texture2D>("sprites/hud/scopes/pistol"),
                        Game.GetInstance().RendererFactoryInst.BuildWeaponPickUpRenderer(type),
                        _content.Load<SoundEffect>("sounds/weapons/sniper-rifle-reload"),
                        _content.Load<SoundEffect>("sounds/weapons/sniper-rifle-fire"),
                        _content.Load<SoundEffect>("sounds/weapons/empty-magazine")
                        );
                case api.WeaponType.HAND_GRENADE:
                    return new HandGrenadeWeapon(
                        _content.Load<Texture2D>("sprites/hud/scopes/hand-grenade"),
                        Game.GetInstance().RendererFactoryInst.BuildWeaponPickUpRenderer(type),
                        _content.Load<SoundEffect>("sounds/weapons/hand-grenade-throw")
                        );
                default:
                    throw new ArgumentException("Invalid argument value " + type);
            }
        }

    }
}
