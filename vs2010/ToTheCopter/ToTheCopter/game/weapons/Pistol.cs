﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ToTheCopter.game.api;
using Microsoft.Xna.Framework.Graphics;
using ToTheCopter.engine.inputs;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework;

namespace ToTheCopter.game.weapons
{
    class Pistol : ABulletWeapon
    {

        /*
         * private constants
         */

        // bullet
        private const float BULLET_DAMAGE = 50f;
        private const float BULLET_SPEED = 1600f;
        private const float BULLET_RADIUS = 0.5f;
        private const float BULLET_RANGE = 400f;
        private const int BULLET_PIERCING_THROUGH = 0;
        // gun
        private const float GUN_FIRE_NOISE = 500f;
        private const float GUN_SPREAD_MIN = 0.05f;
        private const float GUN_SPREAD_MAX = 0.4f;
        private const float GUN_SPREAD_INC = 0.12f;
        private const float GUN_SPREAD_DEC = 0.4f;
        private const float GUN_MAX_FIRE_DELAY = 0.1f;
        private const float GUN_MAX_RELOAD_DELAY = 2.1f;
        private const int GUN_MAGAZINE_CAPACITY = 9;
        private const bool GUN_HOLD_TO_FIRE = false;
        private const bool GUN_AUTO_RELOAD = false;

        /*
         * private attributes
         */

        public Pistol(
            Texture2D scopeSprite,
            engine.api.IEntityRenderer icon,
            SoundEffect sndReload,
            SoundEffect sndFire,
            SoundEffect sndEmptyMagazine
            )
            : base(api.WeaponType.PISTOL, icon)
        {
            _properties.BulletProperties.Damage = BULLET_DAMAGE;
            _properties.BulletProperties.Range = BULLET_RANGE;
            _properties.BulletProperties.Radius = BULLET_RADIUS;
            _properties.BulletProperties.Speed = BULLET_SPEED;
            _properties.BulletProperties.PiercingThrough = BULLET_PIERCING_THROUGH;
            _properties.FireNoise = GUN_FIRE_NOISE;
            _properties.SpreadMin = GUN_SPREAD_MIN;
            _properties.SpreadMax = GUN_SPREAD_MAX;
            _properties.SpreadInc = GUN_SPREAD_INC;
            _properties.SpreadDec = GUN_SPREAD_DEC;
            _properties.MaxFireDelay = GUN_MAX_FIRE_DELAY;
            _properties.MaxReloadDelay = GUN_MAX_RELOAD_DELAY;
            _properties.MagazineCapacity = GUN_MAGAZINE_CAPACITY;
            _properties.HoldToFire = GUN_HOLD_TO_FIRE;
            _properties.AutoReload = GUN_AUTO_RELOAD;
            _properties.ScopeSprite = scopeSprite;
            _properties.SoundReload = sndReload;
            _properties.SoundFire = sndFire;
            _properties.SoundEmptyMagazine = sndEmptyMagazine;

            _magazineLoad = _properties.MagazineCapacity;
        }

    }
}
