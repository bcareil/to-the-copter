﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework;
using ToTheCopter.engine.inputs;
using ToTheCopter.engine.utils;

namespace ToTheCopter.game.weapons
{
    class HandGrenadeWeapon : api.IThrowableWeapon
    {

        private const float RELOADING_DELAY = 0.5f;
        private const float THROWING_NOISE = 200f;

        private SoundEffect _soundThrow;
        private Timer _reloadingTimer;

        public HandGrenadeWeapon(
            Texture2D scope,
            engine.api.IEntityRenderer icon,
            SoundEffect soundThrow
            )
        {
            this._soundThrow = soundThrow;
            this._reloadingTimer = new Timer();
            this.ScopeSprite = scope;
            this.Icon = icon;
            this.Count = 2;
        }

        public bool IsThrowable { get { return true; } }

        public api.WeaponType Type { get { return api.WeaponType.HAND_GRENADE; } }

        public int Count { get; set; }

        public int MaxCount { get { return 3; } }

        public api.IPlayer Owner { get; set; }

        public Texture2D ScopeSprite { get; set; }

        public engine.api.IEntityRenderer Icon { get; set; }

        public int Ammo { get { return 1; } }

        public int MaxAmmo { get { return 1; } }

        public bool IsDepleted { get { return Count == 0; } }

        public float ReloadingRemainingPercent
        {
            get { return _reloadingTimer.Delay / RELOADING_DELAY; }
        }

        public float SpreadAngle { get { return 0f; } set { } }

        public float Noise { get; protected set; }

        public void Fire(Vector2 origin, Vector2 direction)
        {
            if (_reloadingTimer.Done && this.IsDepleted == false)
            {
                Game.GetInstance().EntityManagerInst.AddEntity(
                    Game.GetInstance().EntityFactoryInst.CreateHandGrenade(
                        origin,
                        direction,
                        400f
                        )
                    );
                Count -= 1;
                Noise = THROWING_NOISE;
                _reloadingTimer.Reset(RELOADING_DELAY);
            }
        }

        public void Reload()
        {
            _reloadingTimer.Reset(RELOADING_DELAY);
        }

        public void Update(
            InputsState inputsState,
            Vector2 fireOrigin,
            Vector2 fireDirection,
            float elapsedTime
            )
        {
            _reloadingTimer.Update(elapsedTime);
            if (inputsState.GetAction(InputAction.FIRE).IsDown())
            {
                Fire(fireOrigin, fireDirection);
            }
        }

        public void OnWeaponUnselected()
        {
            // NOTE: we dont care if the player change weapon to reload faster
            // in this case
            _reloadingTimer.Reset(0f);
        }
    }
}
