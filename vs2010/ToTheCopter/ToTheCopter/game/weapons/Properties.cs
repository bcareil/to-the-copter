﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Audio;

namespace ToTheCopter.game.weapons
{
    struct BulletProperties
    {
        public float Damage;
        public float Range;
        public float Radius;
        public float Speed;
        public int PiercingThrough;
    }

    struct BulletWeaponProperties
    {
        public BulletProperties BulletProperties;
        public float FireNoise;
        public float SpreadMin;
        public float SpreadMax;
        public float SpreadInc;
        public float SpreadDec;
        public float MaxFireDelay;
        public float MaxReloadDelay;
        public int MagazineCapacity;
        public bool HoldToFire;
        public bool AutoReload;
        public Texture2D ScopeSprite;
        public SoundEffect SoundReload;
        public SoundEffect SoundFire;
        public SoundEffect SoundEmptyMagazine;
    }

}
