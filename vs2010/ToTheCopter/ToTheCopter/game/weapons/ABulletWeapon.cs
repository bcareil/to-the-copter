﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ToTheCopter.game.api;
using Microsoft.Xna.Framework.Graphics;
using ToTheCopter.engine.inputs;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework;
using ToTheCopter.engine.utils;

namespace ToTheCopter.game.weapons
{
    class ABulletWeapon : IWeapon
    {

        /*
         * private attributes
         */

        protected BulletWeaponProperties _properties;
        protected Timer _fireTimer;
        protected Timer _reloadTimer;
        protected Timer _emptyMagazineSoundTimer;
        protected int _magazineLoad;
        protected bool _reloading;

        public ABulletWeapon(api.WeaponType weaponType, engine.api.IEntityRenderer icon)
        {
            _properties = new BulletWeaponProperties();
            _fireTimer = new Timer();
            _reloadTimer = new Timer();
            _emptyMagazineSoundTimer = new Timer();
            _magazineLoad = _properties.MagazineCapacity;
            _reloading = false;
            this.Type = weaponType;
            this.Owner = null;
            this.Icon = icon;
            this.SpreadAngle = 0.1f;
        }

        public bool IsThrowable { get { return false; } }

        public api.WeaponType Type { get; private set; }

        public IPlayer Owner { get; set; }

        public Texture2D ScopeSprite { get { return _properties.ScopeSprite; } set { _properties.ScopeSprite = value; } }

        public engine.api.IEntityRenderer Icon { get; set; }

        public int Ammo { get { return _magazineLoad; } }

        public int MaxAmmo { get { return _properties.MagazineCapacity; } }

        public float ReloadingRemainingPercent { get { return _reloadTimer.Delay / _properties.MaxReloadDelay; } }

        public float SpreadAngle { get; set; }

        public float Noise { get; protected set; }

        public bool IsDepleted { get { return false; } } // TODO

        public void Fire(Vector2 origin, Vector2 direction)
        {
            Random rand;
            float angle;

            rand = new Random();
            if (_fireTimer.Done == false || _reloadTimer.Done == false)
            {
                return;
            }
            if (_magazineLoad == 0)
            {
                if (_emptyMagazineSoundTimer.Done)
                {
                    _emptyMagazineSoundTimer.Reset(0.1f);
                    _properties.SoundEmptyMagazine.Play();
                    Reload();
                }
                return;
            }
            _fireTimer.Reset(_properties.MaxFireDelay);
            _magazineLoad -= 1;
            _properties.SoundFire.Play();
            // compute spread angle and apply it
            angle = SpreadAngle * (((float)rand.NextDouble()) - 0.5f) * 2f;
            direction = new Vector2(
                direction.X * (float)Math.Cos(angle) - direction.Y * (float)Math.Sin(angle),
                direction.X * (float)Math.Sin(angle) + direction.Y * (float)Math.Cos(angle)
                );
            // create the bullet
            Game.GetInstance().EntityManagerInst.AddEntity(
                Game.GetInstance().EntityFactoryInst.CreateBullet(
                Owner,
                origin,
                direction,
                _properties.BulletProperties.Damage,
                _properties.BulletProperties.Speed,
                _properties.BulletProperties.Radius,
                _properties.BulletProperties.Range,
                _properties.BulletProperties.PiercingThrough
                ));
            // increase the spread angle
            SpreadAngle += _properties.SpreadInc;
            if (SpreadAngle > _properties.SpreadMax)
            {
                SpreadAngle = _properties.SpreadMax;
            }
            // noise !
            Noise = _properties.FireNoise;
            // auto reload
            if (_properties.AutoReload && _magazineLoad == 0)
            {
                Reload();
            }
        }

        public void Reload()
        {
            if (_reloadTimer.Done == false || _magazineLoad == _properties.MagazineCapacity)
            {
                return;
            }
            _reloadTimer.Reset(_properties.MaxReloadDelay);
            _properties.SoundReload.Play();
            _reloading = true;
        }

        public void Update(InputsState inputsState, Vector2 fireOrigin, Vector2 fireDirection, float elapsedTime)
        {
            Noise = 0f;
            // update timers
            _fireTimer.Update(elapsedTime);
            _reloadTimer.Update(elapsedTime);
            _emptyMagazineSoundTimer.Update(elapsedTime);
            // reload
            if (_reloading && _reloadTimer.Done)
            {
                _reloading = false;
                _magazineLoad = _properties.MagazineCapacity;
            }
            // spread
            SpreadAngle -= _properties.SpreadDec * elapsedTime;
            if (SpreadAngle < _properties.SpreadMin)
            {
                SpreadAngle = _properties.SpreadMin;
            }
            // fire/reload
            if (_properties.HoldToFire && inputsState.GetAction(InputAction.FIRE).IsDown()
                || inputsState.GetAction(InputAction.FIRE) == InputState.PRESSED)
            {
                Fire(fireOrigin, fireDirection);
            }
            if (inputsState.GetAction(InputAction.RELOAD).IsDown())
            {
                Reload();
            }
        }

        public void OnWeaponUnselected()
        {
            _reloadTimer.Reset(0f);
            _reloading = false;
        }
    }
}
