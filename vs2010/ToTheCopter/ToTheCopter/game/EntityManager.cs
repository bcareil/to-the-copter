﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ToTheCopter.game.api;
using Microsoft.Xna.Framework;
using ToTheCopter.engine.utils;

namespace ToTheCopter.game
{
    class EntityManager
    {
        private collision.CollisionMediator _collisionMediator;
        private ParticleManager             _particleManager;
        private ICollection<IGameEntity>    _pickUp;
        private ICollection<IGameEntity>    _bullets;
        private ICollection<IGameEntity>    _players;
        private ICollection<IGameEntity>    _enemies;
        private ICollection<IGameEntity>    _walls;
        private ICollection<IGameEntity>    _areas;
        private ICollection<IGameEntity>    _copters;
        private ICollection<IGameEntity>    _delayedEntities;
        private bool                        _updating;

        public EntityManager(Point mapDimensions)
        {
            _collisionMediator = new collision.CollisionMediator(mapDimensions);
            _particleManager = new ParticleManager();
            _pickUp = new List<IGameEntity>();
            _bullets = new LinkedList<IGameEntity>();
            _players = new List<IGameEntity>();
            _enemies = new LinkedList<IGameEntity>();
            _walls = new List<IGameEntity>();
            _areas = new LinkedList<IGameEntity>();
            _copters = new LinkedList<IGameEntity>();
            _delayedEntities = new LinkedList<IGameEntity>();
            _updating = false;
        }

        public void AddEntity(IGameEntity entity)
        {
            if (_updating)
            {
                _delayedEntities.Add(entity);
                return;
            }
            entity.CollisionMediator = _collisionMediator;
            switch (entity.Type)
            {
                case EntityType.PARTICLE:
                    _particleManager.AddParticle(entity);
                    break;
                case EntityType.PICKUP:
                    _pickUp.Add(entity);
                    break;
                case EntityType.BULLET:
                    _bullets.Add(entity);
                    break;
                case EntityType.PLAYER:
                    _players.Add(entity);
                    break;
                case EntityType.ENEMY:
                    _enemies.Add(entity);
                    break;
                case EntityType.WALL:
                    _walls.Add(entity);
                    break;
                case EntityType.AREA:
                    _areas.Add(entity);
                    break;
                case EntityType.COPTER:
                    _copters.Add(entity);
                    break;
                default:
                    throw new ArgumentException("Invalid entity type " + entity.Type);
            }
        }

        public void Update(float elapsed, float totalElapsed)
        {
            _updating = true;
            // update of particles is unconditional
            _particleManager.Update(elapsed, totalElapsed);
            _collisionMediator.UpdateEntities(EntityType.PICKUP, _pickUp);
            _collisionMediator.UpdateEntities(EntityType.PLAYER, _players);
            _collisionMediator.UpdateEntities(EntityType.ENEMY, _enemies);
            _collisionMediator.UpdateEntities(EntityType.COPTER, _copters);
            _collisionMediator.UpdateEntities(EntityType.BULLET, _bullets);

            UpdateCollection(_pickUp, elapsed, totalElapsed);
            UpdateCollection(_bullets, elapsed, totalElapsed);
            UpdateCollection(_players, elapsed, totalElapsed);
            UpdateCollection(_enemies, elapsed, totalElapsed);
            UpdateCollection(_walls, elapsed, totalElapsed);
            UpdateCollection(_areas, elapsed, totalElapsed);
            UpdateCollection(_copters, elapsed, totalElapsed);
            _updating = false;
            foreach (IGameEntity entity in _delayedEntities)
            {
                AddEntity(entity);
            }
            _delayedEntities.Clear();
        }

        public void Draw(Microsoft.Xna.Framework.Graphics.SpriteBatch sb, Rectf scene)
        {
            scene.X -= 64;
            scene.Y -= 64;
            scene.Width += 128;
            scene.Height += 128;
            _particleManager.Draw(sb, scene.Center);
            DrawCollection(api.EntityType.PICKUP, sb, scene);
            DrawCollection(api.EntityType.BULLET, sb, scene);
            DrawCollection(api.EntityType.ENEMY, sb, scene);
            DrawCollection(api.EntityType.PLAYER, sb, scene);
            DrawCollection(api.EntityType.WALL, sb, scene);
            DrawCollection(api.EntityType.AREA, sb, scene);
            DrawCollection(api.EntityType.COPTER, sb, scene);
        }

        /// <summary>
        /// Used to say that all the walls have been added and can be passed to the collision mediator
        /// </summary>
        public void CommitStaticEntities()
        {
            _collisionMediator.UpdateEntities(EntityType.WALL, _walls);
            _collisionMediator.UpdateEntities(EntityType.AREA, _areas);
        }

        /*
         * private methods
         */

        private void UpdateCollection(ICollection<IGameEntity> collection, float elapsed, float totalElapsed)
        {
            // TODO: optimize entity removal
            ICollection<IGameEntity> toRemove;

            toRemove = new LinkedList<IGameEntity>();
            foreach (IGameEntity entity in collection)
            {
                entity.Update(elapsed, totalElapsed);
                if (entity.Remove)
                {
                    toRemove.Add(entity);
                }
            }
            foreach (IGameEntity entity in toRemove)
            {
                collection.Remove(entity);
            }
        }

        private void DrawCollection(
            api.EntityType entityType,
            Microsoft.Xna.Framework.Graphics.SpriteBatch sb,
            Rectf scene
            )
        {
            foreach (IGameEntity entity in _collisionMediator.GetEntitiesInArea(scene, new EntityType[] { entityType }))
            {
                entity.Draw(sb);
            }
        }

    }
}
