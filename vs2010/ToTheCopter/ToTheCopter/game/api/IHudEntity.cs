﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ToTheCopter.game.api
{
    interface IHudEntity : engine.api.IDrawable, engine.api.IUpdatable
    {
        bool Hide { get; set; }
        bool Remove { get; set; }
    }
}
