﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using ToTheCopter.map;

namespace ToTheCopter.game.api
{
    interface IPlayerScreen : engine.api.IUpdatable
    {
        api.IPlayer Player { get; }
        Microsoft.Xna.Framework.Vector2 Translation { get; set; }
        engine.utils.Rectf SceneArea { get; }

        void Draw(SpriteBatch sb, EntityManager entityManager);

    }
}
