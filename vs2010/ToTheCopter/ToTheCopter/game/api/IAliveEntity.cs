﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ToTheCopter.game.api
{
    interface IAliveEntity : IGameEntity
    {
        float MaxHP { get; set; }
        float HP { get; set; }
        bool IsAlive { get; }

        void TakeDamage(float amount);
    }
}
