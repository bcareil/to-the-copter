﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ToTheCopter.engine;
using ToTheCopter.engine.utils;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace ToTheCopter.game.api
{
    enum EntityType
    {
        PARTICLE,   // No specific entity type -> no collision check
        PICKUP,     // Items the players can pick-up -> passive collide check
        BULLET,     // Bullet or throwable -> collide check with players, enemies and walls
        PLAYER,     // Player -> collide check with pick-up, player, enemies, walls and copter
        ENEMY,      // Enemy -> collide check with walls, players, enemies and copter
        WALL,       // all kind of obstacle -> passive collide check
        AREA,       // area for event purpose -> collide with player
        COPTER,     // copter -> no collision check

        COUNT
    };

    enum ZAxisValue
    {
        GROUND          = 0,
        KNEE            = 1,
        WAIST           = 2,
        ELBOW           = 3,
        SHOULDER        = 4,
        SIGHT           = 5,
        HUMAN_HEIGHT    = 6,
        H7              = 7,
        H8              = 8,
        H9              = 9,
        H10             = 10,
        H11             = 11,
        H12             = 12,
        H13             = 13,
        H14             = 14,
        H15             = 15,
        SKY             = 16
    };

    interface IGameEntity : engine.api.IUpdatable, engine.api.IMoveable, engine.api.IDrawable
    {
        Rectf CollideRect { get; set; }
        ICollisionMediator CollisionMediator { get; set; }
        OneDimensionalSegmentInt ZAxis { get; set; }
        bool Remove { get; set; }
        EntityType Type { get; }

        /// <summary>
        /// The entity enter in collision with the one recieve as first parameter
        /// </summary>
        /// <param name="oth">The colliding entity</param>
        void Collide(IGameEntity oth);

        /// <summary>
        /// Check if the entity collide with the given one. The caller will then call
        /// Collide(IGameEntity) if needed.
        /// </summary>
        /// <param name="oth">The entity to check the collision with</param>
        /// <returns>True if the entities are colliding</returns>
        bool IsCollidingWith(IGameEntity oth);
    }
}
