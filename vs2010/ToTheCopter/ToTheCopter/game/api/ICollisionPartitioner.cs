﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ToTheCopter.engine.utils;

namespace ToTheCopter.game.api
{
    interface ICollisionPartitioner
    {
        void Feed(ICollection<IGameEntity> entities);
        void AddEntity(IGameEntity entity);
        void RemoveEntity(IGameEntity entity);
        void Clear();
        ICollection<IGameEntity> GetEntitiesInArea(Rectf area);
    }
}
