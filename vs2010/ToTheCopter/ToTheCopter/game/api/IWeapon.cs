﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using ToTheCopter.engine.inputs;
using Microsoft.Xna.Framework;

namespace ToTheCopter.game.api
{
    enum WeaponType
    {
        PISTOL,
        ASSAULT_RIFLE,
        SHOTGUN,
        FLAME_THROWER,
        SNIPER_RIFLE,
        HAND_GRENADE,

        COUNT
    }

    interface IWeapon
    {
        bool IsThrowable { get; }
        WeaponType Type { get; }
        IPlayer Owner { get; set; }
        Texture2D ScopeSprite { get; set; }
        engine.api.IEntityRenderer Icon { get; set; }
        int Ammo { get; }
        int MaxAmmo { get; }
        bool IsDepleted { get; }
        float ReloadingRemainingPercent { get; }
        float SpreadAngle { get; set; }

        /// <summary>
        /// The current noise made by the weapon
        /// </summary>
        float Noise { get; }

        void Fire(Vector2 origin, Vector2 direction);
        void Reload();
        void Update(InputsState inputsState, Vector2 fireOrigin, Vector2 fireDirection, float elapsedTime);
        void OnWeaponUnselected();

    }
}
