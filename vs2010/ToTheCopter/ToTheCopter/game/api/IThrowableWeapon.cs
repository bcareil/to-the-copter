﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ToTheCopter.game.api
{
    interface IThrowableWeapon : IWeapon
    {

        int Count { get; set; }
        int MaxCount { get; }

    }
}
