﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ToTheCopter.game.api
{

    delegate void OnWeaponChanged(IPlayer player, IWeapon newWeapon);

    class PlayerConstants
    {
        public const int WEAPON_IDX_START = 0;
        public const int WEAPON_IDX_MAIN1 = 1;
        public const int WEAPON_IDX_MAIN2 = 2;
        public const int WEAPON_IDX_THROWABLE = 3;
        public const int NB_WEAPON_SLOTS = 4;
    }

    interface IPlayer : IAliveEntity
    {

        event OnWeaponChanged WeaponChanged;

        int PlayerNo { get; }
        IWeapon Weapon { get; }
        IWeapon[] WeaponList { get; }
        float LastTimeHurt { get; }
        float Stamina { get; set; }
        float MaxStamina { get; set; }
        bool Exhausted { get; set; }

        bool PickUp(IWeapon weapon);
        void Regen(float hp);
        void SetWeapon(int idx, IWeapon weapon);
        void Succeed();
    }
}
