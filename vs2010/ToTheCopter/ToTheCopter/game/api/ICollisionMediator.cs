﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using ToTheCopter.engine.utils;

namespace ToTheCopter.game.api
{
    interface ICollisionMediator
    {
        Vector2 HandleMovementFor(IGameEntity entity, Vector2 movement, api.EntityType[] collidingEntityTypes);

        List<IGameEntity> GetEntitiesInArea(Rectf CollideRect, EntityType[] entityType);

        void OrderedCollisionCheck(IGameEntity entity, Rectf area, Vector2 origin, EntityType[] collidingEntitieTypes);
    }
}
