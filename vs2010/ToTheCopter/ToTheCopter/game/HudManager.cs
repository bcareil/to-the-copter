﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ToTheCopter.game
{
    class HudManager : engine.api.IUpdatable, engine.api.IDrawable
    {
        private ICollection<api.IHudEntity> _hudEntities;

        public HudManager()
        {
            _hudEntities = new LinkedList<api.IHudEntity>();
        }

        public void Add(api.IHudEntity entity)
        {
            _hudEntities.Add(entity);
        }

        public void Remove(api.IHudEntity entity)
        {
            _hudEntities.Remove(entity);
        }

        public void Update(float elapsed, float totalElapsed)
        {
            foreach (api.IHudEntity entity in _hudEntities)
            {
                entity.Update(elapsed, totalElapsed);
            }
        }

        public void Draw(Microsoft.Xna.Framework.Graphics.SpriteBatch sb)
        {
            foreach (api.IHudEntity entity in _hudEntities)
            {
                entity.Draw(sb);
            }
        }
    }
}
