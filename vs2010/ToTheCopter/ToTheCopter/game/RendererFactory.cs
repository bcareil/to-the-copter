﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using ToTheCopter.engine.utils;

namespace ToTheCopter.game
{
    class RendererFactory
    {
        private ContentManager _content;
        private Texture2D _spriteEntitiesCopter;
        private Texture2D _spriteEntitiesCopterBladesRotating;
        private Texture2D _spriteEntitiesEnemy;
        private Texture2D _spriteEntitiesHandGrenade;
        private Texture2D _spriteEntitiesPlayer;
        private Texture2D _spriteHudGauge;
        private Texture2D _spriteHudWeaponSlot;
        private Texture2D _spriteParticlesExplosion;
        private Texture2D _spritePickupSupply;
        private Texture2D _spritePickupWeapon;
        private Texture2D _spritePrimitiveSquare;

        public RendererFactory(ContentManager content)
        {
            _content = content;
            _spriteEntitiesCopter = _content.Load<Texture2D>("sprites/entities/copter");
            _spriteEntitiesCopterBladesRotating = _content.Load<Texture2D>("sprites/entities/copter-blades-rotating");
            _spriteEntitiesEnemy = _content.Load<Texture2D>("sprites/entities/player");
            _spriteEntitiesHandGrenade = _content.Load<Texture2D>("sprites/entities/hand-grenade");
            _spriteEntitiesPlayer = _content.Load<Texture2D>("sprites/entities/player");
            _spriteHudGauge = _content.Load<Texture2D>("sprites/hud/gauge");
            _spriteHudWeaponSlot = _content.Load<Texture2D>("sprites/hud/weapon-slot");
            _spriteParticlesExplosion = _content.Load<Texture2D>("sprites/particles/explosion");
            _spritePickupSupply = _content.Load<Texture2D>("sprites/pickup/supply");
            _spritePickupWeapon = _content.Load<Texture2D>("sprites/pickup/weapons");
            _spritePrimitiveSquare = _content.Load<Texture2D>("sprites/primitives/square");
        }

        public engine.api.IEntityRenderer BuildLineRenderer(Vector2 position, Vector2 direction, float width, float length, Color color)
        {
            engine.api.IEntityRenderer renderer;

            direction.Normalize();
            renderer = CreateRectangleRenderer();
            renderer.SpriteDimensionsDestination = new Point((int)length, (int)width);
            renderer.Center = position + direction * (length / 2);
            renderer.Direction = direction;
            renderer.OverrideColor = color;
            return renderer;
        }

        public engine.api.IEntityRenderer BuildRectangleRenderer(Rectf rectangle, Color color)
        {
            engine.api.IEntityRenderer renderer;

            renderer = CreateRectangleRenderer();
            renderer.SpriteDimensionsDestination = new Point((int)rectangle.Width, (int)rectangle.Height);
            renderer.Center = rectangle.Center;
            renderer.OverrideColor = color;
            return renderer;
        }

        public engine.api.IEntityRenderer CreateRectangleRenderer()
        {
            return new engine.gfx.BasicEntityRenderer(
                _spritePrimitiveSquare,
                new Point(0, 0),
                new Point(1, 1),
                new Vector2(0.5f, 0.5f),
                new Vector2(0, -1),
                new Vector2(0, 0),
                Color.White
                );
        }

        public engine.api.IEntityRenderer CreatePlayerRenderer()
        {
            return new engine.gfx.BasicEntityRenderer(
                    _spriteEntitiesPlayer,
                    new Point(0, 0),
                    new Point(64, 64),
                    new Vector2(32, 32),
                    new Vector2(0, -1),
                    new Vector2(),
                    Color.White
                    );

        }

        internal engine.api.IEntityRenderer CreateEnemyRenderer()
        {
            return new engine.gfx.BasicEntityRenderer(
                   _spriteEntitiesEnemy,
                   new Point(0, 0),
                   new Point(64, 64),
                   new Vector2(32, 32),
                   new Vector2(0, -1),
                   new Vector2(),
                   Color.Green
                   );
        }


        internal engine.api.IEntityRenderer BuildWeaponPickUpRenderer(api.WeaponType weaponType)
        {
            return new engine.gfx.BasicEntityRenderer(
                _spritePickupWeapon,
                new Point(((int)weaponType % 4) * 64, ((int)weaponType / 4) * 64),
                new Point(64, 64),
                new Vector2(32, 32),
                new Vector2(1, 0),
                new Vector2(0, 0),
                Color.White
                );
        }

        internal engine.api.IEntityRenderer CreateGaugeRenderer()
        {
            return new engine.gfx.BasicEntityRenderer(
                _spriteHudGauge,
                new Point(0, 0),
                new Point(24, 64),
                new Vector2(12, 32),
                new Vector2(1, 0),
                new Vector2(0, 0),
                Color.White
                );
        }

        internal engine.api.IEntityRenderer CreateHandGrenadeEntityRenderer()
        {
            return new engine.gfx.BasicEntityRenderer(
                _spriteEntitiesHandGrenade,
                new Point(0, 0),
                new Point(16, 16),
                new Vector2(8, 8),
                new Vector2(1, 0),
                new Vector2(0, 0),
                Color.White
                );
        }

        internal engine.api.IEntityRenderer CreateHelicopterRenderer()
        {
            return new engine.gfx.BasicEntityRenderer(
                _spriteEntitiesCopter,
                new Point(0, 0),
                new Point(256, 256),
                new Vector2(95, 128),
                new Vector2(1, 0),
                new Vector2(0, 0),
                Color.White
                );
        }

        internal engine.api.IEntityRenderer CreateHelicopterBladesRenderer()
        {
            return new engine.gfx.BasicEntityRenderer(
                _spriteEntitiesCopterBladesRotating,
                new Point(0, 0),
                new Point(256, 256),
                new Vector2(95, 128),
                new Vector2(1, 0),
                new Vector2(0, 0),
                Color.White
                );
        }

        internal engine.api.IEntityRenderer CreateExplosionRenderer(float delay)
        {
            return new engine.gfx.AnimatedEntityRenderer(
                _spriteParticlesExplosion,
                new Point(4, 4),
                new Point(0, 0),
                new Point(64, 64),
                new Vector2(32, 32),
                new Vector2(1, 0),
                new Vector2(0, 0),
                Color.White,
                delay
                );
        }

        internal engine.api.IEntityRenderer BuildHealthKitPickUpRenderer(int subType)
        {
            return new engine.gfx.BasicEntityRenderer(
                _spritePickupSupply,
                new Point(subType * 64, subType / 64),
                new Point(64, 64),
                new Vector2(32, 32),
                new Vector2(1, 0),
                new Vector2(0, 0),
                Color.White
                );
        }

        internal engine.api.IEntityRenderer CreateHudWeaponSlotRenderer()
        {
            return new engine.gfx.BasicEntityRenderer(
                _spriteHudWeaponSlot,
                new Point(2, 0),
                new Point(126, 99),
                new Vector2(126 / 2f, 99 / 2f),
                new Vector2(1, 0),
                new Vector2(0, 0),
                Color.White
                );
        }
    }
}
