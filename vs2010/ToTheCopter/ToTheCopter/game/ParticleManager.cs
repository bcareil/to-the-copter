﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace ToTheCopter.game
{
    class ParticleManager : engine.api.IUpdatable, engine.api.IDrawableEntity
    {
        private const int MAX_PARTICLES = 2000;
        private ICollection<api.IGameEntity> _particles;

        public ParticleManager()
        {
            _particles = new LinkedList<api.IGameEntity>();
        }

        public void AddParticle(api.IGameEntity entity)
        {
            _particles.Add(entity);
        }

        public void Draw(Microsoft.Xna.Framework.Graphics.SpriteBatch sb, Vector2 translation)
        {
            foreach (api.IGameEntity particle in _particles)
            {
                particle.Draw(sb);
            }
        }

        public void Update(float elapsed, float totalElapsed)
        {
            // TODO: optimize entity removal
            ICollection<api.IGameEntity> toRemove;

            toRemove = new LinkedList<api.IGameEntity>();
            foreach (api.IGameEntity particle in _particles)
            {
                particle.Update(elapsed, totalElapsed);
                if (particle.Remove)
                {
                    toRemove.Add(particle);
                }
            }
            foreach (api.IGameEntity particle in toRemove)
            {
                _particles.Remove(particle);
            }
        }
    }
}
