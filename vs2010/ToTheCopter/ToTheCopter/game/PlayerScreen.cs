﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using ToTheCopter.map;
using Microsoft.Xna.Framework.Content;
using ToTheCopter.engine.gfx;
using Microsoft.Xna.Framework.Graphics;

namespace ToTheCopter.game
{

    delegate void OnPlayerSuccess(api.IPlayerScreen playerScreen);
    delegate void OnPlayerDeath(api.IPlayerScreen playerScreen);

    class PlayerScreen : api.IPlayerScreen
    {

        private OnPlayerSuccess _onPlayerSuccess;
        private OnPlayerDeath _onPlayerDeath;
        private Viewport _viewport;
        private Point _screenDimensions;
        private hud.HudFactory _hudFactory;
        private api.IPlayer _player;
        private api.IScope _scope;
        private HudManager _hud;
        private BufferedCamera _playerCamera;
        private MapRenderer _mapRenderer;
        private Effect _effectPlayerHealth;
        private Effect _effectPlayerHurt;

        public PlayerScreen(
            OnPlayerSuccess onPlayerSuccess,
            OnPlayerDeath onPlayerDeath,
            ContentManager content,
            Map map,
            Rectangle screenDimensions,
            int playerNo)
        {
            _onPlayerSuccess = onPlayerSuccess;
            _onPlayerDeath = onPlayerDeath;
            _viewport = new Viewport(screenDimensions);
            _viewport.MinDepth = 0;
            _viewport.MaxDepth = 1;
            _screenDimensions = new Point(screenDimensions.Width, screenDimensions.Height);
            _hudFactory = new hud.HudFactory(content, _screenDimensions);
            _player = Game.GetInstance().EntityFactoryInst.CreatePlayer(this.OnPlayerSuccess, _screenDimensions, map.GetPlayerStartPosition(playerNo), playerNo);
            _scope = _hudFactory.CreateScope(this, api.ScopeType.MOUSE);
            _hud = new HudManager();
            _playerCamera = new BufferedCamera(new Vector2(800, 600), _player.CollideRect.Center, new Vector2(1, 1));
            _mapRenderer = new MapRenderer(_screenDimensions, map);
            _effectPlayerHealth = content.Load<Effect>("effects/player-health");
            _effectPlayerHurt = content.Load<Effect>("effects/player-hurt");

            _hud.Add(_scope);
            _hud.Add(_hudFactory.CreateWeaponList(_player));
            _player.SetWeapon(api.PlayerConstants.WEAPON_IDX_START, Game.GetInstance().WeaponFactoryInst.CreateWeapon(api.WeaponType.PISTOL));
            _playerCamera.AddPostEffect(_effectPlayerHealth);
            _playerCamera.AddPostEffect(_effectPlayerHurt);
        }

        public api.IPlayer Player { get { return _player; } }

        public Vector2 Translation { get { return _playerCamera.Translation; } set { _playerCamera.Translation = value; } }

        public engine.utils.Rectf SceneArea { get { return _playerCamera.SceneArea; } }

        public void Draw(Microsoft.Xna.Framework.Graphics.SpriteBatch sb, EntityManager entityManager)
        {
            MyGame.GetInstance().Graphics.GraphicsDevice.Viewport = _viewport;
            _playerCamera.Begin(sb);
            _mapRenderer.Draw(sb, Translation);
            entityManager.Draw(sb, _playerCamera.SceneArea);
#if DEBUG && FALSE
            Game.GetInstance().AIMapInst.Draw(sb, _playerCamera.SceneArea);
#endif
            _playerCamera.End(sb);
            sb.Begin();
            _hud.Draw(sb);
            sb.End();
        }

        public void Update(float elapsed, float totalElapsed)
        {
            _hud.Update(elapsed, totalElapsed);
            Translation = _playerCamera.ScreenCenter - _player.CollideRect.Center;
            _effectPlayerHurt.Parameters["time"].SetValue(1f - Math.Min(1f, _player.LastTimeHurt));
            _effectPlayerHealth.Parameters["playerHealth"].SetValue(_player.HP / _player.MaxHP);
            if (_player.IsAlive == false)
            {
                this._onPlayerDeath(this);
            }
        }

        /*
         * private methods
         */

        private void OnPlayerSuccess(api.IPlayer player)
        {
            this._onPlayerSuccess(this);
        }

    }
}
