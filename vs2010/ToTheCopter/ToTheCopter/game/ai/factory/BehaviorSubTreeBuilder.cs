﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ToTheCopter.ai.api;
using ToTheCopter.ai.factory;

namespace ToTheCopter.game.ai.factory
{
    class BehaviorSubTreeBuilder
    {
        private ICompositeBehavior _product;

        public BehaviorSubTreeBuilder()
        {
            _product = null;
        }

        public void BuildCompositNode(String name, AINodeFactory.CompositeBehaviorTypes type)
        {
            _product = AINodeFactory.CreateCompositBehavior(name, type);
        }

        public void BuildChildMove(String name)
        {
            this.AddChild(new nodes.AIBMove(name));
        }

        public void BuildChildRandomMovement(String name)
        {
            this.AddChild(new nodes.AIBRandomMovement(name));
        }

        public void AddChild(IBehavior childBehavior)
        {
            _product.AddChild(childBehavior);
        }

        public ICompositeBehavior GetProduct()
        {
            return _product;
        }

        public IRootBehavior GetProductAsRoot(String name)
        {
            IRootBehavior root;

            root = AINodeFactory.CreateRootBehaviorFromBehavior(name, _product);
            return root;
        }
    }
}
