﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ToTheCopter.ai.api;

namespace ToTheCopter.game.ai.factory
{
    static class GameAINodeFactory
    {

        public static IBehavior CreateMoveBehavior(String name)
        {
            return new nodes.AIBMove(name);
        }

        public static IBehavior CreateRandomMovementBehavior(String name)
        {
            return new nodes.AIBRandomMovement(name);
        }

        public static IBehavior CreateBumpWatchDogBehavior(String name)
        {
            return new nodes.sensors.AIBSensorBump(name);
        }
        
    }
}
