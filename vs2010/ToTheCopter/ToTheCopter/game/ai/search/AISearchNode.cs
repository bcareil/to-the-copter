﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ToTheCopter.engine.algorithm.search.api;
using Microsoft.Xna.Framework;

namespace ToTheCopter.game.ai.search
{
    class AISearchNode : ISearchNode
    {
        private ISearchNode _parent;
        private int _depth;
        private Microsoft.Xna.Framework.Point _position;

        public AISearchNode(ISearchNode parent, Point position)
        {
            this._parent = parent;
            this._depth = 0;
            this._position = position;

            if (this._parent != null)
            {
                this._depth = this._parent.Depth + 1;
            }
        }

        public int Depth { get { return _depth; } }

        public ICollection<ISearchNode> GetPathFromRoot()
        {
            LinkedList<ISearchNode> path;

            if (_parent == null)
            {
                return new LinkedList<ISearchNode>();
            }
            path = (LinkedList<ISearchNode>)_parent.GetPathFromRoot();
            path.AddLast(this);
            return path;
        }

        public Point GetPosition()
        {
            return _position;
        }

        public override bool Equals(object obj)
        {
            if (obj is AISearchNode)
            {
                AISearchNode oth;

                oth = (AISearchNode)obj;
                return (_position.X == oth._position.X && _position.Y == oth._position.Y);
            }
            return false;
        }

        public override int GetHashCode()
        {
            return 51 * (_position.X + _position.Y);
        }
    }
}
