﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ToTheCopter.engine.algorithm.search.api;
using ToTheCopter.game.ai.data;
using Microsoft.Xna.Framework;

namespace ToTheCopter.game.ai.search
{
    class AISearchProblem :ISearchProblem
    {
        private Point _start;
        private Point _goal;
        private AIMap _aiMapRef;
        private int _maxIteration;

        public AISearchProblem(Vector2 start, Vector2 goal, AIMap aiMap, int maxIteration)
        {
            _start = aiMap.ExternalCoordinatesToInternal(start);
            _goal = aiMap.ExternalCoordinatesToInternal(goal);
            _aiMapRef = aiMap;
            _maxIteration = maxIteration;
        }

        /*
         * public implementation
         */

        public bool IsGoal(ISearchNode node)
        {
            AISearchNode searchNode;

            searchNode = (AISearchNode)node;
            return (_goal == searchNode.GetPosition());
        }

        public ICollection<ISearchNode> ExpandNode(ISearchNode node)
        {
            ICollection<ISearchNode> newNodes;
            ICollection<Point> reachablePoints;
            AISearchNode searchNode;

            searchNode = (AISearchNode)node;
            newNodes = new List<ISearchNode>();
            //reachablePoints = _aiMapRef.GetReachablePointsFrom(searchNode.GetPosition());
            //newNodes = new List<ISearchNode>(reachablePoints.Count);
            //foreach (Point p in reachablePoints)
            //{
            //    newNodes.Add(new AISearchNode(node, p));
            //}
            return newNodes;
        }

        public float NodeHeuristic(ISearchNode node)
        {
            AISearchNode searchNode;
            Point searchNodePosition;

            searchNode = (AISearchNode)node;
            searchNodePosition = searchNode.GetPosition();
            return (
                node.Depth +
                new Vector2(_goal.X - searchNodePosition.X, _goal.Y - searchNodePosition.Y).Length()
                );
        }

        public bool HasHitIterationLimit()
        {
            return ((--_maxIteration) > 0);
        }

        public ISearchNode GetStartNode()
        {
            return new AISearchNode(null, _start);
        }

        /*
         * public methods
         */

        public ICollection<Vector2> NodeListToCoordinateList(ICollection<ISearchNode> nodes)
        {
            List<Vector2> coordinates;
            Vector2 toTheMiddle;

            coordinates = new List<Vector2>(nodes.Count);
            toTheMiddle = new Vector2(_aiMapRef.SquareEdgeLength / 2, _aiMapRef.SquareEdgeLength / 2);
            foreach (ISearchNode node in nodes)
            {
                AISearchNode aiSearchNode;

                aiSearchNode = (AISearchNode)node;
                coordinates.Add(_aiMapRef.InternalCoordinatesToExternal(aiSearchNode.GetPosition()) + toTheMiddle);
            }
            return coordinates;
        }
    }
}
