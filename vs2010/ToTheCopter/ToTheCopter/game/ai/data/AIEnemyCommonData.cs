﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace ToTheCopter.game.ai.data
{
    class AIEnemyCommonData
    {

        public AIEnemyCommonData(AIEnemySensorVector sensorVector)
        {
            Target = new Vector2();
            MovingSpeed = 0f;
            Moving = false;
            SensorVector = sensorVector;
            Path = null;
        }

        public Vector2 Target { get; set; }

        public float MovingSpeed { get; set; }

        public AIEnemySensorVector SensorVector { get; private set; }

        public bool Moving { get; set; }

        public ICollection<Vector2> Path { get; set; }

#if DEBUG
        public ICollection<Vector2> DebugPath { get; set; }
#endif

    }
}
