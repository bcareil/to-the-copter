﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ToTheCopter.engine.utils;
using Microsoft.Xna.Framework;

namespace ToTheCopter.game.ai.data
{
    class OldAIMap
    {

        private byte[] _map;
        private int _powerOfTwo;
        private int _width;
        private int _height;

        public OldAIMap(int width, int height, int powerOfTwo)
        {
            _powerOfTwo = powerOfTwo;
            _width = width;
            _height = height;
            _map = new byte[_width * _height];

            for (int i = 0; i < _map.Length; ++i)
            {
                _map[i] = 1;
            }
        }

        /*
         * public properties
         */

        public int SquareEdgeLength { get { return 1 << _powerOfTwo; } }

        /*
         * public methods
         */

        public void BlacklistArea(Rectf area)
        {
            Rectangle internalMapArea;

            internalMapArea = ExternalMapAreaToInternal(area);
            for (int x = internalMapArea.X; x < internalMapArea.Right; ++x)
            {
                for (int y = internalMapArea.Y; y < internalMapArea.Bottom; ++y)
                {
                    _map[CoordinatesToIndex(x, y)] = 0;
                }
            }
        }

        public ICollection<Point> GetReachablePointsFrom(Point internalCoordinates)
        {
            ICollection<Point> points;

            points = new List<Point>(4);
            if (SafeRead(internalCoordinates) == 1)
            {
                Point[] possiblePoints = new Point[] {
                    new Point( 1,  0),
                    new Point( 0,  1),
                    new Point(-1,  0),
                    new Point( 0, -1)
                };

                foreach (Point p in possiblePoints)
                {
                    Point op;

                    op = new Point(p.X + internalCoordinates.X, p.Y + internalCoordinates.Y);
                    if (SafeRead(op) == 1)
                    {
                        points.Add(op);
                    }
                }
            }
            return points;
        }

        public Point ExternalCoordinatesToInternal(Vector2 external)
        {
            return new Point(
                ((int)external.X) >> _powerOfTwo,
                ((int)external.Y) >> _powerOfTwo
                );
        }

        public Vector2 InternalCoordinatesToExternal(Point point)
        {
            return new Vector2(
                point.X << _powerOfTwo,
                point.Y << _powerOfTwo
                );
        }

        /*
         * private methods
         */

        private int SafeRead(Point p)
        {
            if (p.X >= 0 && p.Y >= 0 && p.X < _width && p.Y < _height)
            {
                return _map[CoordinatesToIndex(p)];
            }
            return 0;
        }

        private Rectangle ExternalMapAreaToInternal(Rectf externalArea)
        {
            Rectangle internalArea;

            internalArea = new Rectangle(
                ((int)externalArea.X) >> _powerOfTwo,
                ((int)externalArea.Y) >> _powerOfTwo,
                0, 0
                );
            internalArea.Width = (((int)(externalArea.Right)) >> _powerOfTwo) - internalArea.X;
            internalArea.Height = (((int)(externalArea.Bottom)) >> _powerOfTwo) - internalArea.Y;
            return internalArea;
        }

        private int CoordinatesToIndex(Point p)
        {
            return CoordinatesToIndex(p.X, p.Y);
        }

        private int CoordinatesToIndex(int x, int y)
        {
            return x + y * _width;
        }

    }
}
