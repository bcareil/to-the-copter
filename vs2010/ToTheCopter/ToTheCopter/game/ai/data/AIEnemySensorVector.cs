﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using ToTheCopter.game.api;

namespace ToTheCopter.game.ai.data
{
    class AIEnemySensorVector
    {

        public bool Bump { get; set; }
        public bool BumpPlayer { get; set; }
        public float ElapsedTime { get; set; }
        public IGameEntity Target { get; set; }
        public bool IsTargetClose { get; set; }

        public AIEnemySensorVector()
        {
            Bump = false;
            BumpPlayer = false;
            ElapsedTime = 0f;
            Target = null;
            IsTargetClose = false;
        }

    }
}
