﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace ToTheCopter.game.ai.data
{
    class HillClimbingData
    {
        public Point Origin;
        public Point PreviousPosition;
        public float BumpingTimer;
        public LinkedList<KeyValuePair<float, Point>> BlackListedNodes;

        public HillClimbingData()
        {
            Origin = new Point(-1, -1);
            PreviousPosition = new Point(-1, -1);
            BumpingTimer = 0f;
            BlackListedNodes = new LinkedList<KeyValuePair<float, Point>>();
        }

    }
}
