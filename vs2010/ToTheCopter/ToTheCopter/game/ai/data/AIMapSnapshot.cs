﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using ToTheCopter.engine.utils;

namespace ToTheCopter.game.ai.data
{
    class AIMapSnapshot
    {

        /*
         * private attributes
         */

        private float[] _map;
        private Point _translation;
        private int _width;
        private int _height;
        private int _powerOfTwo;

        /*
         * public constructor
         */

        public AIMapSnapshot(Point translation, int width, int height, int powerOfTwo)
        {
            _translation = translation;
            _width = width;
            _height = height;
            _powerOfTwo = powerOfTwo;
            _map = new float[_width * _height];
        }

        /*
         * public properties
         */

        public int SquareEdgeLength { get { return 1 << _powerOfTwo; } }

        /*
         * public methods
         */

        public void BlackListArea(Rectf area)
        {
            Rectangle internalArea;

            internalArea = ExternalAreaToInternal(area);
            for (int x = internalArea.X; x < internalArea.Right; ++x)
            {
                for (int y = internalArea.Y; y < internalArea.Bottom; ++y)
                {
                    _map[GetIndexForCoordinates(x, y)] = -1f;
                }
            }
        }

        public ICollection<AIMap.AIMapTile> GetReachablePointsFrom(Point p)
        {
            Point[] translations = new Point[] {
                new Point(-1, 0),
                new Point(1, 0),
                new Point(0, -1),
                new Point(0, 1)
                };
            ICollection<AIMap.AIMapTile> reachablePoints;
            Point localPoint;
            float track;

            reachablePoints = new LinkedList<AIMap.AIMapTile>();
            localPoint = new Point(p.X - _translation.X, p.Y - _translation.Y);
            foreach (Point translation in translations)
            {
                track = GetTrackSafely(localPoint.X + translation.X, localPoint.Y + translation.Y);
                if (track >= 0f)
                {
                    reachablePoints.Add(new AIMap.AIMapTile(track, new Point(p.X + translation.X, p.Y + translation.Y)));
                }
            }
            return reachablePoints;
        }

        public float GetTrack(int x, int y)
        {
            return _map[GetIndexForCoordinates(x - _translation.X, y - _translation.Y)];
        }

        public void SetTrack(int x, int y, float track)
        {
            _map[GetIndexForCoordinates(x - _translation.X, y - _translation.Y)] = track;
        }

        /*
         * private methods
         */

        private Rectangle ExternalAreaToInternal(Rectf area)
        {
            Rectangle internalArea;

            internalArea = new Rectangle(
                Math.Max(0, (int)area.X >> _powerOfTwo - _translation.X),
                Math.Max(0, (int)area.Y >> _powerOfTwo - _translation.Y),
                0, 0
                );
            internalArea.Width = Math.Min(_width - 1, (int)(area.Right + (SquareEdgeLength - 1)) >> _powerOfTwo - internalArea.X);
            internalArea.Height = Math.Min(_height - 1, (int)(area.Left + (SquareEdgeLength - 1)) >> _powerOfTwo - internalArea.Y);
            return internalArea;
        }

        private float GetTrackSafely(int x, int y)
        {
            if (x >= 0 && y >= 0 && x < _width && y < _height)
            {
                return _map[GetIndexForCoordinates(x, y)];
            }
            return -1f;
        }

        private int GetIndexForCoordinates(int x, int y)
        {
            return x + y * _width;
        }

    }
}
