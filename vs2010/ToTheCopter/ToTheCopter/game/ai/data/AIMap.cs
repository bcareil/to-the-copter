﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ToTheCopter.engine.utils;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace ToTheCopter.game.ai.data
{
    class AIMap : engine.api.IUpdatable
    {
        public struct AIMapTile
        {
            public float Track;
            public Point Position;

            public AIMapTile(float track, Point Position)
            {
                this.Track = track;
                this.Position = Position;
            }

            public bool HasTrack
            {
                get
                {
                    return Track >= 0f;
                }
            }
        }

        class Square
        {
            public const float MAX_VALUE = 32768f;
            public const float INCREASE_SPEED = MAX_VALUE / 4f;
            public const float VOID = -3f;
            public const float OBSTACLE = -2f;
            public const float NO_TRACK = -1f;

            private float _track;
            private float _lastUpdate;

            public Square()
            {
                _track = NO_TRACK;
                _lastUpdate = 0f;
            }

            public float LastUpdate { get { return _lastUpdate; } }
            public bool IsVoid { get { return _track == VOID; } }
            public bool IsObstacle { get { return _track == OBSTACLE; } }
            public bool HasTrack { get { return _track >= 0f; } }

            public void SetTrack(float totalElapsed, float track)
            {
                _track = track;
                _lastUpdate = totalElapsed;
            }

            public float GetTrack(float totalElapsed)
            {
                if (_track >= 0f && _lastUpdate != totalElapsed)
                {
                    float diff;
                    
                    diff = totalElapsed - _lastUpdate;
                    _track += diff * INCREASE_SPEED;
                    _lastUpdate = totalElapsed;
                    if (_track > MAX_VALUE)
                    {
                        _track = NO_TRACK;
                    }
                }
                return _track;
            }

        }

        public const float MAX_TRACK_VALUE = Square.MAX_VALUE;

        private Square[] _map;
        private int _powerOfTwo;
        private int _width;
        private int _height;
        private float _timestamp;

        public AIMap(int width, int height, int powerOfTwo)
        {
            _powerOfTwo = powerOfTwo;
            _width = width;
            _height = height;
            _map = new Square[_width * _height];
            _timestamp = 0f;

            for (int i = 0; i < _map.Length; ++i)
            {
                _map[i] = new Square();
            }
        }

        /*
         * public properties
         */

        public int SquareEdgeLength { get { return 1 << _powerOfTwo; } }

        /*
         * public methods
         */

        public void BlacklistArea(Rectf area)
        {
            SetTrackInArea(area, Square.OBSTACLE);
        }

        public void VoidArea(Rectf area)
        {
            SetTrackInArea(area, Square.VOID);
        }

        public void UnvoidArea(Rectf area)
        {
            SetTrackInArea(area, Square.NO_TRACK);
        }

        public void Update(float elapsed, float totalElapsed)
        {
            _timestamp = totalElapsed;
        }

        /// <summary>
        /// For debug purpose.
        /// </summary>
        /// <param name="sb"></param>
        public void Draw(SpriteBatch sb, Rectf area)
        {
            Rectangle internalArea;

            internalArea = ExternalMapAreaToInternal(area);
            for (int x = internalArea.X; x <= internalArea.Right; ++x)
            {
                for (int y = internalArea.Y; y <= internalArea.Bottom; ++y)
                {
                    float track;

                    track = SafeRead(x, y);
                    if (track >= 0f)
                    {
                        Game.GetInstance().RendererFactoryInst.BuildRectangleRenderer(
                            new Rectf(
                                x << _powerOfTwo,
                                y << _powerOfTwo,
                                SquareEdgeLength,
                                SquareEdgeLength
                                ),
                            Color.FromNonPremultiplied(0, 255, 0, (int)(128 - 128 * track / Square.MAX_VALUE))
                            ).Draw(sb);
                    }
                    else if (track == Square.NO_TRACK)
                    {
                        Game.GetInstance().RendererFactoryInst.BuildRectangleRenderer(
                            new Rectf(
                                x << _powerOfTwo,
                                y << _powerOfTwo,
                                SquareEdgeLength,
                                SquareEdgeLength
                                ),
                            Color.Blue * 0.25f
                            ).Draw(sb);
                    }
                    else if (track == Square.OBSTACLE)
                    {
                        Game.GetInstance().RendererFactoryInst.BuildRectangleRenderer(
                            new Rectf(
                                x << _powerOfTwo,
                                y << _powerOfTwo,
                                SquareEdgeLength,
                                SquareEdgeLength
                                ),
                            Color.Red * 0.25f
                            ).Draw(sb);
                    }
                }
            }
        }

        public void PropagateTrack(Vector2 center, float radius, float minimumPercent)
        {
            LinkedList<search.AISearchNode> frontier;
            HashSet<search.AISearchNode> explored;
            Point internalCenter;
            int internalRadius;
            float minTrackValue;

            frontier = new LinkedList<search.AISearchNode>();
            explored = new HashSet<search.AISearchNode>();
            internalCenter = ExternalCoordinatesToInternal(center);
            internalRadius = (int)radius >> _powerOfTwo;
            minTrackValue = minimumPercent * Square.MAX_VALUE;
            frontier.AddLast(new search.AISearchNode(null, internalCenter));
            if (internalRadius <= 0)
            {
                return;
            }
            while (frontier.Count > 0)
            {
                search.AISearchNode node;
                Point position;
                Square square;
                float track;

                node = frontier.First.Value;
                frontier.RemoveFirst();
                explored.Add(node);
                //
                position = node.GetPosition();
                square = _map[CoordinatesToIndex(position)];
                track = ((float)node.Depth / (float)internalRadius) * (Square.MAX_VALUE - minTrackValue) + minTrackValue;
                if (square.IsVoid == false && (square.HasTrack == false || square.GetTrack(_timestamp) > track))
                {
                    square.SetTrack(_timestamp, track);
                }
                //
                if (node.Depth < internalRadius)
                {
                    foreach (search.AISearchNode expandedNode in ExpandNode(node))
                    {
                        if (explored.Contains(expandedNode) == false && frontier.Contains(expandedNode) == false)
                        {
                            frontier.AddLast(expandedNode);
                        }
                    }
                }
            }
        }

        public bool IsThereTrack(Vector2 externalCoordinates)
        {
            return (SafeRead(ExternalCoordinatesToInternal(externalCoordinates)) >= 0f);
        }

        internal float GetTrack(Point internalCoordinates)
        {
            return SafeRead(internalCoordinates);
        }

        public ICollection<AIMapTile> GetReachablePointsFrom(Point internalCoordinates)
        {
            ICollection<AIMapTile> points;

            points = new List<AIMapTile>(4);
            if (SafeRead(internalCoordinates) >= 0f)
            {
                Point[] possiblePoints = new Point[] {
                    new Point( 1,  0),
                    new Point( 0,  1),
                    new Point(-1,  0),
                    new Point( 0, -1)
                };

                foreach (Point p in possiblePoints)
                {
                    float track;
                    Point op;

                    op = new Point(p.X + internalCoordinates.X, p.Y + internalCoordinates.Y);
                    track = SafeRead(op);
                    if (track >= 0)
                    {
                        points.Add(new AIMapTile(track, op));
                    }
                }
            }
            return points;
        }

        public Point ExternalCoordinatesToInternal(Vector2 external)
        {
            return new Point(
                ((int)external.X) >> _powerOfTwo,
                ((int)external.Y) >> _powerOfTwo
                );
        }

        public Vector2 InternalCoordinatesToExternal(Point point)
        {
            return new Vector2(
                point.X << _powerOfTwo,
                point.Y << _powerOfTwo
                );
        }

        /*
         * private methods
         */

        private void SetTrackInArea(Rectf area, float value)
        {
            Rectangle internalMapArea;

            internalMapArea = ExternalMapAreaToInternal(area);
            for (int x = internalMapArea.X; x < internalMapArea.Right; ++x)
            {
                for (int y = internalMapArea.Y; y < internalMapArea.Bottom; ++y)
                {
                    _map[CoordinatesToIndex(x, y)].SetTrack(_timestamp, value);
                }
            }
        }

        private ICollection<search.AISearchNode> ExpandNode(search.AISearchNode node)
        {
            ICollection<search.AISearchNode> expandedNodes;
            Point nodePosition;
            Point[] translations = {
                new Point(-1, 0),
                //new Point(-1, 1),
                new Point(0, 1),
                //new Point(1, 1),
                new Point(1, 0),
                //new Point(1, -1),
                new Point(0, -1),
                //new Point(-1, -1),
            };

            nodePosition = node.GetPosition();
            expandedNodes = new List<search.AISearchNode>();
            foreach (Point translation in translations)
            {
                Point newNodePosition;
                
                newNodePosition = new Point(
                    translation.X + nodePosition.X,
                    translation.Y + nodePosition.Y
                    );
                if (SafeRead(newNodePosition) != Square.OBSTACLE)
                {
                    expandedNodes.Add(new search.AISearchNode(node, newNodePosition));
                }
            }
            return expandedNodes;
        }

        private float SafeRead(Point p)
        {
            return SafeRead(p.X, p.Y);
        }

        private float SafeRead(int x, int y)
        {
            if (x >= 0 && y >= 0 && x < _width && y < _height)
            {
                return _map[CoordinatesToIndex(x, y)].GetTrack(_timestamp);
            }
            return Square.OBSTACLE;
        }

        private Rectangle ExternalMapAreaToInternal(Rectf externalArea)
        {
            Rectangle internalArea;

            internalArea = new Rectangle(
                ((int)externalArea.X) >> _powerOfTwo,
                ((int)externalArea.Y) >> _powerOfTwo,
                0, 0
                );
            internalArea.Width = (((int)(externalArea.Right)) >> _powerOfTwo) - internalArea.X;
            internalArea.Height = (((int)(externalArea.Bottom)) >> _powerOfTwo) - internalArea.Y;
            return internalArea;
        }

        private int CoordinatesToIndex(Point p)
        {
            return CoordinatesToIndex(p.X, p.Y);
        }

        private int CoordinatesToIndex(int x, int y)
        {
            return x + y * _width;
        }

    }
}
