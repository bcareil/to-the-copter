﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ToTheCopter.game.ai.nodes
{
    class AIBDirectPath : AAIBehavior
    {

        public AIBDirectPath(String name) : base(name) { }

        public override ToTheCopter.ai.api.Status Tick()
        {
            if (_commonData.SensorVector.Target == null)
            {
                return ToTheCopter.ai.api.Status.FAILURE;
            }
            _commonData.Target = _commonData.SensorVector.Target.CollideRect.Center;
            return ToTheCopter.ai.api.Status.SUCCESS;
        }
    }
}
