﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ToTheCopter.game.ai.nodes.sensors
{
    class AIBSensorHasDirectPath : AAIBehavior
    {
        public AIBSensorHasDirectPath(String name) : base(name) { }

        public override ToTheCopter.ai.api.Status Tick()
        {
            return (_commonData.SensorVector.IsTargetClose ? ToTheCopter.ai.api.Status.SUCCESS : ToTheCopter.ai.api.Status.FAILURE);
        }
    }
}
