﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ToTheCopter.ai.nodes;
using ToTheCopter.ai.api;

namespace ToTheCopter.game.ai.nodes.sensors
{
    class AIBSensorBump : ABehavior
    {
        public AIBSensorBump(String name) : base(name) { }

        public override Status Tick()
        {
            data.AIEnemyCommonData commonData;

            commonData = (data.AIEnemyCommonData)_root.Data.CommonData;
            return (commonData.SensorVector.Bump && commonData.Moving ? Status.SUCCESS : Status.FAILURE);
        }
    }
}
