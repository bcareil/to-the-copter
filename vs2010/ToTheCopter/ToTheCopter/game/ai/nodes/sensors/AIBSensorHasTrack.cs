﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ToTheCopter.ai.api;

namespace ToTheCopter.game.ai.nodes.sensors
{
    class AIBSensorHasTrack : AAIBehavior
    {
        public AIBSensorHasTrack(String name) : base(name) { }

        public override Status Tick()
        {
            return (Game.GetInstance().AIMapInst.IsThereTrack(_entity.CollideRect.Center) ? Status.SUCCESS : Status.FAILURE);
        }
    }
}
