﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ToTheCopter.game.ai.nodes.expressions
{
    class AIBExpressionHasValidPath : AAIBehavior
    {
        public AIBExpressionHasValidPath(String name) : base(name) { }

        public override ToTheCopter.ai.api.Status Tick()
        {
            return (_commonData.Path != null && _commonData.Path.Count > 0 ?
                        ToTheCopter.ai.api.Status.SUCCESS :
                        ToTheCopter.ai.api.Status.FAILURE
                        );
        }
    }
}
