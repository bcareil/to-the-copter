﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ToTheCopter.ai.api;
using Microsoft.Xna.Framework;

namespace ToTheCopter.game.ai.nodes
{
    class AIBHillClimbing : AAIBehavior
    {
        private const float DELAY_NODE_BLACKLISTED = 1f;
        private const float DELAY_BEFORE_BLACKLISTING = 0.3f;

        public AIBHillClimbing(String name) : base(name) { }

        public override IBehaviorDataNode FirstSetUp(IRootBehavior root)
        {
            IBehaviorDataNode dataNode;

            dataNode = base.FirstSetUp(root);
            dataNode.Data = new data.HillClimbingData();
            return dataNode;
        }

        public override ToTheCopter.ai.api.Status Tick()
        {
            data.HillClimbingData hillClimbingData;
            Point currentPosition;
            data.AIMap aiMap;
            Vector2? target;
            float min;

            target = null;
            aiMap = Game.GetInstance().AIMapInst;
            hillClimbingData = (data.HillClimbingData)_dataNode.Data;
            //
            currentPosition = aiMap.ExternalCoordinatesToInternal(_entity.CollideRect.Center);
            if (currentPosition != hillClimbingData.PreviousPosition)
            {
                hillClimbingData.Origin = hillClimbingData.PreviousPosition;
                hillClimbingData.PreviousPosition = currentPosition;
            }
            //
            UpdateBlacklistedNode(hillClimbingData, _commonData.SensorVector.ElapsedTime);
            if (_commonData.SensorVector.Bump && _commonData.SensorVector.BumpPlayer == false)
            {
                hillClimbingData.BumpingTimer += _commonData.SensorVector.ElapsedTime;
                if (hillClimbingData.BumpingTimer > DELAY_BEFORE_BLACKLISTING)
                {
                    hillClimbingData.BumpingTimer = 0f;
                    hillClimbingData.PreviousPosition = currentPosition;
                    hillClimbingData.BlackListedNodes.AddLast(
                        new LinkedListNode<KeyValuePair<float, Point>>(
                            new KeyValuePair<float, Point>(
                                DELAY_NODE_BLACKLISTED,
                                aiMap.ExternalCoordinatesToInternal(_commonData.Target)
                                )
                            )
                        );
                }
            }
            else
            {
                hillClimbingData.BumpingTimer = 0f;
            }
            //
            min = data.AIMap.MAX_TRACK_VALUE;
            foreach (data.AIMap.AIMapTile aiMapTile in aiMap.GetReachablePointsFrom(currentPosition))
            {
                if (aiMapTile.Position != hillClimbingData.Origin
                    && aiMapTile.HasTrack && aiMapTile.Track < min
                    && IsBlacklisted(hillClimbingData, aiMapTile.Position) == false)
                {
                    min = aiMapTile.Track;
                    target = aiMap.InternalCoordinatesToExternal(aiMapTile.Position)
                        + new Vector2(aiMap.SquareEdgeLength / 2, aiMap.SquareEdgeLength / 2);
                }
            }
            //
            if (target == null)
            {
                if (min != 0)
                {
                    return Status.FAILURE;
                }
                target = aiMap.InternalCoordinatesToExternal(currentPosition)
                    + new Vector2(aiMap.SquareEdgeLength / 2, aiMap.SquareEdgeLength / 2);
            }
            //
            _commonData.Target = (Vector2)target;
            return Status.SUCCESS;
        }

        /*
         * private methods
         */

        private void UpdateBlacklistedNode(data.HillClimbingData hillClimbingData, float elapsed)
        {
            LinkedListNode<KeyValuePair<float, Point>> node;

            node = hillClimbingData.BlackListedNodes.First;
            while (node != null)
            {
                float key;

                key = node.Value.Key - elapsed;
                if (key <= 0f)
                {
                    hillClimbingData.BlackListedNodes.Remove(node);
                }
                else
                {
                    node.Value = new KeyValuePair<float, Point>(key, node.Value.Value);
                }
                node = node.Next;
            }
        }

        private bool IsBlacklisted(data.HillClimbingData hillClimbingData, Point p)
        {
            foreach (KeyValuePair<float, Point> node in hillClimbingData.BlackListedNodes)
            {
                if (node.Value == p)
                {
                    return true;
                }
            }
            return false;
        }

    }
}
