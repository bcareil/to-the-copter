﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ToTheCopter.ai.nodes;
using ToTheCopter.ai.api;
using ToTheCopter.game.ai.data;
using Microsoft.Xna.Framework;

namespace ToTheCopter.game.ai.nodes
{
    class AIBMove : AAIBehavior
    {

        public AIBMove(String name) : base(name)
        {
        }

        public override Status Tick()
        {
            Vector2 movement;

            movement = _commonData.Target - _entity.CollideRect.Center;
            if (movement.LengthSquared()
                < ((_entity.CollideRect.Width * _entity.CollideRect.Width) / 4f))
            {
                return Status.SUCCESS;
            }
            _commonData.Moving = true;
            movement.Normalize();
            _entity.LookingDirection = movement;
            _entity.MovingVector += movement * _commonData.MovingSpeed;
            return Status.RUNNING;
        }

        public override void TearDown(Status status)
        {
            if (status != Status.RUNNING)
            {
                _commonData.Moving = false;
            }
        }
    }
}
