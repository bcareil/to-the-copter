﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ToTheCopter.ai.nodes;
using ToTheCopter.game.ai.data;

namespace ToTheCopter.game.ai.nodes
{
    abstract class AAIBehavior : ABehavior
    {

        protected entities.Enemy _entity;
        protected AIEnemyCommonData _commonData;

        public AAIBehavior(String name)
            : base(name)
        {
        }

        public override void InitializeData(ToTheCopter.ai.api.IBehaviorDataNode node)
        {
            base.InitializeData(node);
            _entity = (entities.Enemy)_root.Data.Entity;
            _commonData = (AIEnemyCommonData)_root.Data.CommonData;
        }

    }
}
