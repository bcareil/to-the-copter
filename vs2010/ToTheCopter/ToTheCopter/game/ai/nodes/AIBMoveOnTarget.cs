﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ToTheCopter.ai.nodes;
using ToTheCopter.ai.api;
using ToTheCopter.game.ai.data;
using Microsoft.Xna.Framework;
using ToTheCopter.game.entities;

namespace ToTheCopter.game.ai.nodes
{
    class AIBMoveOnTarget : AAIBehavior
    {

        public AIBMoveOnTarget(String name)
            : base(name)
        {
        }

        public override Status Tick()
        {
            Vector2 movement;

            _commonData.Moving = true;
            _commonData.MovingSpeed = _entity.Properties.RunSpeed;
            movement = _commonData.Target - _entity.CollideRect.Center;
            movement.Normalize();
            _entity.LookingDirection = movement;
            _entity.MovingVector += movement * _commonData.MovingSpeed;
            return Status.SUCCESS;
        }

        public override void TearDown(Status status)
        {
            if (status != Status.RUNNING)
            {
                _commonData.Moving = false;
            }
        }
    }
}
