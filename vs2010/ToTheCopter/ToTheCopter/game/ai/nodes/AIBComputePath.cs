﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ToTheCopter.ai.nodes;
using ToTheCopter.game.ai.data;
using ToTheCopter.game.ai.search;
using ToTheCopter.engine.algorithm.search;
using Microsoft.Xna.Framework;
using ToTheCopter.engine.algorithm.search.api;
using ToTheCopter.engine.utils;

namespace ToTheCopter.game.ai.nodes
{
    class AIBComputePath : AAIBehavior
    {

        const int MAX_ITERATION = 128;

        public AIBComputePath(String name)
            : base(name)
        {
        }

        public override ToTheCopter.ai.api.Status Tick()
        {
            #region DEBUG
            if (_commonData.SensorVector.Target == null)
            {
                throw new Exception("AIBComputePath : target is null");
            }
            #endregion

            AISearchProblem searchProblem;
            ISearchAlgorithm aStartSearch;
            ICollection<ISearchNode> path;

            aStartSearch = new AStarSearch();
            searchProblem = new AISearchProblem(
                _entity.CollideRect.Center,
                _commonData.SensorVector.Target.CollideRect.Center,
                Game.GetInstance().AIMapInst,
                MAX_ITERATION
                );
            path = aStartSearch.ComputePath(searchProblem);
            if (path == null)
            {
                return ToTheCopter.ai.api.Status.FAILURE;
            }
            _commonData.Path = SimplifyPath(searchProblem.NodeListToCoordinateList(path), 64 / 2 - 30 / 2 /* TODO: use a constant */);
#if DEBUG
            _commonData.DebugPath = searchProblem.NodeListToCoordinateList(path);
#endif
            return ToTheCopter.ai.api.Status.SUCCESS;
        }

        private ICollection<Vector2> SimplifyPath(ICollection<Vector2> path, float threshold)
        {
            Vector2 previous;
            Vector2 start;
            Vector2 end;
            ICollection<Vector2> processedWayPoints;
            ICollection<Vector2> simplifiedPath;

            processedWayPoints = new LinkedList<Vector2>();
            simplifiedPath = new LinkedList<Vector2>();
            previous = new Vector2();
            start = new Vector2();
            end = new Vector2();
            // we cannot simplify a path of less than 3 steps
            if (path.Count < 3)
            {
                return path;
            }
            // get the first waypoint
            foreach (Vector2 wayPoint in path)
            {
                start = wayPoint;
                break;
            }
            // start the main computation loop
            foreach (Vector2 wayPoint in path)
            {
                Vector2 line;

                // create a line between the start and the new waypoint
                end = wayPoint;
                line = end - start;
                line.Normalize();
                // then, for each processedWayPoints, check that, by deleting this step,
                // we do not simplify too much the path
                if (processedWayPoints.Count >= 3)
                {
                    foreach (Vector2 processedWayPoint in processedWayPoints)
                    {
                        // this check is performed by computing the distance between the line and the
                        // processed way point.
                        if (CollisionHelper.ComputeDistancePointToALine(start, line, processedWayPoint) > threshold)
                        {
                            // if it cross the threshold, the previous node was the last one of the
                            // aligned nodes, so it become our new start.
                            simplifiedPath.Add(start);
                            start = previous;
                            processedWayPoints.Clear();
                            processedWayPoints.Add(start);
                            break;
                        }
                    }
                }
                // then we add end to the processed way point
                processedWayPoints.Add(end);
                // and save it as the previous node
                previous = end;
            }
            //
            foreach (Vector2 processedWayPoint in processedWayPoints)
            {
                simplifiedPath.Add(processedWayPoint);
                break;
            }
            simplifiedPath.Add(end);
            return simplifiedPath;
        }
    }
}
