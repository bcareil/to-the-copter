﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ToTheCopter.ai.api;
using ToTheCopter.game.ai.data;
using Microsoft.Xna.Framework;
using ToTheCopter.game.entities;

namespace ToTheCopter.game.ai.nodes
{
    class AIBFollowPath : AAIBehavior
    {

        private FollowPathData _data;
        private AIBMove _move;

        public AIBFollowPath(String name)
            : base(name)
        {
            _move = new AIBMove("MoveFollowingPath");
        }

        public override IBehaviorDataNode FirstSetUp(IRootBehavior root)
        {
            IBehaviorDataNode dataNode;
            
            dataNode = base.FirstSetUp(root);
            dataNode.Data = new FollowPathData();
            dataNode.AddChild(_move.FirstSetUp(root));
            return dataNode;
        }

        public override void InitializeData(IBehaviorDataNode node)
        {
            base.InitializeData(node);
            _data = (FollowPathData)_dataNode.Data;
            foreach (IBehaviorDataNode childData in _dataNode.Children)
            {
                _move.InitializeData(childData);
            }
        }

        public override void SetUp()
        {
            base.SetUp();
            if (_data.PathIterator == null)
            {
                _data.PathIterator = _commonData.Path.GetEnumerator();
            }
        }

        public override Status Tick()
        {
            if (_commonData.Moving)
            {
                RunMove();
            }
            else
            {
                if (_data.PathIterator.MoveNext() == false)
                {
                    return ToTheCopter.ai.api.Status.FAILURE;
                }
                _commonData.Target = _data.PathIterator.Current;
                RunMove();
            }
            return Status.RUNNING;
        }

        public override void TearDown(Status status)
        {
            if (status != Status.RUNNING)
            {
                _data.PathIterator = null;
                _commonData.Path = null;
            }
        }

        /*
         * private methods
         */

        private void RunMove()
        {
            Status moveStatus;

            _commonData.MovingSpeed = _entity.Properties.RunSpeed;
            _move.SetUp();
            moveStatus = _move.Tick();
            _move.TearDown(moveStatus);
        }
    }
}
