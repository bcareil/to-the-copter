﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ToTheCopter.ai.nodes;
using Microsoft.Xna.Framework;
using ToTheCopter.game.entities;

namespace ToTheCopter.game.ai.nodes
{
    class AIBRandomMovement : AAIBehavior
    {
        private Random _rand = new Random();

        public AIBRandomMovement(String name)
            : base(name)
        {
        }

        public override ToTheCopter.ai.api.Status Tick()
        {
            _commonData.Target =
                _entity.CollideRect.Center
                + new Vector2(_rand.Next(256) - 128, _rand.Next(256) - 128);
            _commonData.MovingSpeed =
                _entity.Properties.WalkSpeed;
            return ToTheCopter.ai.api.Status.SUCCESS;
        }
    }
}
